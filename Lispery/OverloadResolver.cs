﻿using Lispery.Containers;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	/// <summary>
	/// Attempts to resolve overload conflicts.
	/// </summary>
	public class OverloadResolver
	{
		private readonly List<NativeFunction> overloads = new();

		private readonly Dictionary<Type?[], NativeFunction> cache;
		private readonly int maxCacheSize = 10;
		private readonly int[] matchArities = new int[9];

		public OverloadResolver()
		{
			cache = new Dictionary<Type?[], NativeFunction>(new ArrayComparer<Type?>());

			ResetCache();
		}

		public IEnumerable<NativeFunction> Overloads
		{
			get { return overloads; }
		}

		private void ResetCache()
		{
			cache.Clear();

			for (int i = 0; i < matchArities.Length; i++)
			{
				matchArities[i] = -1;
			}
		}

		/// <summary>
		/// Adds a new overload to the list of overloads.
		/// </summary>
		public void AddOverload(NativeFunction function)
		{
			overloads.Add(function);
		}

		private static void ThrowAmbiguous(NativeFunction lhs, NativeFunction rhs)
		{
			throw new InvalidOperationException(
				$"Ambiguous overload. Can't decide between {lhs} and {rhs}.");
		}

		/// <summary>
		/// Determines whether lhs can be assigned to rhs.
		/// </summary>
		private static bool IsAssignableTo(Type? lhs, Type rhs)
		{
			if (lhs == null)
			{
				return !rhs.IsValueType;
			}

			if (lhs == rhs)
			{
				return true;
			}

			if (rhs == typeof(double))
			{
				if (lhs == typeof(long))
				{
					return true;
				}
			}

			return lhs.IsAssignableTo(rhs);
		}

		public enum MatchResult
		{
			ExactMatch,
			Match,
			NoMatch
		}

		/// <summary>
		/// Checks if the given argument types are compatible.
		/// </summary>
		public static MatchResult MatchesArgumentTypes(NativeFunction fun, Type?[] types)
		{
			if (!fun.SupportsArity(types.Length))
			{
				return MatchResult.NoMatch;
			}

			bool isEqual = true;

			for (int i = 0; i < types.Length; i++)
			{
				Type lhsType = fun.GetArgumentType(i);
				Type? rhsType = types[i];

				if (lhsType != rhsType)
				{
					isEqual = false;

					if (!IsAssignableTo(rhsType, lhsType))
					{
						return MatchResult.NoMatch;
					}
				}
			}

			return isEqual ?
				MatchResult.ExactMatch :
				MatchResult.Match;
		}

		public enum Specificity
		{
			MoreSpecific,
			LessSpecific,
			Ambiguous
		}

		/// <summary>
		/// Tries to determine if the current type list is more specific than
		/// the given one. Also will determine if the answer is ambiguous.
		/// </summary>
		public static Specificity IsMoreSpecificThan(NativeFunction lhs, NativeFunction rhs)
		{
			// If one function is variadic, while the other is not and if the
			// arity matches (ie. the variadic part is empty), then the non-
			// variadic function is always a better match.
			if (lhs.IsVariadic != rhs.IsVariadic && lhs.Arity == rhs.Arity)
			{
				return lhs.IsVariadic ?
					Specificity.LessSpecific :
					Specificity.MoreSpecific;
			}

			Specificity result = Specificity.Ambiguous;

			for (int i = 0; i < lhs.Arity; i++)
			{
				Type lhsType = lhs.GetArgumentType(i);
				Type rhsType = rhs.GetArgumentType(i);

				if (lhsType == rhsType)
				{
					continue;
				}
				else if (IsAssignableTo(lhsType, rhsType))
				{
					if (result == Specificity.LessSpecific)
					{
						return Specificity.Ambiguous;
					}

					// lhs seems to be more specific.
					result = Specificity.MoreSpecific;
				}
				else if (IsAssignableTo(rhsType, lhsType))
				{
					if (result == Specificity.MoreSpecific)
					{
						return Specificity.Ambiguous;
					}

					// lhs seems to be less specific.
					result = Specificity.LessSpecific;
				}
				else
				{
					// How can there be one type that can be assigned to both?
					return Specificity.Ambiguous;
				}
			}

			return result;
		}

		private int FindMatchArity(int callArity)
		{
			int matchArity = -1;

			if (callArity < matchArities.Length)
			{
				matchArity = matchArities[callArity];

				if (matchArity >= 0)
				{
					return matchArity;
				}
			}

			foreach (var candidate in overloads)
			{
				// Filter out overloads that don't support the call arity.
				if (!candidate.SupportsArity(callArity))
				{
					continue;
				}

				int arity = candidate.Arity;

				// If we have a variadic function and if we can match on the
				// first variadic argument, do so.
				if (candidate.IsVariadic && arity < callArity)
				{
					arity++;
				}

				// Keep track of the maximum arity.
				if (arity > matchArity)
				{
					matchArity = arity;
				}
			}

			if (callArity < matchArities.Length)
			{
				matchArities[callArity] = matchArity;
			}

			return matchArity;
		}

		private NativeFunction FindBestOverload(Type?[] argTypes)
		{
			NativeFunction? best = null;

			foreach (var candidate in overloads)
			{
				if (!candidate.SupportsArity(argTypes.Length))
				{
					continue;
				}

				var matchResult = MatchesArgumentTypes(candidate, argTypes);

				if (matchResult == MatchResult.ExactMatch)
				{
					return candidate;
				}

				if (matchResult == MatchResult.Match)
				{
					if (best == null)
					{
						best = candidate;
					}
					else
					{
						var specificity = IsMoreSpecificThan(candidate, best);

						if (specificity == Specificity.Ambiguous)
						{
							// TODO: this might be wrong.
							//best = null;
							ThrowAmbiguous(best, candidate);
						}
						else if (specificity == Specificity.MoreSpecific)
						{
							best = candidate;
						}
					}
				}
			}

			if (best == null)
			{
				// TODO: output parameter types?
				throw new InvalidOperationException("No compatible overload found");
			}

			return best;
		}

		private NativeFunction FindBestOverloadCached(Type?[] argTypes)
		{
			if (!cache.TryGetValue(argTypes, out var result))
			{
				result = FindBestOverload(argTypes);

				if (cache.Count == maxCacheSize)
				{
					cache.Remove(cache.First().Key);
				}

				cache[argTypes] = result;
			}

			return result;
		}

		private static Type?[] MakeTypeArray(List args, int maxCount)
		{
			Type?[] result = new Type?[maxCount];

			int i = 0;
			foreach (var arg in args)
			{
				result[i++] = arg?.GetType();

				if (i == maxCount)
				{
					break;
				}
			}

			return result;
		}

		public NativeFunction Resolve(List args)
		{
			int matchArity = FindMatchArity(args.Count);

			if (matchArity < 0)
			{
				throw new InvalidOperationException("No compatible overload found");
			}

			// Get the types for the arguments, up to the match arity.
			var argTypes = MakeTypeArray(args, matchArity);

			return FindBestOverloadCached(argTypes);
		}
	}
}
