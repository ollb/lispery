﻿using Lispery.Interfaces;
using Lispery.Values;
using Lispery.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lispery.Sequences;

namespace Lispery
{
	/// <summary>
	/// Helper class that can be used to optimize and speed up specific types
	/// of operations, by avoiding boxing / unboxing and type conversions.
	/// </summary>
	public class Optimizer
	{
		/// <summary>
		/// Converts the given function into an optimized function, under the assumption
		/// that it is a function that aggregates a list of values.
		/// </summary>
		public static Func<IEnumerable<double>, double> OptimizeAggregate(IFunction fun)
		{
			return fun.FunctionType switch
			{
				FunctionType.Sum     => v => v.Sum(),
				FunctionType.Average => v => v.Average(),
				FunctionType.Product => v => v.Aggregate((a, b) => a * b),
				FunctionType.Median  => v => v.Median(),
				_                    => v => Conversion.To<double>(fun.Call(new LazySequence(v.Cast<object?>())))
			};
		}

		/// <summary>
		/// Converts the given function into an optimized function, under the assumption
		/// that it is a function that combines two double values.
		/// </summary>
		public static Func<double, double, double> OptimizeCombineDouble(IFunction fun)
		{
			return fun.FunctionType switch
			{
				FunctionType.Add      => (a, b) => a + b,
				FunctionType.Subtract => (a, b) => a - b,
				FunctionType.Divide   => (a, b) => a / b,
				FunctionType.Multiply => (a, b) => a * b,
				_                     => (a, b) => Conversion.To<double>(fun.Call(a, b)),
			};
		}

		/// <summary>
		/// Converts the given function into an optimized function, under the assumption
		/// that it is a function that combines two long values.
		/// </summary>
		public static Func<long, long, long> OptimizeCombineLong(IFunction fun)
		{
			return fun.FunctionType switch
			{
				FunctionType.Add      => (a, b) => a + b,
				FunctionType.Subtract => (a, b) => a - b,
				FunctionType.Divide   => (a, b) => a / b,
				FunctionType.Multiply => (a, b) => a * b,
				_                     => (a, b) => Conversion.To<long>(fun.Call(a, b)),
			};
		}

		/// <summary>
		/// Converts the given function into an optimized function, under the assumption
		/// that it is a function that compares two double values.
		/// </summary>
		public static Func<double, double, bool> OptimizeCompareDouble(IFunction fun)
		{
			return fun.FunctionType switch
			{
				FunctionType.Less           => (a, b) => a < b,
				FunctionType.LessOrEqual    => (a, b) => a <= b,
				FunctionType.Greater        => (a, b) => a > b,
				FunctionType.GreaterOrEqual => (a, b) => a >= b,
				FunctionType.Equal          => (a, b) => a == b,
				FunctionType.NotEqual       => (a, b) => a != b,
				_                           => (a, b) => Conversion.To<bool>(fun.Call(a, b)),
			};
		}

		/// <summary>
		/// Converts the given function into an optimized function, under the assumption
		/// that it is a function that compares two long values.
		/// </summary>
		public static Func<long, long, bool> OptimizeCompareLong(IFunction fun)
		{
			return fun.FunctionType switch
			{
				FunctionType.Less           => (a, b) => a < b,
				FunctionType.LessOrEqual    => (a, b) => a <= b,
				FunctionType.Greater        => (a, b) => a > b,
				FunctionType.GreaterOrEqual => (a, b) => a >= b,
				FunctionType.Equal          => (a, b) => a == b,
				FunctionType.NotEqual       => (a, b) => a != b,
				_                           => (a, b) => Conversion.To<bool>(fun.Call(a, b)),
			};
		}
	}
}
