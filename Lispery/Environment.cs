﻿using Lispery.Containers;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	public class Environment
	{
		private readonly Dictionary<Symbol, object?> values = new(ReferenceEqualityComparer.Instance);

		/// <summary>
		/// Creates a new environment.
		/// </summary>
		public Environment(Interpreter interpreter)
		{
			Interpreter = interpreter;
		}

		/// <summary>
		/// Creates a child environment.
		/// </summary>
		public Environment(Environment parent)
		{
			Interpreter = parent.Interpreter;
			Parent = parent;
		}

		/// <summary>
		/// Returns the global symbol table for the interpreter.
		/// </summary>
		public SymbolTable SymbolTable
		{
			get { return Interpreter.SymbolTable; }
		}

		/// <summary>
		/// Returns the global keyword table for the interpreter.
		/// </summary>
		public KeywordTable KeywordTable
		{
			get { return Interpreter.KeywordTable; }
		}

		/// <summary>
		/// The interpreter that the environment belongs to.
		/// </summary>
		public Interpreter Interpreter { get; init; }

		/// <summary>
		/// The parent environment or <c>null</c> if there is none.
		/// </summary>
		public Environment? Parent { get; init; }

		/// <summary>
		/// Removes all symbol definitions from the environment.
		/// </summary>
		public void Clear()
		{
			values.Clear();
		}

		/// <summary>
		/// Returns the symbol with the given name.
		/// </summary>
		public Symbol GetSymbol(string name)
		{
			return SymbolTable.Get(name);
		}

		/// <summary>
		/// Returns the keyword with the given name.
		/// </summary>
		public Keyword GetKeyword(string name)
		{
			return KeywordTable.Get(name);
		}

		/// <summary>
		/// Resolves the given symbol. Throws an exception if the symbol is not found.
		/// </summary>
		public object? Resolve(string name)
		{
			return Resolve(SymbolTable.Get(name));
		}

		/// <summary>
		/// Resolves the given symbol. Throws an exception if the symbol is not found.
		/// </summary>
		public object? Resolve(Symbol symbol)
		{
			if (TryResolve(symbol, out var value))
			{
				return value;
			}

			throw new KeyNotFoundException($"Symbol \"{symbol}\" not found");
		}

		/// <summary>
		/// Tries to resolves the given symbol.
		/// </summary>
		public bool TryResolve(Symbol symbol, out object? value)
		{
			if (values.TryGetValue(symbol, out value))
			{
				return true;
			}
			else if (Parent != null)
			{
				return Parent.TryResolve(symbol, out value);
			}

			value = null;
			return false;
		}

		/// <summary>
		/// Assign a value to an existing mapping.
		/// </summary>
		public void Assign(Symbol symbol, object? value)
		{
			if (!TryAssign(symbol, value))
			{
				throw new KeyNotFoundException($"Symbol \"{symbol}\" not found");
			}
		}

		/// <summary>
		/// Tries to assign a value to an existing mapping.
		/// </summary>
		public bool TryAssign(Symbol symbol, object? value)
		{
			if (values.ContainsKey(symbol))
			{
				values[symbol] = value;
				return true;
			}
			else if (Parent != null)
			{
				return Parent.TryAssign(symbol, value);
			}

			return false;
		}

		/// <summary>
		/// Undefines a symbol.
		/// </summary>
		public void Undefine(string symbolName)
		{
			Undefine(GetSymbol(symbolName));
		}

		/// <summary>
		/// Undefines a symbol.
		/// </summary>
		public void Undefine(Symbol symbol)
		{
			values.Remove(symbol);
		}

		/// <summary>
		/// Defines a new symbol.
		/// </summary>
		public void Define(string symbolName, object? value)
		{
			Define(SymbolTable.Get(symbolName), value);
		}

		/// <summary>
		/// Defines a new symbol.
		/// </summary>
		public void Define(Symbol symbol, object? value)
		{
			values[symbol] = value;
		}

		/// <summary>
		/// Defines the given symbol or tries to overload it.
		/// </summary>
		public void DefineOrOverload(string symbol, NativeFunction fun)
		{
			DefineOrOverload(SymbolTable.Get(symbol), fun);
		}

		/// <summary>
		/// Defines the given symbol or tries to overload it.
		/// </summary>
		public void DefineOrOverload(Symbol symbol, NativeFunction fun)
		{
			if (TryResolve(symbol, out var value))
			{ 
				switch (value)
				{
					case NativeFunction oldFun:
						Define(symbol, new OverloadFunction(oldFun, fun));
						return;

					case OverloadFunction dispatch:
						dispatch.AddFunction(fun);
						return;
				}
			}

			Define(symbol, fun);
		}

		#region Define function helper methods
		/// <summary>
		/// Defines a function.
		/// </summary>
		public void DefineFunction(string name, Func<object?> func)
		{
			var types = Array.Empty<Type>();

			DefineOrOverload(name, new NativeFunction(name, types, null, args =>
			{
				Check.Arity(args, 0, name);
				return func();
			}));
		}

		/// <summary>
		/// Defines a function.
		/// </summary>
		public void DefineFunction<T>(string name, Func<T?, object?> func)
		{
			var types = new Type[] { typeof(T) };

			DefineOrOverload(name, new NativeFunction(name, types, null, args =>
			{
				Check.Arity(args, 1, name);

				var a = args.First();
				return func(
					Conversion.ToNullable<T>(a));
			}));
		}

		/// <summary>
		/// Defines a function.
		/// </summary>
		public void DefineFunction<T>(string name, FunctionType functionType, Func<T?, object?> func)
		{
			var types = new Type[] { typeof(T) };

			DefineOrOverload(name, new NativeFunction(name, functionType, types, null, args =>
			{
				Check.Arity(args, 1, name);

				var a = args.First();
				return func(
					Conversion.ToNullable<T>(a));
			}));
		}

		/// <summary>
		/// Defines a function.
		/// </summary>
		public void DefineFunction<T>(string name, Func<T?[], object?> func)
		{
			var types = Array.Empty<Type>();

			DefineOrOverload(name, new NativeFunction(name, types, typeof(T), args =>
			{
				var rest = args;
				return func(
					rest.Select(v => Conversion.ToNullable<T>(v)).ToArray());
			}));
		}

		/// <summary>
		/// Defines a function.
		/// </summary>
		public void DefineFunction<T, U>(string name, Func<T?, U?, object?> func)
		{
			var types = new Type[] { typeof(T), typeof(U) };

			DefineOrOverload(name, new NativeFunction(name, types, null, args =>
			{
				Check.Arity(args, 2, name);

				var (a, b) = args.Match2();
				return func(
					Conversion.ToNullable<T>(a),
					Conversion.ToNullable<U>(b));
			}));
		}

		/// <summary>
		/// Defines a function.
		/// </summary>
		public void DefineFunction<T, U>(string name, Func<T?, U?[], object?> func)
		{
			var types = new Type[] { typeof(T) };

			DefineOrOverload(name, new NativeFunction(name, types, typeof(U), args =>
			{
				Check.MinArity(args, 1, name);

				var (a, rest) = args.Match1R();
				return func(
					Conversion.ToNullable<T>(a),
					rest.Select(v => Conversion.ToNullable<U>(v)).ToArray());
			}));
		}

		/// <summary>
		/// Defines a function.
		/// </summary>
		public void DefineFunction<T, U, V>(string name, Func<T?, U?, V?, object?> func)
		{
			var types = new Type[] { typeof(T), typeof(U), typeof(V) };

			DefineOrOverload(name, new NativeFunction(name, types, null, args =>
			{
				Check.Arity(args, 3, name);

				var (a, b, c) = args.Match3();
				return func(
					Conversion.ToNullable<T>(a),
					Conversion.ToNullable<U>(b),
					Conversion.ToNullable<V>(c));
			}));
		}

		/// <summary>
		/// Defines a function.
		/// </summary>
		public void DefineFunction<T, U, V>(string name, Func<T?, U?, V?[], object?> func)
		{
			var types = new Type[] { typeof(T), typeof(U) };

			DefineOrOverload(name, new NativeFunction(name, types, typeof(V), args =>
			{
				Check.MinArity(args, 2, name);

				var (a, b, rest) = args.Match2R();
				return func(
					Conversion.ToNullable<T>(a),
					Conversion.ToNullable<U>(b),
					rest.Select(v => Conversion.ToNullable<V>(v)).ToArray());
			}));
		}

		/// <summary>
		/// Defines a function.
		/// </summary>
		public void DefineFunction<T, U, V, W>(string name, Func<T?, U?, V?, W?, object?> func)
		{
			var types = new Type[] { typeof(T), typeof(U), typeof(V), typeof(W) };

			DefineOrOverload(name, new NativeFunction(name, types, null, args =>
			{
				Check.Arity(args, 4, name);

				var (a, b, c, d) = args.Match4();
				return func(
					Conversion.ToNullable<T>(a),
					Conversion.ToNullable<U>(b),
					Conversion.ToNullable<V>(c),
					Conversion.ToNullable<W>(d));
			}));
		}

		/// <summary>
		/// Defines a function.
		/// </summary>
		public void DefineFunction<T, U, V, W>(string name, Func<T?, U?, V?, W?[], object?> func)
		{
			var types = new Type[] { typeof(T), typeof(U), typeof(V) };

			DefineOrOverload(name, new NativeFunction(name, types, typeof(W), args =>
			{
				Check.MinArity(args, 3, name);

				var (a, b, c, rest) = args.Match3R();
				return func(
					Conversion.ToNullable<T>(a),
					Conversion.ToNullable<U>(b),
					Conversion.ToNullable<V>(c),
					rest.Select(v => Conversion.ToNullable<W>(v)).ToArray());
			}));
		}
		#endregion
	}
}
