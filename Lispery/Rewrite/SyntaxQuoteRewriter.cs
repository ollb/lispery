﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Rewrite
{
	/// <summary>
	/// Rewrites syntax-quoted syntax trees.
	/// </summary>
	internal class SyntaxQuoteRewriter : TreeRewriter
	{
		private readonly Environment env;
		private readonly SymbolGenerator symbolGenerator;

		/// <summary>
		/// Creates a new rewriter.
		/// </summary>
		public SyntaxQuoteRewriter(Environment env)
		{
			this.env = env;
			symbolGenerator = new SymbolGenerator(env.SymbolTable);
		}

		/// <summary>
		/// Callback handler function for symbols.
		/// </summary>
		protected override bool HandleSymbol(Symbol symbol, out object? result)
		{
			if (SymbolGenerator.IsTemplateSymbol(symbol))
			{
				result = symbolGenerator.Generate(symbol);
				return true;
			}

			result = null;
			return false;
		}

		/// <summary>
		/// Callback handler function for list values.
		/// </summary>
		protected override bool HandleList(List list, out object? result)
		{
			if (list.Count == 2)
			{
				switch (list.First())
				{
					case var sym when sym == env.SymbolTable.SymUnquote:
						// Evaluate the unquoted value.
						result = env.Interpreter.Evaluate(list.Nth(1), env);
						return true;

					case var sym when sym == env.SymbolTable.SymUnquoteSplicing:
						// Evaluate the unquoted value, convert it to a sequence and splice it.
						var seqObj = env.Interpreter.Evaluate(list.Nth(1), env);
						var seq = Conversion.To<ISequence>(seqObj);
						result = Splice(seq.GetEntries());
						return true;
				}
			}

			result = null;
			return false;
		}
	}
}
