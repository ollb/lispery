﻿using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Rewrite
{
	/// <summary>
	/// Helper class for rewriting the syntax tree.
	/// </summary>
	/// <remarks>
	/// The order is depth-first top-down.
	/// </remarks>
	internal abstract class TreeRewriter
	{
		private record Spliced(IEnumerable<object?> Values);

		/// <summary>
		/// Gets the current recursion depth.
		/// </summary>
		protected int CurrentDepth { get; private set; }

		/// <summary>
		/// Creates a spliced value that can be returned from the handler functions.
		/// </summary>
		protected static object? Splice(IEnumerable<object?> values)
		{
			return new Spliced(values);
		}

		/// <summary>
		/// Creates a spliced value that can be returned from the handler functions.
		/// </summary>
		protected static object? Splice(params object?[] values)
		{
			return new Spliced(values);
		}

		/// <summary>
		/// Callback handler function for uncategorized values.
		/// </summary>
		protected virtual bool HandleValue(object? value, out object? result)
		{
			result = null;
			return false;
		}

		/// <summary>
		/// Callback handler function for symbols.
		/// </summary>
		protected virtual bool HandleSymbol(Symbol symbol, out object? result)
		{
			result = null;
			return false;
		}

		/// <summary>
		/// Callback handler function for list values.
		/// </summary>
		protected virtual bool HandleList(List list, out object? result)
		{
			result = null;
			return false;
		}

		/// <summary>
		/// Callback handler function for vector values.
		/// </summary>
		protected virtual bool HandleVector(Vector vector, out object? result)
		{
			result = null;
			return false;
		}

		/// <summary>
		/// Callback handler function for hash set values.
		/// </summary>
		protected virtual bool HandleHashSet(Set hashSet, out object? result)
		{
			result = null;
			return false;
		}

		/// <summary>
		/// Callback handler function for tree map values.
		/// </summary>
		protected virtual bool HandleTreeMap(Dictionary treeMap, out object? result)
		{
			result = null;
			return false;
		}

		/// <summary>
		/// Rewrites the given syntax tree.
		/// </summary>
		public object? Rewrite(object? value)
		{
			CurrentDepth = 0;
			var result = RewriteValue(value);
			ThrowIfSpliced(result);
			return result;
		}

		/// <summary>
		/// Throws an exception if the given value is spliced.
		/// </summary>
		private static void ThrowIfSpliced(object? value)
		{
			if (value is Spliced)
			{
				throw new InvalidOperationException("Spliced value not allowed here.");
			}
		}

		/// <summary>
		/// Rewrites the given syntax tree.
		/// </summary>
		protected object? RewriteValue(object? value)
		{
			switch (value)
			{
				case List list:     return RewriteValue(list);
				case Vector vector:       return RewriteValue(vector);
				case Set set:         return RewriteValue(set);
				case Dictionary map:         return RewriteValue(map);
				case ExpressionList list: return RewriteValue(list);
				case Symbol symbol:       return RewriteValue(symbol);

				default:
					if (HandleValue(value, out var newValue))
					{
						return newValue;
					}

					return value;
			};
		}

		/// <summary>
		/// Rewrites the given list.
		/// </summary>
		protected object? RewriteValue(List list)
		{
			CurrentDepth++;

			try
			{
				if (HandleList(list, out var result))
				{
					return result;
				}

				List ListRewrite(List list)
				{
					if (list.Count == 0)
					{
						return list;
					}

					// Rewrite the rest of the list.
					var newRest = ListRewrite(list.Rest());
					var value = list.First();

					switch (RewriteValue(value))
					{
						case Spliced spliced:
							foreach (var spliceValue in spliced.Values.Reverse())
							{
								newRest = newRest.Cons(spliceValue);
							}
							return newRest;

						case var result:
							return newRest.Cons(result);
					}
				}

				return ListRewrite(list);
			}
			finally
			{
				CurrentDepth--;
			}
		}

		/// <summary>
		/// Rewrites the given vector.
		/// </summary>
		protected object? RewriteValue(Vector vector)
		{
			CurrentDepth++;

			try
			{
				if (HandleVector(vector, out var result))
				{
					return result;
				}

				var builder = new Vector.Builder();

				foreach (var item in vector)
				{
					switch (RewriteValue(item))
					{
						case Spliced spliced:
							builder.AddRange(spliced.Values);
							break;

						case var newItem:
							builder.Add(newItem);
							break;
					}
				}

				return builder.ToImmutable();
			}
			finally
			{
				CurrentDepth--;
			}
		}

		/// <summary>
		/// Rewrites the given hash set.
		/// </summary>
		protected object? RewriteValue(Set set)
		{
			CurrentDepth++;

			try
			{
				if (HandleHashSet(set, out var result))
				{
					return result;
				}

				var builder = new Set.Builder();

				foreach (var item in set)
				{
					switch (RewriteValue(item))
					{
						case Spliced spliced:
							builder.AddRange(spliced.Values);
							break;

						case var newItem:
							builder.Add(newItem);
							break;
					}
				}

				return builder.ToImmutable();
			}
			finally
			{
				CurrentDepth--;
			}
		}

		/// <summary>
		/// Rewrites the given tree map.
		/// </summary>
		protected object? RewriteValue(Dictionary map)
		{
			CurrentDepth++;

			try
			{
				if (HandleTreeMap(map, out var result))
				{
					return result;
				}

				var builder = new Dictionary.Builder();

				foreach (var entry in map)
				{
					object? key = RewriteValue(entry.Key);
					object? value = RewriteValue(entry.Value);

					ThrowIfSpliced(key);
					ThrowIfSpliced(value);
					ArgumentNullException.ThrowIfNull(key);

					builder.Set(key, value);
				}

				return builder.ToImmutable();
			}
			finally
			{
				CurrentDepth--;
			}
		}

		/// <summary>
		/// Rewrites the given expression sequence.
		/// </summary>
		protected object? RewriteValue(ExpressionList list)
		{
			List<object?> newList = new();

			foreach (var item in list)
			{
				switch (RewriteValue(item))
				{
					case Spliced spliced:
						newList.AddRange(spliced.Values);
						break;

					case var result:
						newList.Add(result);
						break;
				}
			}

			return new ExpressionList(list);
		}

		/// <summary>
		/// Rewrites the given symbol.
		/// </summary>
		protected object? RewriteValue(Symbol symbol)
		{
			if (HandleSymbol(symbol, out var result))
			{
				return result;
			}

			return symbol;
		}
	}
}
