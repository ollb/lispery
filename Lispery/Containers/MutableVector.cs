﻿using Lispery.Interfaces;
using Lispery.Sequences;
using Lispery.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Containers
{
	/// <summary>
	/// Mutable random-access list container.
	/// </summary>
	public class MutableVector :
		IEnumerable<object?>,
		ISequence,
		ISupportGet,
		ISupportLast,
		ISupportAddMut,
		ISupportRemoveMut,
		ISupportInsertMut,
		ISupportRemoveAtMut,
		ISupportSetMut,
		ISupportReverseMut,
		ISupportSortMut,
		ICachedSequence,
		IEvaluable,
		IFormatable
	{
		private readonly List<object?> values;

		/// <summary>
		/// Creates a new vector containing the given items.
		/// </summary>
		public MutableVector(IEnumerable<object?> items)
		{
			values = new List<object?>(items);
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		public override string ToString()
		{
			IEnumerable<object?> list = this;
			string suffix = "";

			if (Count > 10)
			{
				list = this.Take(10);
				suffix = "...";
			}

			return $"![{string.Join(" ", list)}{suffix}]";
		}

		#region Convenience interface
		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		public int Count
		{
			get { return values.Count; }
		}
		#endregion

		#region IEvaluable implementation
		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			return new MutableVector(this.Select(o => env.Interpreter.Evaluate(o, env)));
		}
		#endregion

		#region IFormatable implementation
		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			formatter.FormatEnumerable(this, writer, "![", "]");
		}
		#endregion

		#region IEnumerable<object> implementation
		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		public IEnumerator<object?> GetEnumerator()
		{
			return values.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		[ExcludeFromCodeCoverage]
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		#endregion

		#region ISupport... implementations
		/// <summary>
		/// Returns an enumerator for all values in the sequence.
		/// </summary>
		public IEnumerable<object?> GetEntries()
		{
			return this;
		}

		/// <summary>
		/// Returns a list with all values except for the first.
		/// </summary>
		public ISequence Rest()
		{
			if (values.Count == 0)
			{
				return this;
			}

			return new SkipSequence<MutableVector>(this);
		}

		/// <summary>
		/// Gets the value with the given key. Throws if not found.
		/// </summary>
		public object? Get(object key)
		{
			return Nth(Conversion.To<int>(key));
		}

		/// <summary>
		/// Gets the value with the given key. Return <c>fallback</c> if not found.
		/// </summary>
		public object? Get(object key, object? fallback)
		{
			int index = Conversion.To<int>(key);

			if (index < 0 || index >= values.Count)
			{
				return fallback;
			}

			return Nth(index);
		}

		/// <summary>
		/// Returns the first value in the container.
		/// </summary>
		public object? First()
		{
			return Count == 0 ? null : values[0];
		}

		/// <summary>
		/// Returns the last value in the container.
		/// </summary>
		public object? Last()
		{
			return Count == 0 ? null : values[^1];
		}

		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		int ISupportCount.Count()
		{
			return values.Count;
		}

		/// <summary>
		/// Returns the n'th value in the container.
		/// </summary>
		public object? Nth(int index)
		{
			return values[index];
		}

		/// <summary>
		/// Adds a value to the data structure.
		/// </summary>
		public void AddMut(object? value)
		{
			values.Add(value);
		}

		/// <summary>
		/// Removes a value from the data structure.
		/// </summary>
		public void RemoveMut(object? value)
		{
			values.Remove(value);
		}

		/// <summary>
		/// Determines if the given value is stored in the data structure.
		/// </summary>
		public bool Contains(object? value)
		{
			return values.Contains(value);
		}

		/// <summary>
		/// Reverses the contents of the container.
		/// </summary>
		public void ReverseMut()
		{
			values.Reverse();
		}

		/// <summary>
		/// Inserts the given value at the given index.
		/// </summary>
		public void InsertMut(int index, object? value)
		{
			values.Insert(index, value);
		}

		/// <summary>
		/// Removes the value at the given index.
		/// </summary>
		public void RemoveAtMut(int index)
		{
			values.RemoveAt(index);
		}

		/// <summary>
		/// Associates the given key with the given value.
		/// </summary>
		public void SetMut(object index, object? value)
		{
			long indexValue = Conversion.To<long>(index);

			if (indexValue < 0 || indexValue >= values.Count)
			{
				throw new ArgumentOutOfRangeException(nameof(index));
			}

			values[(int)indexValue] = value;
		}

		/// <summary>
		/// Returns a list with N values skipped.
		/// </summary>
		public object Skip(long skip)
		{
			if (skip == 0 || values.Count == 0)
			{
				return this;
			}

			Check.IsNonNegativeInt(skip);

			return new SkipSequence<MutableVector>(this, (int)skip);
		}

		/// <summary>
		/// Sorts the contents of the container.
		/// </summary>
		public void SortMut(SortOrder order = SortOrder.Ascending)
		{
			switch (order)
			{
				case SortOrder.Descending:
					values.Sort((a, b) => -Comparer.Default.Compare(a, b));
					break;

				default:
					values.Sort();
					break;
			}
		}
		#endregion
	}
}
