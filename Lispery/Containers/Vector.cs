﻿using Lispery.Adapters;
using Lispery.Interfaces;
using Lispery.Sequences;
using Lispery.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Containers
{
	/// <summary>
	/// Random-access list container.
	/// </summary>
	public class Vector :
		IEnumerable<object?>,
		ISequence,
		ISupportLast,
		ISupportAdd,
		ISupportRemove,
		ISupportInsert,
		ISupportRemoveAt,
		ISupportSet,
		ISupportGet,
		ICachedSequence,
		IEvaluable,
		IFormatable
	{
		#region Builder implementation
		/// <summary>
		/// Builder object to help construct or modify vectors efficiently.
		/// </summary>
		public class Builder
		{
			private readonly ImmutableList<object?>.Builder builder;

			/// <summary>
			/// Creates a new empty builder.
			/// </summary>
			public Builder()
			{
				builder = ImmutableList.CreateBuilder<object?>();
			}

			/// <summary>
			/// Creates a builder for the given vector.
			/// </summary>
			internal Builder(Vector vector)
			{
				builder = vector.values.ToBuilder();
			}

			/// <summary>
			/// Removes all items from the list.
			/// </summary>
			public void Clear()
			{
				builder.Clear();
			}

			/// <summary>
			/// Adds an item at the end of the list.
			/// </summary>
			public void Add(object? obj)
			{
				builder.Add(obj);
			}

			/// <summary>
			/// Adds an item range at the end of the list.
			/// </summary>
			public void AddRange(IEnumerable<object?> range)
			{
				foreach (var item in range)
				{
					Add(item);
				}
			}

			/// <summary>
			/// Converts the builder to a vector.
			/// </summary>
			public Vector ToImmutable()
			{
				return new Vector(builder.ToImmutable());
			}
		}

		/// <summary>
		/// Creates a new builder for this vector.
		/// </summary>
		public Builder ToBuilder()
		{
			return new Builder(this);
		}
		#endregion

		/// <summary>
		/// The empty vector.
		/// </summary>
		public static readonly Vector Empty = new(ImmutableList<object?>.Empty);

		private readonly ImmutableList<object?> values;

		/// <summary>
		/// Creates a new vector containing the given items.
		/// </summary>
		public Vector(IEnumerable<object?> items)
		{
			var builder = ImmutableList.CreateBuilder<object?>();

			foreach (var item in items)
			{
				builder.Add(item);
			}

			values = builder.ToImmutable();
		}

		/// <summary>
		/// Creates a new vector from the given immutable list.
		/// </summary>
		private Vector(ImmutableList<object?> list)
		{
			this.values = list;
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		public override string ToString()
		{
			IEnumerable<object?> list = this;
			string suffix = "";

			if (Count > 10)
			{
				list = this.Take(10);
				suffix = "...";
			}

			return $"[{string.Join(" ", list)}{suffix}]";
		}

		#region Convenience interface
		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		public int Count
		{
			get { return values.Count; }
		}

		/// <summary>
		/// Returns the first value in the container.
		/// </summary>
		public object? First()
		{
			return Count == 0 ? null : values[0];
		}

		/// <summary>
		/// Returns the last value in the container.
		/// </summary>
		public object? Last()
		{
			return Count == 0 ? null : values[^1];
		}

		/// <summary>
		/// Returns the n'th value in the container.
		/// </summary>
		public object? this[int index]
		{
			get { return values[index]; }
		}

		/// <summary>
		/// Returns the n'th value in the container.
		/// </summary>
		public object? Nth(int index)
		{
			return values[index];
		}

		/// <summary>
		/// Adds a value to the data structure.
		/// </summary>
		public Vector Add(object? value)
		{
			return new Vector(values.Add(value));
		}

		/// <summary>
		/// Removes a value from the data structure.
		/// </summary>
		public Vector Remove(object? value)
		{
			return new Vector(values.Remove(value));
		}

		/// <summary>
		/// Determines if the given value is stored in the data structure.
		/// </summary>
		public bool Contains(object? value)
		{
			return values.Contains(value);
		}

		/// <summary>
		/// Reverses the contents of the container.
		/// </summary>
		public Vector Reverse()
		{
			return new Vector(values.Reverse());
		}

		/// <summary>
		/// Inserts the given value at the given index.
		/// </summary>
		public Vector Insert(int index, object? value)
		{
			return new Vector(values.Insert(index, value));
		}

		/// <summary>
		/// Removes the value at the given index.
		/// </summary>
		public Vector RemoveAt(int index)
		{
			return new Vector(values.RemoveAt(index));
		}
		#endregion

		#region IEvaluable implementation
		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			return new Vector(this.Select(o => env.Interpreter.Evaluate(o, env)));
		}
		#endregion

		#region IFormatable implementation
		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			formatter.FormatEnumerable(this, writer, "[", "]");
		}
		#endregion

		#region IEnumerable<T> implementation
		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		public IEnumerator<object?> GetEnumerator()
		{
			return values.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		[ExcludeFromCodeCoverage]
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		#endregion

		#region ISupport... implementations
		/// <summary>
		/// Returns an enumerator for all values in the sequence.
		/// </summary>
		public IEnumerable<object?> GetEntries()
		{
			return this;
		}

		/// <summary>
		/// Returns a list with all values except for the first.
		/// </summary>
		ISequence ISequence.Rest()
		{
			if (values.Count == 0)
			{
				return this;
			}

			return new SkipSequence<Vector>(this);
		}

		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		int ISupportCount.Count()
		{
			return values.Count;
		}

		/// <summary>
		/// Adds a value to the data structure.
		/// </summary>
		object? ISupportAdd.Add(object? value)
		{
			return Add(value);
		}

		/// <summary>
		/// Removes a value from the data structure.
		/// </summary>
		object? ISupportRemove.Remove(object? value)
		{
			return Remove(value);
		}

		/// <summary>
		/// Reverses the contents of the container.
		/// </summary>
		object? ISupportReverse.Reverse()
		{
			return Reverse();
		}

		/// <summary>
		/// Inserts the given value at the given index.
		/// </summary>
		object? ISupportInsert.Insert(int index, object? value)
		{
			return Insert(index, value);
		}

		/// <summary>
		/// Removes the value at the given index.
		/// </summary>
		object? ISupportRemoveAt.RemoveAt(int index)
		{
			return RemoveAt(index);
		}

		/// <summary>
		/// Associates the given key with the given value.
		/// </summary>
		object? ISupportSet.Set(object index, object? value)
		{
			long indexValue = Conversion.To<long>(index);

			if (indexValue < 0 || indexValue >= values.Count)
			{
				throw new ArgumentOutOfRangeException(nameof(index));
			}

			return new Vector(values.SetItem((int)indexValue, value));
		}

		/// <summary>
		/// Gets the value with the given key. Throws if not found.
		/// </summary>
		object? ISupportGet.Get(object key)
		{
			return Nth(Conversion.To<int>(key));
		}

		/// <summary>
		/// Gets the value with the given key. Return <c>fallback</c> if not found.
		/// </summary>
		object? ISupportGet.Get(object key, object? fallback)
		{
			int index = Conversion.To<int>(key);

			if (index < 0 || index >= values.Count)
			{
				return fallback;
			}

			return Nth(index);
		}

		/// <summary>
		/// Returns a list with N values skipped.
		/// </summary>
		object ISupportSkip.Skip(long skip)
		{
			if (skip == 0 || values.Count == 0)
			{
				return this;
			}

			Check.IsNonNegativeInt(skip);

			return new SkipSequence<Vector>(this, (int)skip);
		}
		#endregion
	}
}
