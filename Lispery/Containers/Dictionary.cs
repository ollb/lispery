﻿using Lispery.Adapters;
using Lispery.Interfaces;
using Lispery.Sequences;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Containers
{
	/// <summary>
	/// Associative container.
	/// </summary>
	public class Dictionary :
		IEnumerable<KeyValuePair<object, object?>>,
		ISequence,
		ISupportCount,
		ISupportContains,
		ISupportGet,
		ISupportSet,
		ISupportRemove,
		ISupportTryGetValue,
		ICachedSequence,
		IEvaluable,
		IFormatable
	{
		#region Builder implementation
		/// <summary>
		/// Builder for the creation of new tree maps.
		/// </summary>
		public class Builder
		{
			private readonly ImmutableDictionary<object, object?>.Builder builder;

			/// <summary>
			/// Creates a new, empty builder.
			/// </summary>
			public Builder()
			{
				builder = ImmutableDictionary.CreateBuilder<object, object?>();
			}

			/// <summary>
			/// Creates a builder for the given tree map.
			/// </summary>
			internal Builder(Dictionary treeMap)
			{
				builder = treeMap.values.ToBuilder();
			}

			/// <summary>
			/// Clears the builder.
			/// </summary>
			public void Clear()
			{
				builder.Clear();
			}

			/// <summary>
			/// Sets a new value in the tree.
			/// </summary>
			public void Set(object key, object? value)
			{
				ArgumentNullException.ThrowIfNull(key);
				builder[key] = value;
			}

			/// <summary>
			/// Constructs a tree map from the builder.
			/// </summary>
			public Dictionary ToImmutable()
			{
				return new Dictionary(builder.ToImmutable());
			}
		}

		/// <summary>
		/// Creates a new builder for this tree map.
		/// </summary>
		public Builder ToBuilder()
		{
			return new Builder(this);
		}
		#endregion

		/// <summary>
		/// The empty dictionary.
		/// </summary>
		public static readonly Dictionary Empty = new(ImmutableDictionary<object, object?>.Empty);

		private readonly ImmutableDictionary<object, object?> values;

		/// <summary>
		/// Creates a new dictionary with the given values.
		/// </summary>
		public Dictionary(IEnumerable<KeyValuePair<object, object?>> entries)
		{
			var builder = ImmutableDictionary.CreateBuilder<object, object?>();

			foreach (var entry in entries)
			{
				builder[entry.Key] = entry.Value;
			}

			this.values = builder.ToImmutable();
		}

		/// <summary>
		/// Creates a new dictionary.
		/// </summary>
		private Dictionary(ImmutableDictionary<object, object?> entries)
		{
			this.values = entries;
		}

		/// <summary>
		/// Returns a string representation of the object.
		/// </summary>
		public override string ToString()
		{
			IEnumerable<KeyValuePair<object, object?>> list = this;
			string suffix = "";

			if (Count > 5)
			{
				list = list.Take(5);
				suffix = "...";
			}

			return $"{{{string.Join(" ", list.SelectMany(e => new object?[] { e.Key, e.Value }))}{suffix}}}";
		}

		#region Convenience interface
		/// <summary>
		/// Returns the number of values stored in the tree map.
		/// </summary>
		public int Count
		{
			get { return values.Count; }
		}

		/// <summary>
		/// Tries to get a value from the dictionary.
		/// </summary>
		public bool TryGetValue(object? key, [MaybeNullWhen(false)] out object? value)
		{
			ArgumentNullException.ThrowIfNull(key);
			return values.TryGetValue(key, out value);
		}

		/// <summary>
		/// Associates the given key with the given value.
		/// </summary>
		public Dictionary Set(object key, object? value)
		{
			return new Dictionary(values.SetItem(key, value));
		}

		/// <summary>
		/// Removes the association for the given key.
		/// </summary>
		public Dictionary Remove(object key)
		{
			return new Dictionary(values.Remove(key));
		}
		#endregion

		#region IEvaluable implementation
		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			var interpreter = env.Interpreter;

			return new Dictionary(this.Select(e =>
				KeyValuePair.Create(
					interpreter.Evaluate(e.Key, env)!,
					interpreter.Evaluate(e.Value, env))));
		}
		#endregion

		#region IFormatable implementation
		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			formatter.FormatEnumerable(this, writer, "{", "}");
		}
		#endregion

		#region IEnumerable<T> implementation
		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		public IEnumerator<KeyValuePair<object, object?>> GetEnumerator()
		{
			return values.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		[ExcludeFromCodeCoverage]
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		#endregion

		#region ISupport... implementations
		/// <summary>
		/// Returns an enumerator for all values in the sequence.
		/// </summary>
		public IEnumerable<object?> GetEntries()
		{
			return this.Cast<object?>();
		}

		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		int ISupportCount.Count()
		{
			return values.Count;
		}

		/// <summary>
		/// Checks if the map contains a value for the given key.
		/// </summary>
		public bool Contains(object? key)
		{
			if (key == null)
			{
				return false;
			}

			return values.ContainsKey(key);
		}

		/// <summary>
		/// Gets a value from the map. Throws an exception when not found.
		/// </summary>
		public object? Get(object? key)
		{
			if (TryGetValue(key, out var result))
            {
				return result;
            }

			throw new KeyNotFoundException();
		}

		/// <summary>
		/// Gets a value from the map. Returns the fallback value when not found.
		/// </summary>
		public object? Get(object? key, object? fallback)
		{
			if (TryGetValue(key, out var result))
			{
				return result;
			}

			return fallback;
		}

		/// <summary>
		/// Associates the given key with the given value.
		/// </summary>
		object ISupportSet.Set(object key, object? value)
		{
			return Set(key, value);
		}

		/// <summary>
		/// Removes the association for the given key.
		/// </summary>
		object ISupportRemove.Remove(object? key)
		{
			ArgumentNullException.ThrowIfNull(key);
			return Remove(key);
		}
		#endregion
	}
}
