﻿using Lispery.Adapters;
using Lispery.Interfaces;
using Lispery.Sequences;
using Lispery.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Containers
{
	/// <summary>
	/// Implements an immutable singly-linked list.
	/// </summary>
	public class List :
		IEnumerable<object?>,
		ISequence,
		ICachedSequence,
		IEvaluable,
		IFormatable
	{
		private record Node(object? Value, Node? Rest);

		#region Builder implementation
		/// <summary>
		/// Builder object to help construct or modify lists efficiently.
		/// </summary>
		public class Builder
		{
			private Node? root;
			private int count;

			/// <summary>
			/// Creates a new empty builder.
			/// </summary>
			public Builder()
			{
				root = null;
				count = 0;
			}

			/// <summary>
			/// Creates a builder for the given list.
			/// </summary>
			internal Builder(List list)
			{
				root = Reverse(list.root);
				count = list.count;
			}

			/// <summary>
			/// Removes all items from the list.
			/// </summary>
			public void Clear()
			{
				root = null;
				count = 0;
			}

			/// <summary>
			/// Adds an item at the end of the list.
			/// </summary>
			public void Add(object? obj)
			{
				root = new Node(obj, root);
				count++;
			}

			/// <summary>
			/// Adds an item range at the end of the list.
			/// </summary>
			public void AddRange(IEnumerable<object?> range)
			{
				foreach (var item in range)
				{
					Add(item);
				}
			}

			/// <summary>
			/// Converts the builder to a list.
			/// </summary>
			public List ToImmutable()
			{
				return new List(Reverse(root), count);
			}
		}

		/// <summary>
		/// Creates a new builder for this list.
		/// </summary>
		public Builder ToBuilder()
		{
			return new Builder(this);
		}
		#endregion

		/// <summary>
		/// The empty linked list.
		/// </summary>
		public static readonly List Empty = new(null, 0);

		private readonly Node? root;
		private readonly int count;

		/// <summary>
		/// Creates a new linked list containing the given items.
		/// </summary>
		public List(IEnumerable<object?> items)
		{
			root = null;
			count = 0;

			foreach (var item in items)
			{
				root = new Node(item, root);
				count++;
			}

			root = Reverse(root);
		}

		/// <summary>
		/// Creates a new linked list containing the given items.
		/// </summary>
		public List(object?[] items)
		{
			root = null;
			count = items.Length;

			for (int i = count - 1; i >= 0; i--)
			{
				root = new Node(items[i], root);
			}
		}

		/// <summary>
		/// Creates a new vector from the given immutable stack and count.
		/// </summary>
		private List(Node? values, int count)
		{
			this.root = values;
			this.count = count;
		}

		/// <summary>
		/// Converts a sequence into a linked list.
		/// </summary>
		public static List FromSequence(ISequence seq)
		{
			if (seq is List list)
			{
				return list;
			}

			return new List(seq.GetEntries());
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			IEnumerable<object?> list = this;
			string suffix = "";

			if (Count > 10)
			{
				list = this.Take(10);
				suffix = "...";
			}

			return $"({string.Join(" ", list)}{suffix})";
		}

		#region Convenience interface
		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		public int Count
		{
			get { return count; }
		}

		/// <summary>
		/// Efficient recursive map.
		/// </summary>
		public List MapAsList(Func<object?, object?> mapFun)
		{
			if (count < 100)
			{
				// Fast but limited by stack space.
				Node? Rewrite(Node? node)
				{
					if (node is null)
					{
						return null;
					}

					var newRest = Rewrite(node.Rest);
					return new Node(mapFun(node.Value), newRest);
				}

				return new List(Rewrite(root), count);
			}
			else
			{
				// Slower but not limited by stack.
				Node? root = null;

				foreach (var item in this)
				{
					root = new Node(mapFun(item), root);
				}

				return new List(Reverse(root), count);
			}
		}

		/// <summary>
		/// Adds a value at the start of the list.
		/// </summary>
		public List Cons(object? value)
		{
			return new List(new Node(value, root), count + 1);
		}

		/// <summary>
		/// Returns a list with all values except for the first.
		/// </summary>
		public List Rest()
		{
			if (count == 0)
			{
				return List.Empty;
			}

			return new List(root!.Rest, count - 1);
		}

		/// <summary>
		/// Determines if the given value is stored in the data structure.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public bool Contains(object? value)
		{
			return ((ISupportContains)this).Contains(value);
		}

		/// <summary>
		/// Reverses the contents of the container.
		/// </summary>
		public List Reverse()
		{
			return new List(Reverse(root), count);
		}
		#endregion

		#region Helper methods
		/// <summary>
		/// Helper function that reverses an immutable stack.
		/// </summary>
		private static Node? Reverse(Node? node)
		{
			Node? result = null;

			while (node is not null)
			{
				result = new Node(node.Value, result);
				node = node.Rest;
			}

			return result;
		}
		#endregion

		#region IEvaluable implementation
		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			return env.Interpreter.EvaluateListContinue(this, env);
		}
		#endregion

		#region IFormatable implementation
		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			formatter.FormatEnumerable(this, writer, "(", ")");
		}
		#endregion

		#region IEnumerable<T> implementation
		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		public IEnumerator<object?> GetEnumerator()
		{
			Node? current = root;

			while (current is not null)
			{
				yield return current.Value;
				current = current.Rest;
			}
		}

		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		[ExcludeFromCodeCoverage]
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		#endregion

		#region ISupport... implementations
		/// <summary>
		/// Returns an enumerator for all values in the sequence.
		/// </summary>
		public IEnumerable<object?> GetEntries()
		{
			return this;
		}

		/// <summary>
		/// Adds a value at the start of the list.
		/// </summary>
		object ISupportCons.Cons(object? value)
		{
			return Cons(value);
		}

		/// <summary>
		/// Returns the first value in the container.
		/// </summary>
		public object? First()
		{
			return root?.Value;
		}

		/// <summary>
		/// Returns a list with all values except for the first.
		/// </summary>
		ISequence ISequence.Rest()
		{
			return Rest();
		}

		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		int ISupportCount.Count()
		{
			return count;
		}

		/// <summary>
		/// Returns the n'th value in the container.
		/// </summary>
		public object? Nth(int index)
		{
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(index));
			}

			Node? current = root;

			while (current is not null && index > 0)
			{
				current = current.Rest;
				index--;
			}

			if (current is null)
			{
				throw new ArgumentOutOfRangeException(nameof(index));
			}

			return current.Value;
		}

		/// <summary>
		/// Reverses the contents of the container.
		/// </summary>
		object ISupportReverse.Reverse()
		{
			return Reverse();
		}

		/// <summary>
		/// Returns a list with N values skipped.
		/// </summary>
		object ISupportSkip.Skip(long skip)
		{
			if (skip == 0 || count == 0)
			{
				return this;
			}

			Check.IsNonNegativeInt(skip);

			var result = root;

			for (long i = 0; i < skip; i++)
			{
				if (result == null)
				{
					break;
				}

				result = result.Rest;
			}

			return new List(result, count - (int)skip);
		}
		#endregion

		#region Match methods
		/// <summary>
		/// Returns the first object in the list.
		/// Throws an exception if the list is too short.
		/// </summary>
		public object? Match1()
		{
			if (count < 1)
			{
				throw new InvalidOperationException("Can not match elements");
			}

			var a = root;
			return a!.Value;
		}

		/// <summary>
		/// Returns the first object in the list and the remainder list.
		/// Throws an exception if the list is too short.
		/// </summary>
		public (object?, List) Match1R()
		{
			if (count < 1)
			{
				throw new InvalidOperationException("Can not match elements");
			}

			var a = root;
			var b = a!.Rest;
			return (a!.Value, new List(b, count - 1));
		}

		/// <summary>
		/// Returns the first two objects in the list.
		/// Throws an exception if the list is too short.
		/// </summary>
		public (object?, object?) Match2()
		{
			if (count < 2)
			{
				throw new InvalidOperationException("Can not match elements");
			}

			var a = root;
			var b = a!.Rest;
			return (a!.Value, b!.Value);
		}

		/// <summary>
		/// Returns the first two objects in the list and the remainder list.
		/// Throws an exception if the list is too short.
		/// </summary>
		public (object?, object?, List) Match2R()
		{
			if (count < 2)
			{
				throw new InvalidOperationException("Can not match elements");
			}

			var a = root;
			var b = a!.Rest;
			var c = b!.Rest;
			return (a!.Value, b!.Value, new List(c, count - 2));
		}

		/// <summary>
		/// Returns the first three objects in the list.
		/// Throws an exception if the list is too short.
		/// </summary>
		public (object?, object?, object?) Match3()
		{
			if (count < 3)
			{
				throw new InvalidOperationException("Can not match elements");
			}

			var a = root;
			var b = a!.Rest;
			var c = b!.Rest;
			return (a!.Value, b!.Value, c!.Value);
		}

		/// <summary>
		/// Returns the first three objects in the list and the remainder list.
		/// Throws an exception if the list is too short.
		/// </summary>
		public (object?, object?, object?, List) Match3R()
		{
			if (count < 3)
			{
				throw new InvalidOperationException("Can not match elements");
			}

			var a = root;
			var b = a!.Rest;
			var c = b!.Rest;
			var d = c!.Rest;
			return (a!.Value, b!.Value, c!.Value, new List(d, count - 3));
		}

		/// <summary>
		/// Returns the first four objects in the list.
		/// Throws an exception if the list is too short.
		/// </summary>
		public (object?, object?, object?, object?) Match4()
		{
			if (count < 4)
			{
				throw new InvalidOperationException("Can not match elements");
			}

			var a = root;
			var b = a!.Rest;
			var c = b!.Rest;
			var d = c!.Rest;
			return (a!.Value, b!.Value, c!.Value, d!.Value);
		}

		/// <summary>
		/// Returns the first four objects in the list and the remainder list.
		/// Throws an exception if the list is too short.
		/// </summary>
		public (object?, object?, object?, object?, List) Match4R()
		{
			if (count < 4)
			{
				throw new InvalidOperationException("Can not match elements");
			}

			var a = root;
			var b = a!.Rest;
			var c = b!.Rest;
			var d = c!.Rest;
			var e = d!.Rest;
			return (a!.Value, b!.Value, c!.Value, d!.Value, new List(e, count - 4));
		}
		#endregion
	}
}
