﻿using Lispery.Interfaces;
using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Containers
{
	/// <summary>
	/// Mutable unique value set container.
	/// </summary>
	public class MutableSet :
		IEnumerable<object?>,
		ISequence,
		ISupportAddMut,
		ISupportRemoveMut,
		ICachedSequence,
		IEvaluable,
		IFormatable
	{
		private readonly HashSet<object?> values;

		/// <summary>
		/// Creates a new set with the given values.
		/// </summary>
		public MutableSet(IEnumerable<object?> values)
		{
			this.values = new HashSet<object?>(values);
		}

		/// <summary>
		/// Returns a string representation of the object.
		/// </summary>
		public override string ToString()
		{
			IEnumerable<object?> list = this;
			string suffix = "";

			if (Count > 10)
			{
				list = list.Take(10);
				suffix = "...";
			}

			return $"!#{{{string.Join(" ", list)}{suffix}}}";
		}

		#region Convenience interface
		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		public int Count
		{
			get { return values.Count; }
		}
		#endregion

		#region IEvaluable implementation
		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			return new MutableSet(this.Select(o => env.Interpreter.Evaluate(o, env)));
		}
		#endregion

		#region IFormatable implementation
		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			formatter.FormatEnumerable(this, writer, "!#{", "}");
		}
		#endregion

		#region IEnumerable<T> implementation
		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		public IEnumerator<object?> GetEnumerator()
		{
			return values.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		[ExcludeFromCodeCoverage]
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		#endregion

		#region ISupport... implementations
		/// <summary>
		/// Returns an enumerator for all values in the sequence.
		/// </summary>
		public IEnumerable<object?> GetEntries()
		{
			return this;
		}

		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		int ISupportCount.Count()
		{
			return values.Count;
		}

		/// <summary>
		/// Adds a value to the data structure.
		/// </summary>
		public void AddMut(object? value)
		{
			values.Add(value);
		}

		/// <summary>
		/// Removes a value from the data structure.
		/// </summary>
		public void RemoveMut(object? value)
		{
			values.Remove(value);
		}
		#endregion
	}
}
