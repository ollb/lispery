﻿using Lispery.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Containers
{
	/// <summary>
	/// Mutable associative container.
	/// </summary>
	internal class MutableDictionary :
		IEnumerable<KeyValuePair<object, object?>>,
		ISequence,
		ISupportCount,
		ISupportContains,
		ISupportGet,
		ISupportSetMut,
		ISupportRemoveMut,
		ISupportTryGetValue,
		ICachedSequence,
		IEvaluable,
		IFormatable
	{
		private readonly Dictionary<object, object?> values;

		/// <summary>
		/// Creates a new dictionary with the given values.
		/// </summary>
		public MutableDictionary()
		{
			values = new Dictionary<object, object?>();
		}

		/// <summary>
		/// Creates a new dictionary with the given values.
		/// </summary>
		public MutableDictionary(IEnumerable<KeyValuePair<object, object?>> entries)
		{
			values = new Dictionary<object, object?>(entries);
		}

		/// <summary>
		/// Returns a string representation of the object.
		/// </summary>
		public override string ToString()
		{
			IEnumerable<KeyValuePair<object, object?>> list = this;
			string suffix = "";

			if (Count > 5)
			{
				list = list.Take(5);
				suffix = "...";
			}

			return $"!{{{string.Join(" ", list.SelectMany(e => new object?[] { e.Key, e.Value }))}{suffix}}}";
		}

		#region Convenience interface
		/// <summary>
		/// Returns the number of values stored in the tree map.
		/// </summary>
		public int Count
		{
			get { return values.Count; }
		}
		#endregion

		#region IEvaluable implementation
		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			var interpreter = env.Interpreter;

			return new MutableDictionary(this.Select(e =>
				KeyValuePair.Create(
					interpreter.Evaluate(e.Key, env)!,
					interpreter.Evaluate(e.Value, env))));
		}
		#endregion

		#region IFormatable implementation
		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			formatter.FormatEnumerable(this, writer, "!{", "}");
		}
		#endregion

		#region IEnumerable<T> implementation
		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		public IEnumerator<KeyValuePair<object, object?>> GetEnumerator()
		{
			return values.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		[ExcludeFromCodeCoverage]
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		#endregion

		#region ISupport... implementations
		/// <summary>
		/// Returns an enumerator for all values in the sequence.
		/// </summary>
		public IEnumerable<object?> GetEntries()
		{
			return this.Cast<object?>();
		}

		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		int ISupportCount.Count()
		{
			return values.Count;
		}

		/// <summary>
		/// Checks if the map contains a value for the given key.
		/// </summary>
		public bool Contains(object? key)
		{
			if (key == null)
			{
				return false;
			}

			return values.ContainsKey(key);
		}

		/// <summary>
		/// Gets a value from the map. Throws an exception when not found.
		/// </summary>
		public object? Get(object? key)
		{
			ArgumentNullException.ThrowIfNull(key);

			if (values.TryGetValue(key, out var result))
			{
				return result;
			}

			return null;
		}

		/// <summary>
		/// Gets a value from the map. Returns the fallback value when not found.
		/// </summary>
		public object? Get(object? key, object? fallback)
		{
			ArgumentNullException.ThrowIfNull(key);

			if (values.TryGetValue(key, out var result))
			{
				return result;
			}

			return fallback;
		}

		/// <summary>
		/// Tries to get a value from the dictionary.
		/// </summary>
		public bool TryGetValue(object key, [MaybeNullWhen(false)] out object? value)
		{
			return values.TryGetValue(key, out value);
		}

		/// <summary>
		/// Associates the given key with the given value.
		/// </summary>
		public void SetMut(object key, object? value)
		{
			values[key] = value;
		}

		/// <summary>
		/// Removes the association for the given key.
		/// </summary>
		public void RemoveMut(object? key)
		{
			ArgumentNullException.ThrowIfNull(key);
			values.Remove(key);
		}
		#endregion
	}
}
