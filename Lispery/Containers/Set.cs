﻿using Lispery.Adapters;
using Lispery.Interfaces;
using Lispery.Sequences;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Containers
{
	/// <summary>
	/// Unique value set container.
	/// </summary>
	public class Set :
		IEnumerable<object?>,
		ISequence,
		ISupportAdd,
		ISupportRemove,
		ICachedSequence,
		IEvaluable,
		IFormatable
	{
		#region Builder implementation
		/// <summary>
		/// Builder for the creation of new hash sets.
		/// </summary>
		public class Builder
		{
			private readonly ImmutableHashSet<object?>.Builder builder;

			/// <summary>
			/// Creates a new, empty builder.
			/// </summary>
			public Builder()
			{
				builder = ImmutableHashSet.CreateBuilder<object?>();
			}

			/// <summary>
			/// Creates a builder for the given hash set.
			/// </summary>
			internal Builder(Set hashSet)
			{
				builder = hashSet.values.ToBuilder();
			}

			/// <summary>
			/// Clears the builder.
			/// </summary>
			public void Clear()
			{
				builder.Clear();
			}

			/// <summary>
			/// Adds a new value to the set.
			/// </summary>
			public void Add(object? value)
			{
				builder.Add(value);
			}

			/// <summary>
			/// Adds a range of new values to the set.
			/// </summary>
			public void AddRange(IEnumerable<object?> range)
			{
				foreach (var item in range)
				{
					Add(item);
				}
			}

			/// <summary>
			/// Constructs a hash set from the builder.
			/// </summary>
			public Set ToImmutable()
			{
				return new Set(builder.ToImmutable());
			}
		}

		/// <summary>
		/// Creates a new builder for this hash set.
		/// </summary>
		public Builder ToBuilder()
		{
			return new Builder(this);
		}
		#endregion

		/// <summary>
		/// The empty set.
		/// </summary>
		public static readonly Set Empty = new(ImmutableHashSet<object?>.Empty);

		private readonly ImmutableHashSet<object?> values;

		/// <summary>
		/// Creates a new set with the given values.
		/// </summary>
		public Set(IEnumerable<object?> values)
		{
			var builder = ImmutableHashSet.CreateBuilder<object?>();

			foreach (var entry in values)
			{
				builder.Add(entry);
			}

			this.values = builder.ToImmutable();
		}

		/// <summary>
		/// Creates a new set.
		/// </summary>
		private Set(ImmutableHashSet<object?> entries)
		{
			this.values = entries;
		}

		/// <summary>
		/// Returns a string representation of the object.
		/// </summary>
		public override string ToString()
		{
			IEnumerable<object?> list = this;
			string suffix = "";

			if (Count > 10)
			{
				list = list.Take(10);
				suffix = "...";
			}

			return $"#{{{string.Join(" ", list)}{suffix}}}";
		}

		#region Convenience interface
		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		public int Count
		{
			get { return values.Count; }
		}

		/// <summary>
		/// Adds a value to the data structure.
		/// </summary>
		public Set Add(object? value)
		{
			return new Set(values.Add(value));
		}

		/// <summary>
		/// Removes a value from the data structure.
		/// </summary>
		public Set Remove(object? value)
		{
			return new Set(values.Remove(value));
		}

		/// <summary>
		/// Determines if the given value is stored in the data structure.
		/// </summary>
		public bool Contains(object? value)
		{
			return values.Contains(value);
		}
		#endregion

		#region IEvaluable implementation
		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			return new Set(this.Select(o => env.Interpreter.Evaluate(o, env)));
		}
		#endregion

		#region IFormatable implementation
		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			formatter.FormatEnumerable(this, writer, "#{", "}");
		}
		#endregion

		#region IEnumerable<T> implementation
		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		public IEnumerator<object?> GetEnumerator()
		{
			return values.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator for the stored values.
		/// </summary>
		[ExcludeFromCodeCoverage]
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		#endregion

		#region ISupport... implementations
		/// <summary>
		/// Returns an enumerator for all values in the sequence.
		/// </summary>
		public IEnumerable<object?> GetEntries()
		{
			return this;
		}

		/// <summary>
		/// Returns the number of values stored in the hash set.
		/// </summary>
		int ISupportCount.Count()
		{
			return values.Count;
		}

		/// <summary>
		/// Adds a value to the data structure.
		/// </summary>
		object ISupportAdd.Add(object? value)
		{
			return Add(value);
		}

		/// <summary>
		/// Removes a value from the data structure.
		/// </summary>
		object ISupportRemove.Remove(object? value)
		{
			return Remove(value);
		}
		#endregion
	}
}
