﻿using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	/// <summary>
	/// Stores and manages the symbols known to an interpreter.
	/// </summary>
	public class SymbolTable
	{
		private readonly Dictionary<string, Symbol> symbolMap = new();
		private readonly List<Symbol> symbols = new();

		public Symbol SymFn { get; init; }
		public Symbol SymUnquote { get; init; }
		public Symbol SymUnquoteSplicing { get; init; }
		public Symbol SymAmpersand { get; init; }
		public Symbol SymSeq { get; init; }
		public Symbol SymInt { get; init; }
		public Symbol SymFloat { get; init; }
		public Symbol SymString { get; init; }
		public Symbol SymBool { get; init; }
		public Symbol SymVector { get; init; }
		public Symbol SymVectorMut { get; init; }
		public Symbol SymList { get; init; }
		public Symbol SymDict { get; init; }
		public Symbol SymDictMut { get; init; }
		public Symbol SymGtDict { get; init; }
		public Symbol SymKeyDict { get; init; }
		public Symbol SymGtKeyDict { get; init; }
		public Symbol SymEquals { get; init; }
		public Symbol SymQuote { get; init; }
		public Symbol SymWhen { get; init; }
		public Symbol SymLet { get; init; }

		/// <summary>
		/// Creates a new symbol table.
		/// </summary>
		public SymbolTable()
		{
			// Initialize pre-defined symbols.
			SymFn = Get("fn");
			SymUnquote = Get("unquote");
			SymUnquoteSplicing = Get("unquote-splicing");
			SymAmpersand = Get("&");
			SymSeq = Get("seq");
			SymInt = Get("int");
			SymFloat = Get("float");
			SymString = Get("string");
			SymBool = Get("bool");
			SymVector = Get("vector");
			SymVectorMut = Get("vector!");
			SymList = Get("list");
			SymDict = Get("dict");
			SymDictMut = Get("dict!");
			SymGtDict = Get(">dict");
			SymKeyDict = Get("key-dict");
			SymGtKeyDict = Get(">key-dict");
			SymEquals = Get("=");
			SymQuote = Get("quote");
			SymWhen = Get("when");
			SymLet = Get("let");
		}

		/// <summary>
		/// Returs the symbol for the given name.
		/// </summary>
		public Symbol Get(string name)
		{
			ArgumentNullException.ThrowIfNull(name);

			if (!symbolMap.TryGetValue(name, out Symbol? symbol))
			{
				if (name.StartsWith("__gen_"))
				{
					// Make sure no-one defines reserved symbols,
					// in order to guarantee uniqueness.
					throw new ArgumentException("Reserved symbol name");
				}

				symbol = new Symbol(symbols.Count, name);
				symbols.Add(symbol);
				symbolMap[name] = symbol;
			}

			return symbol;
		}

		/// <summary>
		/// Generates a new unique symbol.
		/// </summary>
		public Symbol GenSym(string? context = null)
		{
			StringBuilder sb = new();
			sb.Append("__gen_");

			if (context != null)
			{
				sb.Append(context);
				sb.Append('_');
			}

			sb.Append(symbols.Count);

			var name = sb.ToString();
			var symbol = new Symbol(symbols.Count, name);

			Debug.Assert(!symbolMap.ContainsKey(name));
			symbols.Add(symbol);
			symbolMap[name] = symbol;
			return symbol;
		}
	}
}
