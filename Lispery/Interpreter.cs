﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Rewrite;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	/// <summary>
	/// Interprets and runs source code.
	/// </summary>
	public class Interpreter
	{
		[SuppressMessage("CodeQuality", "IDE0052")]
		private readonly StdLib stdLib;
		private readonly Environment env;

		private readonly Dictionary<object, object?> macroCache = new(ReferenceEqualityComparer.Instance);
		private long interruptFrequency = 10000;

		/// <summary>
		/// Event produced when the interpreter tries to print data.
		/// </summary>
		public event EventHandler<PrintEventArgs>? Print;

		/// <summary>
		/// Regular interrupt event. Can be used to abort the program.
		/// </summary>
		public event EventHandler<InterruptEventArgs>? Interrupt;

		/// <summary>
		/// Creates a new interpreter.
		/// </summary>
		public Interpreter()
		{
			SymbolTable = new SymbolTable();
			KeywordTable = new KeywordTable();
			env = new Environment(this);
			stdLib = new StdLib(env);
		}

		/// <summary>
		/// Returns the global symbol table for the interpreter.
		/// </summary>
		public SymbolTable SymbolTable { get; init; }

		/// <summary>
		/// Returns the global keyword table for the interpreter.
		/// </summary>
		public KeywordTable KeywordTable { get; init; }

		/// <summary>
		/// Returns the global environment of the interpreter.
		/// </summary>
		public Environment Environment
		{
			get { return env; }
		}

		/// <summary>
		/// Parses the given string and returns an object structure.
		/// </summary>
		public ExpressionList Parse(string source)
		{
			using StringReader reader = new(source);
			return Parse(reader);
		}

		/// <summary>
		/// Parses source from the given reader and returns an object structure.
		/// </summary>
		public ExpressionList Parse(TextReader reader)
		{
			Parser parser = new(SymbolTable, KeywordTable);
			return parser.Parse(reader);
		}

		/// <summary>
		/// Returns the number of steps of the last evaluation.
		/// </summary>
		public long EvaluationSteps { get; private set; }

		/// <summary>
		/// Returns the execution time of the last evaluation.
		/// </summary>
		public TimeSpan EvaluationTime { get; private set; }

		/// <summary>
		/// Returns the current limit for evaluated steps.
		/// </summary>
		/// <remarks>
		/// Set to 0 to disable the limit.
		/// </remarks>
		public long EvaluationStepLimit { get; set; }

		/// <summary>
		/// Specifies how often the interrupt event is generated (in steps).
		/// </summary>
		public long InterruptFrequency
		{
			get { return interruptFrequency; }

			set
			{
				if (value < 1)
				{
					throw new ArgumentException("Interrupt frequency cant be <= 0.");
				}

				interruptFrequency = value;
			}
		}

		/// <summary>
		/// The size of the execution thread stack in bytes.
		/// </summary>
		public int ThreadStackSize { get; set; } = 10 * 1024 * 1024;

		/// <summary>
		/// Prints an output value.
		/// </summary>
		public void PrintOutput(string output)
		{
			if (Print != null)
			{
				Print.Invoke(this, new PrintEventArgs(output));
			}
			else
			{
				Console.WriteLine(output);
			}
		}

		/// <summary>
		/// Prints an output value.
		/// </summary>
		public void PrintOutput(string format, params object?[] args)
		{
			PrintOutput(String.Format(format, args));
		}

		/// <summary>
		/// Evaluates the given string and returns a result object.
		/// </summary>
		public object? Evaluate(string source)
		{
			return RunThread(() => Evaluate(Parse(source)));
		}

		/// <summary>
		/// Evaluates source from the given reader and returns a result object.
		/// </summary>
		public object? Evaluate(TextReader reader)
		{
			return Evaluate(Parse(reader));
		}

		/// <summary>
		/// Evaluates the result from <c>Parse</c> and returns a result object.
		/// </summary>
		public object? Evaluate(ExpressionList list)
		{
			Stopwatch sw = new();
			sw.Start();

			macroCache.Clear();
			EvaluationTime = TimeSpan.Zero;
			EvaluationSteps = 0;

			try
			{
				return Evaluate(list, env);
			}
			finally
			{
				EvaluationTime = sw.Elapsed;
			}
		}

		/// <summary>
		/// Evaluates the given expression and returns a result object.
		/// </summary>
		/// <remarks>
		/// This should be used if the caller needs to evaluate the result value
		/// (aka not in tail position).
		/// </remarks>
		internal object? Evaluate(object? expr, Environment env)
		{
			while (true)
			{
				switch (EvaluateContinue(expr, env))
				{
					case Continuation cont:
						// Executing a continuation will just loop again. If the new
						// expression results in another continuation, it will continue
						// to iterate, until we get a non-continuation.
						expr = cont;
						break;

					case var result:
						return result;
				}
			}
		}

		/// <summary>
		/// Evaluates the given expression and returns a result object or continuation.
		/// </summary>
		/// <remarks>
		/// This should be used if the caller is not interested in the result and
		/// only passes it up the stack (aka in tail position).
		/// </remarks>
		internal object? EvaluateContinue(object? expr, Environment env)
		{
			EvaluationSteps++;

			if (EvaluationSteps % interruptFrequency == 0)
			{
				if (EvaluationSteps >= EvaluationStepLimit && EvaluationStepLimit > 0)
				{
					throw new InvalidOperationException("Evaluation limit exceeded");
				}

				var args = new InterruptEventArgs(EvaluationSteps);
				Interrupt?.Invoke(this, args);
			}

			if (expr is IEvaluable eval)
			{
				return eval.EvaluateContinue(env);
			}

			return expr;
		}

		/// <summary>
		/// Evaluates a <c>LinkedList</c> and returns a result object or continuation.
		/// </summary>
		internal object? EvaluateListContinue(List list, Environment env)
		{
			if (list.Count == 0)
			{
				return list;
			}

			var callable = Conversion.To<ICallable>(Evaluate(list.First(), env));
			return callable.CallContinue(list.Rest(), env, list);
		}

		/// <summary>
		/// Evaluates a macro.
		/// </summary>
		internal object? EvaluateMacro(Macro macro, List args, Environment env, object? context)
		{
			// Without context, don't do any caching.
			if (context == null)
			{
				return EvaluateContinue(macro.Expand(args), env);
			}

			// First try to see if we have the expansion in the cache.
			if (!macroCache.TryGetValue(context, out var result))
			{
				// Expand the macro and store the expansion result.
				result = macro.Expand(args);
				macroCache[context] = result;
			}

			// Evaluate the macro result.
			return EvaluateContinue(result, env);
		}

		/// <summary>
		/// Runs the given action in the interpreter thread.
		/// </summary>
		private object? RunThread(Func<object?> action)
		{
			if (ThreadStackSize > 0)
			{
				object? result = null;
				Exception? exception = null;

				Thread thread = new(() =>
				{
					try
					{
						result = action();
					}
					catch (Exception ex)
					{
						exception = ex;
					}
				}, ThreadStackSize);

				thread.Start();
				thread.Join();

				if (exception != null)
				{
					throw exception;
				}

				return result;
			}

			return action();
		}
	}
}
