﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the nth function.
	/// </summary>
	public interface ISupportNth
	{
		/// <summary>
		/// Returns the n'th value in the container.
		/// </summary>
		object? Nth(int index);
	}
}
