﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the mutable remove-at function.
	/// </summary>
	public interface ISupportRemoveAtMut
	{
		/// <summary>
		/// Removes the value at the given index.
		/// </summary>
		void RemoveAtMut(int index);
	}
}
