﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the first function.
	/// </summary>
	public interface ISupportFirst
	{
		/// <summary>
		/// Returns the first value in the container.
		/// </summary>
		object? First();
	}
}
