﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the mutable add function.
	/// </summary>
	public interface ISupportAddMut
	{
		/// <summary>
		/// Adds a value to the data structure.
		/// </summary>
		void AddMut(object? value);
	}
}
