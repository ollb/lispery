﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	public interface ISupportTryGetValue
	{
		/// <summary>
		/// Tries to get a value from the dictionary.
		/// </summary>
		bool TryGetValue(object key, [MaybeNullWhen(false)] out object? value);
	}
}
