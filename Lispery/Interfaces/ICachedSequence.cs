﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Marks a sequence as cached.
	/// </summary>
	public interface ICachedSequence
	{
	}
}
