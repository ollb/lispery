﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the insert function.
	/// </summary>
	public interface ISupportInsert
	{
		/// <summary>
		/// Inserts the given value at the given index.
		/// </summary>
		object? Insert(int index, object? value);
	}
}
