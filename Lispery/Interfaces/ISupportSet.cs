﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the set function.
	/// </summary>
	public interface ISupportSet
	{
		/// <summary>
		/// Associates the given key with the given value.
		/// </summary>
		object? Set(object key, object? value);
	}
}
