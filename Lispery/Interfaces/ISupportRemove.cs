﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the remove function.
	/// </summary>
	public interface ISupportRemove
	{
		/// <summary>
		/// Removes a value from the data structure.
		/// </summary>
		object? Remove(object? value);
	}
}
