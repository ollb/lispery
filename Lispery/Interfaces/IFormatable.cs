﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	public interface IFormatable
	{
		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		void Format(TextWriter writer, Formatter formatter);
	}
}
