﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the reverse function.
	/// </summary>
	public interface ISupportReverse
	{
		/// <summary>
		/// Reverses the contents of the container.
		/// </summary>
		object? Reverse();
	}
}
