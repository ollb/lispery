﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the rest function.
	/// </summary>
	public interface ISupportRest
	{
		/// <summary>
		/// Returns a list with all values except for the first.
		/// </summary>
		object Rest();
	}
}
