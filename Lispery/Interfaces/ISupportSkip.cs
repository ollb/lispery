﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the skip function.
	/// </summary>
	public interface ISupportSkip
	{
		/// <summary>
		/// Returns a list with N values skipped.
		/// </summary>
		object Skip(long skip);
	}
}
