﻿using Lispery.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Interface for objects that are callable.
	/// </summary>
	public interface ICallable
	{
		/// <summary>
		/// Returns the name of the callable or <c>null</c> if unknown.
		/// </summary>
		string? Name { get; }

		/// <summary>
		/// Calls the object in a given context.
		/// </summary>
		object? CallContinue(List args, Environment env, object? context);
	}
}
