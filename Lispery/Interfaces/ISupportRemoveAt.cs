﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the remove-at function.
	/// </summary>
	public interface ISupportRemoveAt
	{
		/// <summary>
		/// Removes the value at the given index.
		/// </summary>
		object? RemoveAt(int index);
	}
}
