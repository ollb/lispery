﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the get function.
	/// </summary>
	public interface ISupportGet
	{
		/// <summary>
		/// Gets the value with the given key. Throws if not found.
		/// </summary>
		object? Get(object key);

		/// <summary>
		/// Gets the value with the given key. Return <c>fallback</c> if not found.
		/// </summary>
		object? Get(object key, object? fallback);
	}
}
