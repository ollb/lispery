﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Interface for values that can evaluate to other values.
	/// </summary>
	public interface IEvaluable
	{
		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		object? EvaluateContinue(Environment env);
	}
}
