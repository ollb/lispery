﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the count function.
	/// </summary>
	public interface ISupportCount
	{
		/// <summary>
		/// Returns the number of values in the container.
		/// </summary>
		int Count();
	}
}
