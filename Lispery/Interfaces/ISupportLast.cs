﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the last function.
	/// </summary>
	public interface ISupportLast
	{
		/// <summary>
		/// Returns the last value in the container.
		/// </summary>
		object? Last();
	}
}
