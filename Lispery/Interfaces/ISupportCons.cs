﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the cons function.
	/// </summary>
	public interface ISupportCons
	{
		/// <summary>
		/// Adds a value at the start of the list.
		/// </summary>
		object Cons(object? value);
	}
}
