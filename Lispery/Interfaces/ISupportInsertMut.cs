﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the mutable insert function.
	/// </summary>
	public interface ISupportInsertMut
	{
		/// <summary>
		/// Inserts the given value at the given index.
		/// </summary>
		void InsertMut(int index, object? value);
	}
}
