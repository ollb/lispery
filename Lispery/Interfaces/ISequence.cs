﻿using Lispery.Adapters;
using Lispery.Sequences;
using Lispery.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Represents a sequence of values.
	/// </summary>
	public interface ISequence :
		ISupportCount,
		ISupportContains,
		ISupportFirst,
		ISupportNth,
		ISupportRest,
		ISupportCons,
		ISupportSkip,
		ISupportReverse,
		ISupportSort
	{
		/// <summary>
		/// Returns an enumerator for all values in the sequence.
		/// </summary>
		IEnumerable<object?> GetEntries();

		/// <summary>
		/// Determines if the given value is stored in the data structure.
		/// </summary>
		bool ISupportContains.Contains(object? value)
		{
			return GetEntries().Contains(value);
		}

		/// <summary>
		/// Returns the first value in the container.
		/// </summary>
		object? ISupportFirst.First()
		{
			var enumerator = GetEntries().GetEnumerator();

			if (enumerator.MoveNext())
			{
				return enumerator.Current;
			}

			return null;
		}

		/// <summary>
		/// Returns the n'th value in the container.
		/// </summary>
		object? ISupportNth.Nth(int index)
		{
			return GetEntries().ElementAt(index);
		}

		/// <summary>
		/// Returns a list with all values except for the first.
		/// </summary>
		new ISequence Rest()
		{
			return new LazySequence(GetEntries().Skip(1));
		}

		/// <summary>
		/// Returns a list with all values except for the first.
		/// </summary>
		object ISupportRest.Rest()
		{
			return Rest();
		}

		/// <summary>
		/// Returns a list with N values skipped.
		/// </summary>
		object ISupportSkip.Skip(long skip)
		{
			if (skip == 0)
			{
				return this;
			}

			Check.IsNonNegativeInt(skip);

			return new LazySequence(GetEntries().Skip((int)skip));
		}

		/// <summary>
		/// Adds a value at the start of the list.
		/// </summary>
		object ISupportCons.Cons(object? value)
		{
			return new LazySequence(value, this);
		}

		/// <summary>
		/// Reverses the contents of the container.
		/// </summary>
		object? ISupportReverse.Reverse()
		{
			return new LazySequence(GetEntries().Reverse());
		}

		/// <summary>
		/// Returns the values in sorted order.
		/// </summary>
		object? ISupportSort.Sort(SortOrder order)
		{
			return new LazySequence(order switch
			{
				SortOrder.Descending => GetEntries().OrderByDescending(v => v),
				_                    => GetEntries().OrderBy(v => v),
			});
		}
	}
}
