﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the mutable remove function.
	/// </summary>
	public interface ISupportRemoveMut
	{
		/// <summary>
		/// Removes a value from the data structure.
		/// </summary>
		void RemoveMut(object? value);
	}
}
