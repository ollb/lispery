﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the add function.
	/// </summary>
	public interface ISupportAdd
	{
		/// <summary>
		/// Adds a value to the data structure.
		/// </summary>
		object? Add(object? value);
	}
}
