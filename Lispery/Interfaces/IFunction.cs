﻿using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Common interface for function objects.
	/// </summary>
	public interface IFunction : ICallable
	{
		/// <summary>
		/// The type of function (used for optimization).
		/// </summary>
		public FunctionType FunctionType
		{
			get { return FunctionType.Unknown; }
		}

		/// <summary>
		/// Calls the function and returns the result.
		/// </summary>
		object? Call(List args);

		/// <summary>
		/// Calls the function and returns the result. May also return a Continuation
		/// object that describes how to continue execution (for tail call optimization).
		/// </summary>
		object? CallContinue(List args)
		{
			return Call(args);
		}

		/// <summary>
		/// Calls the object in a given context.
		/// </summary>
		object? ICallable.CallContinue(List args, Environment env, object? context)
		{
			return CallContinue(args.MapAsList(a => env.Interpreter.Evaluate(a, env)));
		}
	}
}
