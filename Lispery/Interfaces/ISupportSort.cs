﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	public enum SortOrder
	{
		Ascending,
		Descending
	}

	/// <summary>
	/// Adds support for the sort function.
	/// </summary>
	public interface ISupportSort
	{
		/// <summary>
		/// Returns the values in sorted order.
		/// </summary>
		object? Sort(SortOrder order = SortOrder.Ascending);
	}
}
