﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the contains function.
	/// </summary>
	public interface ISupportContains
	{
		/// <summary>
		/// Determines if the given value is stored in the data structure.
		/// </summary>
		bool Contains(object? value);
	}
}
