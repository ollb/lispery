﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the mutable set function.
	/// </summary>
	public interface ISupportSetMut
	{
		/// <summary>
		/// Associates the given key with the given value.
		/// </summary>
		void SetMut(object key, object? value);
	}
}
