﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Interfaces
{
	/// <summary>
	/// Adds support for the mutable sort function.
	/// </summary>
	public interface ISupportSortMut
	{
		/// <summary>
		/// Sorts the contents of the container.
		/// </summary>
		void SortMut(SortOrder order = SortOrder.Ascending);
	}
}
