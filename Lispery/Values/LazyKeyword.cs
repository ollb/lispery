﻿using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// Keyword that is looked up on first use.
	/// </summary>
	public record LazyKeyword(string Name) : IEvaluable, IFormatable
	{
		private Keyword? keyword;

		/// <summary>
		/// Evaluates the keyword.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			if (keyword == null)
			{
				keyword = env.GetKeyword(Name);
			}

			return keyword;
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			return $":{Name}";
		}

		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			writer.Write(':');
			writer.Write(Name);
		}
	}
}
