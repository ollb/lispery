﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// Represents a LISP-defined macro.
	/// </summary>
	public class Macro : ICallable
	{
		private record Case(Matcher Matcher, ExpressionList Body);

		private readonly Environment defEnv;
		private readonly List<Case> cases = new();
		private int arity = -1;
		private bool variadic;

		/// <summary>
		/// Creates a new macro.
		/// </summary>
		public Macro(Environment defEnv)
		{
			this.defEnv = defEnv;
		}

		/// <summary>
		/// Creates a new macro.
		/// </summary>
		public Macro(Environment defEnv, string? name)
		{
			this.defEnv = defEnv;
			Name = name;
		}

		/// <summary>
		/// Returns the name of the callable or <c>null</c> if unknown.
		/// </summary>
		public string? Name { get; init; }

		/// <summary>
		/// Adds a new case to the macro.
		/// </summary>
		public void AddCase(Vector vector, ExpressionList body)
		{
			MatchParser parser = new(MatchMode.Define, defEnv);
			Matcher matcher = parser.ParseSequence(vector);
			cases.Add(new Case(matcher, body));

			if (cases.Count == 1)
			{
				arity = Matcher.GetArity(defEnv, vector, out variadic);
			}
			else
			{
				arity = -1;
				variadic = false;
			}
		}

		/// <summary>
		/// Expands the macro.
		/// </summary>
		public object? Expand(List args)
		{
			if (arity >= 0)
			{
				Check.Arity(args, arity, variadic, Name);
			}

			Environment boundEnv = new(defEnv);

			foreach (var @case in cases)
			{
				boundEnv.Clear();

				if (@case.Matcher.TryMatch(boundEnv, args))
				{
					if (Name != null)
					{
						boundEnv.Define(Name, this);
					}

					return boundEnv.Interpreter.EvaluateContinue(@case.Body, boundEnv);
				}
			}

			string funStr = Name != null ? $" in macro '{Name}'" : "";
			throw new ArgumentException($"Match failure{funStr}");
		}

		/// <summary>
		/// Calls the object in a given context.
		/// </summary>
		public object? CallContinue(List args, Environment env, object? context)
		{
			return env.Interpreter.EvaluateMacro(this, args, env, context);
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			return $"Macro {Name ?? ""}";
		}
	}
}
