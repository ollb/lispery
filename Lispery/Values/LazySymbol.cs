﻿using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// Symbol that is looked up on first use.
	/// </summary>
	public record LazySymbol(string Name) : IEvaluable, IFormatable
	{
		private Symbol? symbol;

		/// <summary>
		/// Evaluates the symbol.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			if (symbol == null)
			{
				symbol = env.GetSymbol(Name);
			}

			return env.Resolve(symbol);
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			return Name;
		}

		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			writer.Write(Name);
		}
	}
}
