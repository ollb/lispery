﻿using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// Represents a keyword.
	/// </summary>
	public class Keyword : IFormatable
	{
		/// <summary>
		/// Creates a new keyword object.
		/// </summary>
		internal Keyword(int id, string name)
		{
			// Note: default Equals() and GetHashCode() with reference
			// equality work just fine for us, because there can only be
			// one keyword for each name.
			// Keywords from different tables will also never be equal.
			Id = id;
			Name = name;
		}

		/// <summary>
		/// Gets the name of the symbol.
		/// </summary>
		public string Name { get; init; }

		/// <summary>
		/// Returns the ID of the symbol.
		/// </summary>
		public int Id { get; init; }

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			return $":{Name}";
		}

		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			writer.Write(':');
			writer.Write(Name);
		}
	}
}
