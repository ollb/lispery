﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	public enum FunctionType
	{
		Unknown,
		Add,
		Subtract,
		Multiply,
		Divide,
		Less,
		LessOrEqual,
		Greater,
		GreaterOrEqual,
		Equal,
		NotEqual,
		Sum,
		Product,
		Average,
		Median
	}
}
