﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// Used to indicate an empty result. Not printed on REPL.
	/// </summary>
	public record EmptyResult;
}
