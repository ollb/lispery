﻿using Lispery.Containers;
using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// Represents a native .NET macro.
	/// </summary>
	public class SpecialForm : ICallable
	{
		private readonly Func<string, List, Environment, object?> func;

		/// <summary>
		/// Creates a new native macro.
		/// </summary>
		public SpecialForm(string name, Func<string, List, Environment, object?> func)
		{
			Name = name;
			this.func = func;
		}

		/// <summary>
		/// The name of the special form.
		/// </summary>
		public string Name { get; init; }

		/// <summary>
		/// Calls the object in a given context.
		/// </summary>
		public object? CallContinue(List args, Environment env, object? context)
		{
			return func(Name, args, env);
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			return $"SpecialForm {Name}";
		}
	}
}
