﻿using Lispery.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// A list of expressions.
	/// </summary>
	public record ExpressionList :
		IEnumerable<object?>,
		IEvaluable,
		IFormatable
	{
		private readonly IEnumerable<object?> expressions;

		/// <summary>
		/// Creates a new expression list.
		/// </summary>
		public ExpressionList(IEnumerable<object?> expressions)
		{
			this.expressions = expressions;
		}

		/// <summary>
		/// Creates a new expression list.
		/// </summary>
		public ExpressionList(params object?[] expressions)
		{
			this.expressions = expressions;
		}

		/// <summary>
		/// Returns an enumerator for the expressions.
		/// </summary>
		public IEnumerator<object?> GetEnumerator()
		{
			return expressions.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator for the expressions.
		/// </summary>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			using StringWriter writer = new();
			Formatter formatter = new();
			formatter.Format(this, writer);
			return writer.ToString();
		}

		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			var interpreter = env.Interpreter;
			object? result = null;

			foreach (var expression in expressions)
			{
				if (result is Continuation cont)
				{
					interpreter.Evaluate(cont, env);
				}

				result = interpreter.EvaluateContinue(expression, env);
			}

			return result;
		}

		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			foreach (var expression in expressions)
			{
				formatter.Format(expression, writer);
				writer.WriteLine();
			}
		}
	}
}
