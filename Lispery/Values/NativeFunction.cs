﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// Represents a native .NET function.
	/// </summary>
	public class NativeFunction : IFunction
	{
		private readonly Func<List, object?> func;

		/// <summary>
		/// Returns the name of the callable or <c>null</c> if unknown.
		/// </summary>
		public string? Name { get; init; }

		/// <summary>
		/// The type of function (used for optimization).
		/// </summary>
		public FunctionType FunctionType { get; init; }

		/// <summary>
		/// Returns the functions arity.
		/// </summary>
		public int Arity
		{
			get { return ArgumentTypes != null ? ArgumentTypes.Length : -1; }
		}

		/// <summary>
		/// Returns true if type information is available.
		/// </summary>
		public bool HasTypeInfo
		{
			get { return ArgumentTypes != null; }
		}

		/// <summary>
		/// Returns the types of the arguments.
		/// </summary>
		public Type[]? ArgumentTypes { get; init; }

		/// <summary>
		/// Returns the type of the variadic argument or null.
		/// </summary>
		public Type? VariadicType { get; init; }

		/// <summary>
		/// Returns the type of the argument at the given index.
		/// </summary>
		public Type GetArgumentType(int i)
		{
			if (!HasTypeInfo)
			{
				throw new InvalidOperationException("No type information");
			}

			return i < ArgumentTypes!.Length ?
				ArgumentTypes[i]! :
				VariadicType!;
		}

		/// <summary>
		/// Returns true if the function is variadic.
		/// </summary>
		public bool IsVariadic
		{
			get
			{
				if (!HasTypeInfo)
				{
					throw new InvalidOperationException("No type information");
				}

				return VariadicType != null;
			}
		}

		/// <summary>
		/// Returns true if the given arity is supported by the function.
		/// </summary>
		public bool SupportsArity(int callArity)
		{
			if (!HasTypeInfo)
			{
				throw new InvalidOperationException("No type information");
			}

			return VariadicType != null ?
				callArity >= ArgumentTypes!.Length :
				callArity == ArgumentTypes!.Length;
		}

		/// <summary>
		/// Creates a new native function.
		/// </summary>
		public NativeFunction(Func<List, object?> func) :
			this(null, null, null, func)
		{
		}

		/// <summary>
		/// Creates a new native function.
		/// </summary>
		public NativeFunction(string? name, Func<List, object?> func) :
			this(name, null, null, func)
		{
		}

		/// <summary>
		/// Creates a new native function.
		/// </summary>
		public NativeFunction(string? name, Type[]? argumentTypes, Type? variadicType, Func<List, object?> func)
		{
			Name = name;
			FunctionType = FunctionType.Unknown;
			ArgumentTypes = argumentTypes;
			VariadicType = variadicType;
			this.func = func;
		}

		/// <summary>
		/// Creates a new native function.
		/// </summary>
		public NativeFunction(string? name, FunctionType functionType, Type[]? argumentTypes, Type? variadicType, Func<List, object?> func)
		{
			Name = name;
			FunctionType = functionType;
			ArgumentTypes = argumentTypes;
			VariadicType = variadicType;
			this.func = func;
		}

		/// <summary>
		/// Calls the function and returns the result.
		/// </summary>
		public object? Call(List args)
		{
			return func(args);
		}

		/// <summary>
		/// Calls the function and returns the result. May also return a Continuation
		/// object that describes how to continue execution (for tail call optimization).
		/// </summary>
		public object? CallContinue(List args)
		{
			return Call(args);
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			StringBuilder sb = new();
			sb.Append("NativeFunction");

			if (Name != null)
			{
				sb.Append(' ');
				sb.Append(Name);
			}

			if (HasTypeInfo)
			{
				foreach (var type in ArgumentTypes!)
				{
					sb.Append(' ');
					sb.Append(type.Name);
				}

				if (VariadicType != null)
				{
					sb.Append(' ');
					sb.Append($"{VariadicType.Name}...");
				}
			}

			return sb.ToString();
		}
	}
}
