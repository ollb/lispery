﻿using Lispery.Containers;
using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// Stores information on how to continue code execution.
	/// </summary>
	internal class Continuation : IEvaluable
	{
		private readonly object? body;
		private readonly Environment env;

		/// <summary>
		/// Creates a new continuation.
		/// </summary>
		public Continuation(object? body, Environment env)
		{
			this.body = body;
			this.env = env;
		}

		/// <summary>
		/// Evaluates the continuation. May return another continuation.
		/// </summary>
		public object? Evaluate()
		{
			return env.Interpreter.Evaluate(body, env);
		}

		/// <summary>
		/// Evaluates the continuation. May return another continuation.
		/// </summary>
		public object? EvaluateContinue()
		{
			return env.Interpreter.EvaluateContinue(body, env);
		}

		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			return EvaluateContinue();
		}
	}
}
