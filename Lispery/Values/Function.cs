﻿using Lispery.Adapters;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// Represents a LISP-defined function.
	/// </summary>
	public class Function : IFunction
	{
		private record Case(Matcher Matcher, ExpressionList Body);

		private readonly Environment defEnv;
		private readonly List<Case> cases = new();
		private int arity = -1;
		private bool variadic;

		/// <summary>
		/// Creates a new function.
		/// </summary>
		public Function(Environment defEnv)
		{
			this.defEnv = defEnv;
		}

		/// <summary>
		/// Creates a new function.
		/// </summary>
		public Function(Environment defEnv, string? name)
		{
			this.defEnv = defEnv;
			Name = name;
		}

		/// <summary>
		/// Returns the name of the callable or <c>null</c> if unknown.
		/// </summary>
		public string? Name { get; init; }

		/// <summary>
		/// Adds a new case to the function.
		/// </summary>
		public void AddCase(Vector vector, ExpressionList body)
		{
			MatchParser parser = new(MatchMode.Define, defEnv);
			Matcher matcher = parser.ParseSequence(vector);
			cases.Add(new Case(matcher, body));

			if (cases.Count == 1)
			{
				arity = Matcher.GetArity(defEnv, vector, out variadic);
			}
			else
			{
				arity = -1;
				variadic = false;
			}
		}

		/// <summary>
		/// Calls the function and returns the result.
		/// </summary>
		public object? Call(List args)
		{
			return CreateContinuation(args).Evaluate();
		}

		/// <summary>
		/// Calls the function and returns the result. May also return a Continuation
		/// object that describes how to continue execution (for tail call optimization).
		/// </summary>
		public object? CallContinue(List args)
		{
			return CreateContinuation(args);
		}

		/// <summary>
		/// Creates a continuation for the call.
		/// </summary>
		private Continuation CreateContinuation(List args)
		{
			if (arity >= 0)
			{
				Check.Arity(args, arity, variadic, Name);
			}

			Environment boundEnv = new(defEnv);

			foreach (var @case in cases)
			{
				boundEnv.Clear();

				if (@case.Matcher.TryMatch(boundEnv, args))
				{
					if (Name != null)
					{
						boundEnv.Define(Name, this);
					}

					return new Continuation(@case.Body, boundEnv);
				}
			}

			string funStr = Name != null ? $" in '{Name}'" : "";
			throw new ArgumentException($"Match failure{funStr}");
		}

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			StringBuilder sb = new();
			sb.Append("Function");

			if (Name != null)
			{
				sb.Append(' ');
				sb.Append(Name);
			}

			return sb.ToString();
		}
	}
}
