﻿using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Values
{
	/// <summary>
	/// Represents a symbol.
	/// </summary>
	public class Symbol : IEvaluable, IFormatable
	{
		/// <summary>
		/// Creates a new symbol object.
		/// </summary>
		internal Symbol(int id, string name)
		{
			// Note: default Equals() and GetHashCode() with reference
			// equality work just fine for us, because there can only be
			// one symbol for each name.
			// Symbols from different tables will also never be equal.
			Id = id;
			Name = name;
		}

		/// <summary>
		/// Gets the name of the symbol.
		/// </summary>
		public string Name { get; init; }

		/// <summary>
		/// Returns the ID of the symbol.
		/// </summary>
		public int Id { get; init; }

		/// <summary>
		/// Returns a string representation.
		/// </summary>
		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			return Name;
		}

		/// <summary>
		/// Evaluates the object. May return a continuation.
		/// </summary>
		public object? EvaluateContinue(Environment env)
		{
			return env.Resolve(this);
		}

		/// <summary>
		/// Formats the value to a writer.
		/// </summary>
		public void Format(TextWriter writer, Formatter formatter)
		{
			writer.Write(Name);
		}
	}
}
