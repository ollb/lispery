﻿using Lispery.Adapters;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Rewrite;
using Lispery.Sequences;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lispery
{
	public class StdLib
	{
		private readonly Interpreter interpreter;
		private readonly Environment globalEnv;
		private readonly Random random = new();
		private readonly Stopwatch stopwatch = new();
		private readonly Regex numberRegex = new(@"^\d+$", RegexOptions.Compiled);
		private readonly Keyword kwAsc;
		private readonly Keyword kwDesc;

		public StdLib(Environment env)
		{
			interpreter = env.Interpreter;
			globalEnv = env;

			kwAsc = env.GetKeyword("asc");
			kwDesc = env.GetKeyword("desc");

			stopwatch.Start();

			// Basic macros
			env.Define("def", new SpecialForm("def", Def));
			env.Define("undef", new SpecialForm("undef", Undef));
			env.Define("assign!", new SpecialForm("assign!", AssignMut));
			env.Define("fn", new SpecialForm("fn", Fn));
			env.Define("macro", new SpecialForm("macro", Macro));
			env.Define("if", new SpecialForm("if", If));
			env.Define("do", new SpecialForm("do", Do));
			env.Define("lazy-seq", new SpecialForm("lazy-seq", LazySeq));
			env.Define("let", new SpecialForm("let", Let));
			env.Define("for", new SpecialForm("for", For));
			env.Define("case", new SpecialForm("case", Case));
			env.Define("quote", new SpecialForm("quote", Quote));
			env.Define("syntax-quote", new SpecialForm("syntax-quote", SyntaxQuote));
			env.Define("macroexpand", new SpecialForm("macroexpand", MacroExpand));
			env.Define("apply", new SpecialForm("apply", Apply));

			// Boolean logic
			env.DefineFunction<bool>("not", v => !v);
			env.Define("or", new SpecialForm("or", Or));
			env.Define("and", new SpecialForm("and", And));

			// Arithmetic operations
			env.Define("+", MakeDynAggregate("+", FunctionType.Add, (a, b) => a + b, a => a, () => (long)0));
			env.Define("-", MakeDynAggregate("-", FunctionType.Subtract, (a, b) => a - b, a => -a));
			env.Define("*", MakeDynAggregate("*", FunctionType.Multiply, (a, b) => a * b, a => a, () => (long)1));
			env.Define("/", MakeDynAggregate("/", FunctionType.Divide, (a, b) => a / b, a => 1.0 / a));
			env.DefineFunction<long, long>("mod", (a, b) => a % b);
			env.DefineFunction<long, long>("div", (a, b) => a / b);
			env.DefineFunction<long>("inc", v => v + 1);
			env.DefineFunction<long>("dec", v => v - 1);
			env.DefineFunction<long>("abs", v => Math.Abs(v));
			env.DefineFunction<double>("abs", v => Math.Abs(v));
			env.DefineFunction<double>("sin", v => Math.Sin(v));
			env.DefineFunction<double>("cos", v => Math.Cos(v));
			env.DefineFunction<double>("tan", v => Math.Tan(v));
			env.Define("math.pi", Math.PI);

			// Comparisons
			env.Define("<", MakeDynComparison("<", FunctionType.Less, (a, b) => a < b));
			env.Define(">", MakeDynComparison(">", FunctionType.Greater, (a, b) => a > b));
			env.Define("<=", MakeDynComparison("<=", FunctionType.LessOrEqual, (a, b) => a <= b));
			env.Define(">=", MakeDynComparison(">=", FunctionType.GreaterOrEqual, (a, b) => a >= b));
			env.Define("!=", MakeDynComparison("!=", FunctionType.NotEqual, (a, b) => a != b));
			env.Define("=", MakeDynComparison("=", FunctionType.Equal, (a, b) => a == b));
			env.Define("not=", env.Resolve("!="));
			env.DefineFunction<dynamic>("zero?", v => v == 0);
			env.DefineFunction<object, object>("eq?", (a, b) => ReferenceEquals(a, b));
			env.Define("max", new NativeFunction("max", Max));
			env.Define("min", new NativeFunction("min", Min));
			env.DefineFunction<double, double, double>("clamp", (v, min, max) => Math.Clamp(v, min, max));

			env.DefineFunction<ISequence>("sum", FunctionType.Sum, Sum!);
			env.DefineFunction<ISequence>("product", FunctionType.Product, Product!);
			env.DefineFunction<ISequence>("average", FunctionType.Average, Average!);
			env.DefineFunction<ISequence>("median", FunctionType.Median, Median!);
			env.DefineFunction<ISequence>("minimum", Minimum!);
			env.DefineFunction<ISequence>("maximum", Maximum!);

			env.DefineFunction<ISequence, ISequence>(
				"seq=",
				(a, b) => Enumerable.SequenceEqual(a!.GetEntries(), b!.GetEntries()));

			// Constructors
			env.DefineFunction<double>("int", v => (int)v);
			env.DefineFunction<long>("float", v => (double)v);
			env.DefineFunction<long>("bigint", v => new System.Numerics.BigInteger(v));
			env.DefineFunction<string>("int", s => long.Parse(s!, CultureInfo.InvariantCulture));
			env.DefineFunction<string>("float", s => double.Parse(s!, CultureInfo.InvariantCulture));
			env.DefineFunction<object>("bool", v => Conversion.IsTruthy(v));
			env.DefineFunction<string>("regex", s => new Regex(s!));
			env.DefineFunction<string>("symbol", s => env.GetSymbol(s!));
			env.DefineFunction<string>("keyword", s => env.GetKeyword(s!));
			env.DefineFunction<object>("function", s => Conversion.ToCallableOrNull(s));
			env.DefineFunction<object>("string", v => v?.ToString() ?? "nil");
			env.Define("make-list", new NativeFunction("make-list", a => a));
			env.Define("make-vec", new NativeFunction("make-vec", a => new Vector(a)));
			env.Define("make-vec!", new NativeFunction("make-vec!", a => new MutableVector(a)));
			env.Define("make-set", new NativeFunction("make-set", a => new Set(a)));
			env.Define("make-set!", new NativeFunction("make-set!", a => new MutableSet(a)));
			env.Define("make-dict", new NativeFunction("make-dict", a => new Dictionary(a.GroupKeyValue()!)));
			env.Define("make-dict!", new NativeFunction("make-dict!", a => new MutableDictionary(a.GroupKeyValue()!)));
			env.Define("make-seq", new NativeFunction("make-seq", a => new LazySequence(a)));
			env.Define("key-dict", new SpecialForm("key-dict", KeyDict));
			env.Define("key-dict!", new SpecialForm("key-dict!", KeyDictMut));
			env.DefineFunction<object, object>("kv-pair", (k, v) => KeyValuePair.Create(k, v));
			env.DefineFunction<ISequence>("to-list", s => new List(s!.GetEntries()));
			env.DefineFunction<ISequence>("to-vec", s => new Vector(s!.GetEntries()));
			env.DefineFunction<ISequence>("to-vec!", s => new MutableVector(s!.GetEntries()));
			env.DefineFunction<ISequence>("to-set", s => new Set(s!.GetEntries()));
			env.DefineFunction<ISequence>("to-set!", s => new MutableSet(s!.GetEntries()));
			env.DefineFunction<ISequence>("to-dict", s => new Dictionary(s!.GetEntries().Cast<KeyValuePair<object, object?>>()));
			env.DefineFunction<ISequence>("to-dict!", s => new MutableDictionary(s!.GetEntries().Cast<KeyValuePair<object, object?>>()));
			env.DefineFunction<ISequence>("to-seq", s => LazySequence.FromSequence(s!));
			env.DefineFunction<long>("range", Range1);
			env.DefineFunction<long, long>("range", Range2);
			env.DefineFunction<long, long, long>("range", Range3);
			env.Define("list", env.Resolve("make-list"));
			env.Define("seq", env.Resolve("make-seq"));
			env.DefineFunction<string>("chars", s => new Vector(s!.Select(c => (object)(long)c)));
			env.DefineFunction<string>("chars!", s => new MutableVector(s!.Select(c => (object)(long)c)));
			env.DefineFunction<ISequence>("make-string", MakeString);
			env.DefineFunction<KeyValuePair<object, object?>>("kv-key", e => e.Key);
			env.DefineFunction<KeyValuePair<object, object?>>("kv-val", e => e.Value);

			// Type checks
			env.DefineFunction<object>("int?", v => v is long);
			env.DefineFunction<object>("float?", v => v is double);
			env.DefineFunction<object>("num?", v => v is long || v is double);
			env.DefineFunction<object>("string?", v => v is string);
			env.DefineFunction<object>("bool?", v => v is bool);
			env.DefineFunction<object>("regex?", v => v is Regex);
			env.DefineFunction<object>("symbol?", v => v is Symbol);
			env.DefineFunction<object>("keyword?", v => v is Keyword);
			env.DefineFunction<object>("function?", v => v is IFunction);
			env.DefineFunction<object>("list?", v => v is List);
			env.DefineFunction<object>("vec?", v => v is Vector);
			env.DefineFunction<object>("vec!?", v => v is MutableVector);
			env.DefineFunction<object>("set?", v => v is Set);
			env.DefineFunction<object>("dict?", v => v is Dictionary);
			env.DefineFunction<object>("dict!?", v => v is MutableDictionary);
			env.DefineFunction<object>("seq?", v => v is ISequence);

			// Interface functions
			env.DefineFunction<object, ISupportAdd>("add", (v, c) => c!.Add(v));
			env.DefineFunction<object, ISupportAddMut>("add!", (v, c) => { c!.AddMut(v); return null; });
			env.DefineFunction<object, object, ISupportSet>("set", (k, v, c) => c!.Set(k!, v));
			env.DefineFunction<object, object, ISupportSetMut>("set!", (k, v, c) => { c!.SetMut(k!, v); return null; });
			env.DefineFunction<object, ISupportCons>("cons", (v, c) => c!.Cons(v));
			env.DefineFunction("contains", (object? v, ISupportContains? c) => c!.Contains(v));
			env.DefineFunction("count", (ISupportCount? c) => (long)c!.Count());
			env.DefineFunction<ISupportFirst>("first", c => c!.First());
			env.DefineFunction<object, ISupportGet>("get", (k, c) => c!.Get(k!));
			env.DefineFunction<object, object, ISupportGet>("get", (k, f, c) => c!.Get(k!, f));
			env.DefineFunction<long, object?, ISupportInsert?>("insert", (i, v, c) => c!.Insert(ToInt(i), v));
			env.DefineFunction<long, object?, ISupportInsertMut?>("insert!", (i, v, c) => { c!.InsertMut(ToInt(i), v); return null; });
			env.DefineFunction<ISupportLast>("last", c => c!.Last());
			env.DefineFunction<long, ISupportNth?>("nth", (i, c) => c!.Nth(ToInt(i)));
			env.DefineFunction<object, ISupportRemove>("remove", (v, c) => c!.Remove(v));
			env.DefineFunction<object, ISupportRemoveMut>("remove!", (v, c) => { c!.RemoveMut(v); return null; });
			env.DefineFunction<long, ISupportRemoveAt>("remove-at", (i, c) => c!.RemoveAt(ToInt(i)));
			env.DefineFunction<long, ISupportRemoveAtMut>("remove-at!", (i, c) => { c!.RemoveAtMut(ToInt(i)); return null; });
			env.DefineFunction<ISupportRest>("rest", c => c!.Rest());
			env.DefineFunction<ISupportReverse>("reverse", c => c!.Reverse());
			env.DefineFunction<ISupportReverseMut>("reverse!", c => { c!.ReverseMut(); return null; });
			env.DefineFunction<ISequence>("concat", Concat);
			env.DefineFunction<long, ISequence>("skip", (n, c) => c!.Skip(n));
			env.DefineFunction<ISupportSort>("sort", s => s!.Sort());
			env.DefineFunction<Keyword, ISupportSort>("sort", (o, s) => s!.Sort(GetSortOrder(o!)));
			env.DefineFunction<ISupportSortMut>("sort!", s => { s!.SortMut(); return null; });
			env.DefineFunction<Keyword, ISupportSortMut>("sort!", (o, s) => { s!.SortMut(GetSortOrder(o!)); return null; });

			// Sequence processing.
			env.DefineFunction<IFunction, ISequence>("filter", Filter!);
			env.DefineFunction<IFunction, ISequence>("fold", Fold!);
			env.DefineFunction<IFunction, object, ISequence>("fold", Fold!);
			env.DefineFunction<IFunction, ISequence>("scan", Scan!);
			env.DefineFunction<IFunction, object, ISequence>("scan", Scan!);
			env.DefineFunction<IFunction?, ISequence?>("map", Map!);
			env.DefineFunction<IFunction?, ISequence?>("flatmap", FlatMap!);

			// Miscellaneous
			env.DefineFunction<string, object>("print", Print);
			env.DefineFunction<long>("sleep", s => { Thread.Sleep(ToInt(s)); return null; });
			env.DefineFunction<KeyValuePair<object?, object?>>("key", e => e.Key);
			env.DefineFunction<KeyValuePair<object?, object?>>("value", e => e.Value);
			env.DefineFunction("rand", () => random.NextDouble());
			env.DefineFunction<double>("rand", max => random.NextDouble() * max);
			env.DefineFunction<long>("rand-int", max => (long)random.Next(ToInt(max)));
			env.DefineFunction<IFunction, object?>("iterate", Iterate);
			env.DefineFunction<object?>("repeat", Repeat1);
			env.DefineFunction<object?, long>("repeat", Repeat2);
			env.DefineFunction<long, ISequence>("take", Take);
			env.DefineFunction<IFunction, ISequence>("take-while", TakeWhile);
			env.DefineFunction<ICallable>("fn-name", FnName!);
			env.DefineFunction<object?>("type-of", v => v?.GetType().ToString() ?? "nil");
			env.DefineFunction("get-time", () => stopwatch.ElapsedMilliseconds);
			env.DefineFunction("gensym", () => env.SymbolTable.GenSym());
			env.DefineFunction<Symbol>("gensym", s => env.SymbolTable.GenSym(s!.Name));
			env.DefineFunction<string, Regex>("re-match", ReMatch);
			env.DefineFunction<string, Regex>("re-matches", ReMatches);
			env.DefineFunction<string, Regex>("re-groups", ReGroups);
			env.DefineFunction<string, string, Regex>("re-replace", ReReplace);

			env.Interpreter.Evaluate(StdLibLisp.Source);
		}

		/// <summary>
		/// Converts a keyword into a sort order.
		/// </summary>
		private SortOrder GetSortOrder(Keyword keyword)
		{
			if (keyword == kwAsc)
			{
				return SortOrder.Ascending;
			}
			else if (keyword == kwDesc)
			{
				return SortOrder.Descending;
			}

			throw new ArgumentException("Invalid sort order.");
		}

		#region Basic macros
		/// <summary>
		/// Defines a new symbol in the environment (implements <c>def</c>).
		/// </summary>
		private object? Def(string name, List args, Environment env)
		{
			Check.Arity(args, 2, name);
			var (matchExpr, valueExpr) = args.Match2();

			object? value = interpreter.Evaluate(valueExpr, env);

			MatchParser parser = new(MatchMode.Define, env);
			Matcher matcher = parser.Parse(matchExpr);
			matcher.Match(env, value);

			return value;
		}

		/// <summary>
		/// Undefines a symbol in the environment (implements <c>undef</c>).
		/// </summary>
		private object? Undef(string name, List args, Environment env)
		{
			Check.Arity(args, 1, name);
			var symbol = Conversion.To<Symbol>(args.First());
			globalEnv.Undefine(symbol);
			return null;
		}

		/// <summary>
		/// Assigns a new value to an existing symbol (implements <c>assign!</c>).
		/// </summary>
		private object? AssignMut(string name, List args, Environment env)
		{
			Check.Arity(args, 2, name);
			var (matchExpr, valueExpr) = args.Match2();

			object? value = interpreter.Evaluate(valueExpr, env);

			MatchParser parser = new(MatchMode.Assign, env);
			Matcher matcher = parser.Parse(matchExpr);
			matcher.Match(env, value);

			return value;
		}

		/// <summary>
		/// Creates a new anonymous function (implements <c>fn</c>).
		/// </summary>
		private object? Fn(string name, List args, Environment env)
		{
			Check.MinArity(args, 1, name);

			string? funName = null;

			if (args.First() is Symbol symbol)
			{
				funName = symbol.Name;
				args = args.Rest();
			}

			Function fun = new(env, funName);

			while (args.Count >= 1)
			{
				if (args.First() is List list)
				{
					var (matchExpr, body) = list.Match1R();
					var vector = Conversion.To<Vector>(matchExpr);
					args = args.Rest();
					fun.AddCase(vector, new ExpressionList(body));
				}
				else
				{
					var (matchExpr, body) = args.Match1R();
					var vector = Conversion.To<Vector>(matchExpr);
					fun.AddCase(vector, new ExpressionList(body));
					break;
				}
			}

			return fun;
		}

		/// <summary>
		/// Creates a new anonymous macro (implements <c>macro</c>).
		/// </summary>
		private object? Macro(string name, List args, Environment env)
		{
			Check.MinArity(args, 1, name);

			string? macroName = null;

			if (args.First() is Symbol symbol)
			{
				macroName = symbol.Name;
				args = args.Rest();
			}

			Macro macro = new(env, macroName);

			while (args.Count >= 1)
			{
				if (args.First() is List list)
				{
					var (matchExpr, body) = list.Match1R();
					var vector = Conversion.To<Vector>(matchExpr);
					args = args.Rest();
					macro.AddCase(vector, new ExpressionList(body));
				}
				else
				{
					var (matchExpr, body) = args.Match1R();
					var vector = Conversion.To<Vector>(matchExpr);
					macro.AddCase(vector, new ExpressionList(body));
					break;
				}
			}

			return macro;
		}

		/// <summary>
		/// Implements the <c>if</c> macro.
		/// </summary>
		private object? If(string name, List args, Environment env)
		{
			object? condExpr, trueBody, falseBody;

			if (args.Count == 2)
			{
				(condExpr, trueBody) = args.Match2();
				falseBody = null;
			}
			else
			{
				Check.Arity(args, 3, name);
				(condExpr, trueBody, falseBody) = args.Match3();
			}

			bool condition = Conversion.To<bool>(interpreter.Evaluate(condExpr, env));

			if (condition)
			{
				return interpreter.EvaluateContinue(trueBody, env);
			}
			else
			{
				return interpreter.EvaluateContinue(falseBody, env);
			}
		}

		/// <summary>
		/// Implements the <c>do</c> macro.
		/// </summary>
		private object? Do(string name, List args, Environment env)
		{
			return env.Interpreter.EvaluateContinue(new ExpressionList(args), env);
		}

		/// <summary>
		/// Creates a new <c>LazySequence</c>.
		/// </summary>
		private object? LazySeq(string name, List args, Environment env)
		{
			return new LazySequence(() =>
			{
				var result = env.Interpreter.Evaluate(new ExpressionList(args), env);
				return Conversion.ToNullable<ISequence>(result);
			});
		}

		/// <summary>
		/// Implements the <c>let</c> macro.
		/// </summary>
		private object? Let(string name, List args, Environment env)
		{
			Check.MinArity(args, 1, name);

			var (bindingsExpr, rest) = args.Match1R();

			Vector bindings = Conversion.To<Vector>(bindingsExpr);

			if (bindings.Count % 2 != 0)
			{
				throw new ArgumentException("binding without value");
			}

			Environment boundEnv = new(env);

			foreach (var entry in bindings.GroupKeyValue())
			{
				MatchParser parser = new(MatchMode.Define, env);
				Matcher matcher = parser.Parse(entry.Key);
				object? value = interpreter.Evaluate(entry.Value, boundEnv);
				matcher.Match(boundEnv, value);
			}

			return env.Interpreter.EvaluateContinue(
				new ExpressionList(rest),
				boundEnv);
		}

		/// <summary>
		/// Implements the <c>for</c> macro.
		/// </summary>
		private object? For(string name, List args, Environment env)
		{
			var forInterpreter = new ForInterpreter(env.KeywordTable, name, args);
			return forInterpreter.Evaluate(env);
		}

		/// <summary>
		/// Implements the <c>case</c> macro.
		/// </summary>
		private object? Case(string name, List args, Environment env)
		{
			Check.MinArity(args, 2, name);

			var (valueExpr, rest) = args.Match1R();

			object? value = interpreter.Evaluate(valueExpr, env);

			while (rest.Count >= 1)
			{
				object? matchExpr;
				List body;

				if (rest.First() is List list)
				{
					(matchExpr, body) = list.Match1R();
					rest = rest.Rest();
				}
				else if (rest.Count >= 2)
				{
					(matchExpr, var expr, rest) = rest.Match2R();
					body = List.Empty.Cons(expr);
				}
				else
				{
					throw new ArgumentException("Case without body");
				}

				Environment boundEnv = new(env);
				MatchParser parser = new(MatchMode.Define, env);
				Matcher matcher = parser.Parse(matchExpr);

				if (matcher.TryMatch(boundEnv, value))
				{
					return env.Interpreter.EvaluateContinue(
						new ExpressionList(body),
						boundEnv);
				}
			}

			return null;
		}

		private object? Quote(string name, List args, Environment env)
		{
			Check.Arity(args, 1, name);
			return args.First();
		}

		private object? SyntaxQuote(string name, List args, Environment env)
		{
			Check.Arity(args, 1, name);
			SyntaxQuoteRewriter rewriter = new(env);
			return rewriter.Rewrite(args.First());
		}

		private object? MacroExpand(string name, List args, Environment env)
		{
			Check.Arity(args, 1, name);

			if (args.First() is not List list || list.Count == 0)
			{
				throw new ArgumentException("Not a macro invocation.");
			}

			var (macroExpr, rest) = list.Match1R();
			var macro = Conversion.To<Macro>(env.Interpreter.Evaluate(macroExpr, env));
			return macro.Expand(rest);
		}

		/// <summary>
		/// Implements the <c>apply</c> function.
		/// </summary>
		private object? Apply(string name, List args, Environment env)
		{
			Check.MinArity(args, 2, name);

			var (callExpr, rest) = args.Match1R();
			var prefix = List.Empty.Cons(callExpr);

			while (rest.Count > 1)
			{
				(var expr, rest) = rest.Match1R();
				prefix = prefix.Cons(env.Interpreter.Evaluate(expr, env));
			}

			var seqExpr = rest.First();
			var seq = Conversion.To<ISequence>(env.Interpreter.Evaluate(seqExpr, env));
			var callArgs = List.FromSequence(seq);

			while (prefix.Count > 0)
			{
				callArgs = callArgs.Cons(prefix.First());
				prefix = prefix.Rest();
			}

			return env.Interpreter.Evaluate(callArgs, env);
		}
		#endregion

		#region Boolean logic
		/// <summary>
		/// Evaluates left to right. Returns the first value that is truthy.
		/// Otherwise returns the last value. <c>(or)</c> returns <c>nil</c>.
		/// </summary>
		private object? Or(string name, List args, Environment env)
		{
			object? result = null;

			foreach (var expr in args)
			{
				var value = interpreter.Evaluate(expr, env);

				if (Conversion.IsTruthy(value))
				{
					return value;
				}

				result = value;
			}

			return result;
		}

		/// <summary>
		/// Evaluates left to right. Returns the first value that is falsy.
		/// Otherwise returns the last value. <c>(and)</c> returns <c>true</c>.
		/// </summary>
		private object? And(string name, List args, Environment env)
		{
			object? result = true;

			foreach (var expr in args)
			{
				var value = interpreter.Evaluate(expr, env);

				if (Conversion.IsFalsy(value))
				{
					return value;
				}

				result = value;
			}

			return result;
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Creates a new <c>Dictionary</c> from symbols.
		/// </summary>
		private object? KeyDict(string name, List args, Environment env)
		{
			var builder = new Dictionary.Builder();

			foreach (var entry in args)
			{
				Symbol symbol = Conversion.To<Symbol>(entry);
				builder.Set(env.GetKeyword(symbol.Name), env.Interpreter.Evaluate(symbol, env));
			}

			return builder.ToImmutable();
		}

		/// <summary>
		/// Creates a new <c>MutableDictionary</c> from symbols.
		/// </summary>
		private object? KeyDictMut(string name, List args, Environment env)
		{
			var dict = new MutableDictionary();

			foreach (var entry in args)
			{
				Symbol symbol = Conversion.To<Symbol>(entry);
				dict.SetMut(env.GetKeyword(symbol.Name), env.Interpreter.Evaluate(symbol, env));
			}

			return dict;
		}

		/// <summary>
		/// Creates a new range sequence.
		/// </summary>
		private object Range1(long end)
		{
			return Range3(0, end, 1);
		}

		/// <summary>
		/// Creates a new range sequence.
		/// </summary>
		private object Range2(long start, long end)
		{
			return Range3(start, end, 1);
		}

		/// <summary>
		/// Creates a new range sequence.
		/// </summary>
		private object Range3(long start, long end, long step)
		{
			if (step == 0)
			{
				throw new ArgumentException("step is zero");
			}

			if (step < 0 && start <= end)
			{
				return LazySequence.Empty;
			}
			else if (step > 0 && start >= end)
			{
				return LazySequence.Empty;
			}

			IEnumerable<object> Generator()
			{
				if (step > 0)
				{
					for (long i = start; i < end; i += step)
					{
						yield return i;
					}
				}
				else
				{
					for (long i = start; i > end; i += step)
					{
						yield return i;
					}
				}
			}

			// TODO: set lazy count?
			return new LazySequence(Generator());
		}

		/// <summary>
		/// Converts chars to a string.
		/// </summary>
		private object? MakeString(ISequence? chars)
		{
			StringBuilder sb = new();

			foreach (var chr in chars!.GetEntries())
			{
				sb.Append((char)(long)chr!);
			}

			return sb.ToString();
		}
		#endregion

		#region Comparisons
		/// <summary>
		/// Returns the maximum of the arguments.
		/// </summary>
		private static object? Max(List args)
		{
			// Similar to Haskell: (max a b) and (maximum list)
			return args.Max(v => v);
		}

		/// <summary>
		/// Returns the minimum of the arguments.
		/// </summary>
		private static object? Min(List args)
		{
			// Similar to Haskell: (min a b) and (minimum list)
			return args.Min(v => v);
		}

		/// <summary>
		/// Returns the product of all values in the sequence.
		/// </summary>
		private static object? Product(ISequence seq)
		{
			return seq.GetEntries().Select(v => Conversion.To<double>(v)).Aggregate((a, b) => a * b);
		}

		/// <summary>
		/// Returns the sum of all values in the sequence.
		/// </summary>
		private static object? Sum(ISequence seq)
		{
			return seq.GetEntries().Sum(v => Conversion.To<double>(v));
		}

		/// <summary>
		/// Returns the average of all values in the sequence.
		/// </summary>
		private static object? Average(ISequence seq)
		{
			return seq.GetEntries().Average(v => Conversion.To<double>(v));
		}

		/// <summary>
		/// Returns the minimum of all values in the sequence.
		/// </summary>
		private static object? Minimum(ISequence seq)
		{
			return seq.GetEntries().Min(v => Conversion.To<double>(v));
		}

		/// <summary>
		/// Returns the maximum of all values in the sequence.
		/// </summary>
		private static object? Maximum(ISequence seq)
		{
			return seq.GetEntries().Max(v => Conversion.To<double>(v));
		}

		/// <summary>
		/// Returns the median of all values in the sequence.
		/// </summary>
		private static object? Median(ISequence seq)
		{
			return seq.GetEntries().Select(v => Conversion.To<double>(v)).Median();
		}
		#endregion

		private object? Concat(ISequence?[] sequences)
		{
			LazySequence? next = null;

			foreach (var sequence in sequences.Reverse())
			{
				next = LazySequence.FromSequence(sequence!, next);
			}

			return next;
		}

		#region Sequence processing
		private object? Filter(IFunction fun, ISequence seq)
		{
			return new LazySequence(seq.GetEntries().Where(v => Conversion.To<bool>(fun.Call(v))));
		}

		private object? Fold(IFunction fun, ISequence seq)
		{
			return seq.GetEntries().Aggregate((a, b) => fun.Call(a, b));
		}

		private object? Fold(IFunction fun, object seed, ISequence seq)
		{
			return seq.GetEntries().Aggregate(seed, (a, b) => fun.Call(a, b)!);
		}

		private object? Scan(IFunction fun, ISequence seq)
		{
			return new LazySequence(seq.GetEntries().Scan((a, b) => fun.Call(a, b)));
		}

		private object? Scan(IFunction fun, object seed, ISequence seq)
		{
			return new LazySequence(seq.GetEntries().Scan(seed, (a, b) => fun.Call(a, b)!));
		}

		private object? Map(IFunction fun, ISequence seq)
		{
			return new LazySequence(seq.GetEntries().Select(v => fun.Call(v)));
		}

		private object? FlatMap(IFunction fun, ISequence seq)
		{
			return new LazySequence(seq.GetEntries().SelectMany(v =>
			{
				return Conversion.To<ISequence>(fun.Call(v)).GetEntries();
			}));
		}
		#endregion

		#region Miscellaneous
		private object? Print(string? format, params object?[] args)
		{
			interpreter.PrintOutput(format!, args);
			return null;
		}

		private object? Iterate(IFunction? fun, object? seed)
        {
			IEnumerable<object?> Generator()
            {
				var value = seed;

				while (true)
                {
					yield return value;
					value = fun!.Call(value);
                }
            }

			return new LazySequence(Generator());
        }

		private object? Repeat1(object? value)
		{
			IEnumerable<object?> Generator()
			{
				while (true)
				{
					yield return value;
				}
			}

			return new LazySequence(Generator());
		}

		private object? Repeat2(object? value, long count)
		{
			return new LazySequence(Enumerable.Repeat(value, ToInt(count)));
		}

		private object? Take(long count, ISequence? seq)
        {
			Check.IsNonNegativeInt(count);
			return new LazySequence(seq!.GetEntries().Take((int)count));
		}

		private object? TakeWhile(IFunction? fun, ISequence? seq)
		{
			return new LazySequence(seq!.GetEntries().TakeWhile(
				v => Conversion.To<bool>(fun!.Call(v))));
		}

		private object? ReMatch(string? str, Regex? rex)
		{
			Match match = rex!.Match(str!);

			if (!match.Success)
			{
				return null;
			}

			return match.Value;
		}

		private object? ReMatches(string? str, Regex? rex)
		{
			MatchCollection matches = rex!.Matches(str!);

			if (matches.Count == 0)
			{
				return null;
			}

			var builder = new List.Builder();

			foreach (Match match in matches)
			{
				builder.Add(match.Value);
			}

			return builder.ToImmutable();
		}

		private object? ReGroups(string? str, Regex? rex)
		{
			Match match = rex!.Match(str!);

			if (!match.Success)
			{
				return null;
			}

			var builder = new Dictionary.Builder();

			foreach (Group group in match.Groups)
			{
				builder.Set(globalEnv.GetKeyword(group.Name), group.Value);
			}

			return builder.ToImmutable();
		}

		private object? ReReplace(string? str, string? replacement, Regex? rex)
		{
			return rex!.Replace(str!, replacement!);
		}

		private object? FnName(ICallable fun)
		{
			if (fun.Name != null)
			{
				return globalEnv.GetSymbol(fun.Name);
			}

			return null;
		}
		#endregion

		#region Helper methods
		/// <summary>
		/// Converts the value to integer. Throws an exception on overflow.
		/// </summary>
		private static int ToInt(long value)
		{
			Check.IsInt(value);
			return (int)value;
		}

		/// <summary>
		/// Creates a function that accepts an arbitrary amount of arguments.
		/// </summary>
		private static NativeFunction MakeAggregate<T>(
			Func<T, T, T> fun,
			Func<T, T>? unaryFun = null,
			Func<T>? emptyFun = null)
		{
			int minArity = 2;
			
			if (emptyFun != null)
			{
				Debug.Assert(unaryFun != null);
				minArity = 0;
			}
			else if (unaryFun != null)
			{
				minArity = 1;
			}

			return new NativeFunction(args =>
			{
				Check.MinArity(args, minArity);

				return args.Count switch
				{
					0 => emptyFun!(),
					1 => unaryFun!(Conversion.To<T>(args.First())),
					_ => args.Select(i => Conversion.To<T>(i)).Aggregate(fun)
				};
			});
		}

		/// <summary>
		/// Creates a function that accepts an arbitrary amount of arguments.
		/// Values are <c>dynamic</c>, to allow for dynamic resolution of the
		/// proper methods / operators.
		/// </summary>
		private static NativeFunction MakeDynAggregate(
			string name,
			FunctionType type,
			Func<dynamic, dynamic, dynamic> fun,
			Func<dynamic, dynamic>? unaryFun = null,
			Func<dynamic>? emptyFun = null)
		{
			int minArity = 2;

			if (emptyFun != null)
			{
				Debug.Assert(unaryFun != null);
				minArity = 0;
			}
			else if (unaryFun != null)
			{
				minArity = 1;
			}

			return new NativeFunction(name, type, null, null, args =>
			{
				Check.MinArity(args, minArity);

				return args.Count switch
				{
					0 => emptyFun!(),
					1 => unaryFun!((dynamic)args.First()!),
					_ => args.Cast<dynamic>().Aggregate(fun)
				};
			});
		}

		/// <summary>
		/// Creates a comparison function. Values are <c>dynamic</c>, to allow
		/// for dynamic resolution of the proper methods / operators.
		/// </summary>
		private static NativeFunction MakeDynComparison(
			string name,
			FunctionType type,
			Func<dynamic?, dynamic?, bool> fun)
		{
			return new NativeFunction(name, type, null, null, args =>
			{
				Check.Arity(args, 2);
				var (a, b) = args.Match2();
				return fun(a, b);
			});
		}
		#endregion
	}
}
