﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	public class PrintEventArgs : EventArgs
	{
		public string Output { get; private set; }

		public PrintEventArgs(string output)
		{
			Output = output;
		}
	}
}
