﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Adapters
{
	/// <summary>
	/// Makes a tree map callable as a function.
	/// </summary>
	internal class DictFunction : IFunction
	{
		private readonly Dictionary treeMap;

		/// <summary>
		/// Creates a new hash set function.
		/// </summary>
		public DictFunction(Dictionary treeMap)
		{
			this.treeMap = treeMap;
		}

		/// <summary>
		/// Returns the name of the callable or <c>null</c> if unknown.
		/// </summary>
		public string? Name => null;

		/// <summary>
		/// Calls the function and returns the result.
		/// </summary>
		public object? Call(List args)
		{
			Check.Arity(args, 1);
			return treeMap.Get(args.First(), null);
		}

		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			return "DictFunction";
		}
	}
}
