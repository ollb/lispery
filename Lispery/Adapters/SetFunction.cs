﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Adapters
{
	/// <summary>
	/// Makes a hash set callable as a function.
	/// </summary>
	internal class SetFunction : IFunction
	{
		private readonly Set hashSet;

		/// <summary>
		/// Creates a new hash set function.
		/// </summary>
		public SetFunction(Set hashSet)
		{
			this.hashSet = hashSet;
		}

		/// <summary>
		/// Returns the name of the callable or <c>null</c> if unknown.
		/// </summary>
		public string? Name => null;

		/// <summary>
		/// Calls the function and returns the result.
		/// </summary>
		public object Call(List args)
		{
			Check.Arity(args, 1);

			object? value = args.First();
			return hashSet.Contains(value);
		}

		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			return "SetFunction";
		}
	}
}
