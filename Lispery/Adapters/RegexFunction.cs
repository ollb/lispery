﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lispery.Adapters
{
	/// <summary>
	/// Makes a regex callable as a function.
	/// </summary>
	internal class RegexFunction : IFunction
	{
		private readonly Regex regex;

		/// <summary>
		/// Creates a new regex function.
		/// </summary>
		public RegexFunction(Regex regex)
		{
			this.regex = regex;
		}

		/// <summary>
		/// Returns the name of the callable or <c>null</c> if unknown.
		/// </summary>
		public string? Name => null;

		/// <summary>
		/// Calls the function and returns the result.
		/// </summary>
		public object? Call(List args)
		{
			Check.Arity(args, 1);
			return regex.IsMatch(Conversion.To<string>(args.First()));
		}

		[ExcludeFromCodeCoverage]
		public override string ToString()
		{
			return "RegexFunction";
		}
	}
}
