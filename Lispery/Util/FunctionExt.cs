﻿using Lispery.Containers;
using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Util
{
	/// <summary>
	/// Extension methods for <c>IFunction</c>.
	/// </summary>
	public static class FunctionExt
	{
		/// <summary>
		/// Calls the function and returns the result.
		/// </summary>
		public static object? Call(this IFunction fun, params object?[] args)
		{
			return fun.Call(new List(args));
		}
	}
}
