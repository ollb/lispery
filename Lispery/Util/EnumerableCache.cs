﻿using Lispery.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Util
{
	internal class EnumerableCache<T> : IEnumerable<T>, ICachedSequence
	{
		private readonly List<T> cache = new();
		private readonly IEnumerator<T> enumerator;

		public EnumerableCache(IEnumerable<T> enumerable)
		{
			enumerator = enumerable.GetEnumerator();
		}

		private bool AdvanceTo(int index)
		{
			while (index >= cache.Count)
			{
				if (!enumerator.MoveNext())
				{
					return false;
				}

				cache.Add(enumerator.Current);
			}

			return true;
		}

		public IEnumerator<T> GetEnumerator()
		{
			int index = 0;

			while (AdvanceTo(index))
			{
				yield return cache[index];
				index++;
			}
		}

		[ExcludeFromCodeCoverage]
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
