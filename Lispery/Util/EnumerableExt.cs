﻿using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Util
{
	/// <summary>
	/// Extension methods for <c>IEnumerable&lt;T&gt;</c>.
	/// </summary>
	public static class EnumerableExt
	{
		/// <summary>
		/// Group adjacent values in the given sequence into key value pairs.
		/// </summary>
		public static IEnumerable<KeyValuePair<T, T>> GroupKeyValue<T>(this IEnumerable<T> list)
		{
			T? last = default;
			bool first = true;

			foreach (T? next in list)
			{
				if (first)
				{
					last = next;
					first = false;
				}
				else
				{
					yield return KeyValuePair.Create<T, T>(last!, next);
					first = true;
				}
			}

			if (!first)
			{
				throw new ArgumentException("not a sequence of pairs");
			}
		}

		/// <summary>
		/// Calculates the median of the given sequence.
		/// </summary>
		public static double Median(this IEnumerable<double> list)
		{
			List<double> sortedList = new List<double>(list);

			if (sortedList.Count < 1)
			{
				throw new ArgumentException("Sequence is empty.");
			}

			sortedList.Sort();

			if (sortedList.Count % 2 == 0)
			{
				int m = sortedList.Count / 2;
				return (sortedList[m - 1] + sortedList[m]) / 2.0;
			}
			else
			{
				int m = (sortedList.Count - 1) / 2;
				return sortedList[m];
			}
		}

		public static IEnumerable<T> Cache<T>(this IEnumerable<T> list)
		{
			if (list is ICachedSequence || list is List<T> || list is T[])
			{
				return list;
			}

			return new EnumerableCache<T>(list);
		}

		public static IEnumerable<T> Scan<T>(this IEnumerable<T> list, Func<T, T, T> fun)
		{
			T? acc = default;
			bool first = true;

			foreach (var value in list)
			{
				acc = first ? value : fun(value, acc!);
				yield return acc;
				first = false;
			}
		}

		public static IEnumerable<U> Scan<T, U>(this IEnumerable<T> list, U seed, Func<T, U, U> fun)
		{
			U acc = seed;

			foreach (var value in list)
			{
				acc = fun(value, acc);
				yield return acc;
			}
		}
	}
}
