﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Util
{
	/// <summary>
	/// Compares two arrays of non-null <c>Type</c>.
	/// </summary>
	public class ArrayComparer<T> : IEqualityComparer<T[]>
	{
		/// <summary>
		/// Returns true if the two arrays match.
		/// </summary>
		public bool Equals(T[]? lhs, T[]? rhs)
		{
			if (lhs == null || rhs == null)
			{
				return lhs == null && rhs == null;
			}

			if (lhs.Length != rhs.Length)
			{
				return false;
			}

			var comparer = EqualityComparer<T>.Default;

			for (int i = 0; i < lhs.Length; i++)
			{
				if (!comparer.Equals(lhs[i], rhs[i]))
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Returns a hash code for the given array.
		/// </summary>
		public int GetHashCode(T[] obj)
		{
			int hash = 17;

			foreach (T item in obj)
			{
				int add = item != null ? item.GetHashCode() : 97;
				hash = hash * 23 + add;
			}

			return hash;
		}
	}
}
