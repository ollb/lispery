﻿using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Util
{
	/// <summary>
	/// Generates unique symbols based on input symbols.
	/// </summary>
	internal class SymbolGenerator
	{
		private readonly Dictionary<Symbol, Symbol> symbols = new();
		private readonly SymbolTable symbolTable;

		/// <summary>
		/// Creates a new symbol generator.
		/// </summary>
		public SymbolGenerator(SymbolTable symbolTable)
		{
			this.symbolTable = symbolTable;
		}

		/// <summary>
		/// Creates a unique symbol based on a template symbol.
		/// </summary>
		public Symbol Generate(Symbol symbol)
		{
			if (!IsTemplateSymbol(symbol))
			{
				throw new ArgumentException("Not a template symbol.");
			}

			if (!symbols.TryGetValue(symbol, out var genSymbol))
			{
				genSymbol = symbolTable.GenSym(symbol.Name[..^1]);
				symbols[symbol] = genSymbol;
			}

			return genSymbol;
		}

		/// <summary>
		/// Determines if the given symbol is a template symbol.
		/// </summary>
		public static bool IsTemplateSymbol(Symbol symbol)
		{
			return symbol.Name.EndsWith('#');
		}
	}
}
