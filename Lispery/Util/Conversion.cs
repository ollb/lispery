﻿using Lispery.Adapters;
using Lispery.Containers;
using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lispery.Util
{
	/// <summary>
	/// Provides various utility functions.
	/// </summary>
	public static class Conversion
	{
		private readonly static Dictionary<Type, Func<object?, object?>> converters = new();
		private readonly static object lockObj = new();

		static Conversion()
		{
			RegisterConverter<ICallable>(CallableConverter);
			RegisterConverter<bool>(v => IsTruthy(v));
		}

		/// <summary>
		/// Registers a new converter.
		/// </summary>
		public static void RegisterConverter<T>(Func<object?, object?> convertFun)
		{
			lock (lockObj)
			{
				converters[typeof(T)] = convertFun;
			}
		}

		/// <summary>
		/// Default converter for <c>ICallable</c>.
		/// </summary>
		private static object CallableConverter(object? arg)
		{
			var callable = ToCallableOrNull(arg);

			if (callable == null)
			{
				string type = arg?.GetType().Name ?? "NULL";
				throw new ArgumentException($"Invalid type (got {type}, expected ICallable)");
			}

			return callable;
		}

		/// <summary>
		/// Determines if a value is considered to be true.
		/// </summary>
		public static bool IsTruthy(object? arg)
		{
			if (arg == null)
			{
				return false;
			}

			if (arg is bool boolArg)
			{
				return boolArg;
			}

			return true;
		}

		/// <summary>
		/// Determines if a value is considered to be false.
		/// </summary>
		public static bool IsFalsy(object? arg)
		{
			return !IsTruthy(arg);
		}

		/// <summary>
		/// Tries to convert the given value to <c>ICallable</c>.
		/// </summary>
		public static ICallable? ToCallableOrNull(object? arg)
		{
			return arg switch
			{
				ICallable callable => callable,
				Regex regex        => new RegexFunction(regex),
				Dictionary dict    => new DictFunction(dict),
				Set set            => new SetFunction(set),
				_                  => null
			};
		}

		/// <summary>
		/// Returns the default converter function for the given type.
		/// </summary>
		public static Func<object?, T?> GetConverter<T>()
		{
			return arg =>
			{
				lock (lockObj)
				{
					if (converters.TryGetValue(typeof(T), out var converter))
					{
						return (T?)converter(arg);
					}
				}

				try
				{
					return (T?)Convert.ChangeType(arg, typeof(T));
				}
				catch
				{
					throw GetTypeException<T>(arg);
				}
			};
		}

		/// <summary>
		/// Returns a type conversion exception or null reference exception.
		/// </summary>
		public static Exception GetTypeException<TExpected>(object? arg)
		{
			if (arg == null)
			{
				throw new NullReferenceException($"Can not convert 'nil' into '{typeof(TExpected).Name}'");
			}

			throw new ArgumentException($"Invalid type (got '{arg.GetType().Name}', expected '{typeof(TExpected).Name}')");
		}

		/// <summary>
		/// Tries to unbox an <c>object</c> to a value of the given type. Uses the
		/// default converter function if the object contains a value of another type.
		/// If unsuccessful, an exception is thrown.
		/// </summary>
		public static T To<T>(object? arg)
		{
			return To<T>(arg, GetConverter<T>());
		}

		/// <summary>
		/// Tries to unbox an <c>object</c> to a value of the given type. Uses the
		/// given converter function if the object contains a value of another type.
		/// If unsuccessful, an exception is thrown.
		/// </summary>
		public static T To<T>(object? arg, Func<object?, T?> converter)
		{
			T? value = ToNullable<T>(arg, converter);

			if (value == null)
			{
				throw new NullReferenceException("Value expected to not be 'nil'");
			}

			return value;
		}

		/// <summary>
		/// Tries to unbox an <c>object</c> to a value of the given type. Uses the
		/// default converter function if the object contains a value of another type.
		/// If unsuccessful, an exception is thrown.
		/// </summary>
		public static T? ToNullable<T>(object? arg)
		{
			return ToNullable<T>(arg, GetConverter<T>());
		}

		/// <summary>
		/// Tries to unbox an <c>object</c> to a value of the given type. Uses the
		/// given converter function if the object contains a value of another type.
		/// If unsuccessful, an exception is thrown.
		/// </summary>
		public static T? ToNullable<T>(object? arg, Func<object?, T?> converter)
		{
			return arg switch
			{
				T typedArg => typedArg,
				_          => converter(arg)
			};
		}
	}
}
