﻿using Lispery.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Util
{
	/// <summary>
	/// Various check functions.
	/// </summary>
	public static class Check
	{
		/// <summary>
		/// Checks that <c>args</c> has the given arity. Throws an exception if not.
		/// </summary>
		public static void Arity(List args, int count, bool variadic, string? funName = null)
		{
			if (variadic)
			{
				MinArity(args, count, funName);
			}
			else
			{
				Arity(args, count, funName);
			}
		}

		/// <summary>
		/// Checks that <c>args</c> has the given arity. Throws an exception if not.
		/// </summary>
		public static void Arity(List args, int count, string? funName = null)
		{
			if (args.Count != count)
			{
				string funStr = funName != null ? $" in '{funName}'" : "";
				throw new ArgumentException($"Invalid arity{funStr} (got {args.Count}, expected {count})");
			}
		}

		/// <summary>
		/// Checks that <c>args</c> has at least the given arity. Throws an exception if not.
		/// </summary>
		public static void MinArity(List args, int count, string? funName = null)
		{
			if (args.Count < count)
			{
				string funStr = funName != null ? $" in '{funName}'" : "";
				throw new ArgumentException($"Invalid arity{funStr} (got {args.Count}, expected >= {count})");
			}
		}

		/// <summary>
		/// Checks that <c>args</c> has at most the given arity. Throws an exception if not.
		/// </summary>
		public static void MaxArity(List args, int count, string? funName = null)
		{
			if (args.Count > count)
			{
				string funStr = funName != null ? $" in '{funName}'" : "";
				throw new ArgumentException($"Invalid arity{funStr} (got {args.Count}, expected <= {count})");
			}
		}

		/// <summary>
		/// Checks that the value is in range for an integer.
		/// </summary>
		public static void IsInt(long value)
		{
			if (value < int.MinValue || value > int.MaxValue)
			{
				throw new ArgumentOutOfRangeException(nameof(value));
			}
		}

		/// <summary>
		/// Checks that the value is in range for an integer.
		/// </summary>
		public static void IsNonNegativeInt(long value)
		{
			if (value < 0 || value > int.MaxValue)
			{
				throw new ArgumentOutOfRangeException(nameof(value));
			}
		}
	}
}
