﻿using Lispery.Adapters;
using Lispery.Containers;
using Lispery.Values;
using Lispery.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Lispery.Interfaces;

namespace Lispery
{
	/// <summary>
	/// Formats source code to text.
	/// </summary>
	public class Formatter
	{
		/// <summary>
		/// Helper function to format values for output.
		/// </summary>
		public static string Format(object? obj)
		{
			using StringWriter sw = new();
			Formatter formatter = new();
			formatter.Format(obj, sw);
			return sw.ToString();
		}

		/// <summary>
		/// Tries to write the given object to the given text writer.
		/// </summary>
		public void Format(object? obj, TextWriter writer)
		{
			switch (obj)
			{
				case null:
					writer.Write("nil");
					break;

				case long num:
					writer.Write(num);
					break;

				case double num:
					writer.Write(num.ToString(CultureInfo.InvariantCulture));
					break;

				case bool value:
					writer.Write(value ? "true" : "false");
					break;

				case string str:
					writer.Write('"');
					writer.Write(EscapeString(str));
					writer.Write('"');
					break;

				case KeyValuePair<object, object> pair:
					writer.Write("(kv-pair ");
					Format(pair.Key, writer);
					writer.Write(' ');
					Format(pair.Value, writer);
					writer.Write(')');
					break;

				case Regex regex:
					// TODO: escaping could be less verbose.
					writer.Write("#\"");
					writer.Write(EscapeString(regex.ToString()));
					writer.Write("\"");
					break;

				case System.Numerics.BigInteger value:
					writer.Write(value.ToString());
					break;

				case IFormatable formatable:
					formatable.Format(writer, this);
					break;

				case ISequence seq:
					FormatSequence(seq, writer);
					break;

				default:
					writer.Write('<');
					writer.Write(obj.ToString());
					writer.Write('>');
					break;
			}
		}

		/// <summary>
		/// Formats an enumerable object.
		/// </summary>
		public void FormatEnumerable(IEnumerable<KeyValuePair<object, object?>> list, TextWriter writer, string open, string close)
		{
			FormatEnumerable(list.SelectMany(e => new object?[] { e.Key, e.Value }), writer, open, close);
		}

		/// <summary>
		/// Formats an enumerable object.
		/// </summary>
		public void FormatEnumerable(IEnumerable<object?> list, TextWriter writer, string open, string close)
		{
			writer.Write(open);
			bool first = true;

			foreach (var item in list)
			{
				if (!first)
				{
					writer.Write(' ');
				}

				Format(item, writer);
				first = false;
			}

			writer.Write(close);
		}

		private void FormatSequence(ISequence seq, TextWriter writer)
        {
			var @enum = seq.GetEntries().GetEnumerator();

			writer.Write("(");

			Stopwatch sw = new();
			sw.Start();

			int count = 0;

			while (@enum.MoveNext())
            {
				if (count > 0)
				{
					writer.Write(' ');
				}

				Format(@enum.Current, writer);

				if (++count >= 100 || sw.ElapsedMilliseconds >= 1000)
				{
					break;
				}
            }

			if (@enum.MoveNext())
            {
				writer.Write("...");
            }

			writer.Write(')');
        }

		/// <summary>
		/// Escapes the given string.
		/// </summary>
		private static string EscapeString(string str)
		{
			StringBuilder sb = new();

			foreach (char chr in str)
			{
				switch (chr)
				{
					case '\\':
						sb.Append("\\\\");
						break;

					case '"':
						sb.Append("\\\"");
						break;

					default:
						sb.Append(chr);
						break;
				}
			}

			return sb.ToString();
		}
	}
}
