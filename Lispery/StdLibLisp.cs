﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	internal static class StdLibLisp
	{
		public const string Source = @"
			(def defmacro (macro defmacro [name arg & body]
				`(def ~name (macro ~name ~arg ~@body))))

			(defmacro defn [name arg & body]
				`(def ~name (fn ~name ~arg ~@body)))

			(defmacro time [& body]
				`(let [start# (get-time)
			           result# (do ~@body)]
					[(- (get-time) start#) result#]))

			(defmacro when [cond & body]
				`(if ~cond (do ~@body)))

			(defmacro when-let [[a b] & body]
				`(let [~a ~b] (when ~a ~@body)))

			(defmacro if-let [[a b] then else]
				`(let [~a ~b] (if ~a ~then ~else)))

			(defn even? [v] (= (mod v 2) 0))
			(defn odd?  [v] (= (mod v 2) 1))

			(defn bind [fun & bind-args]
				(fn [& call-args] (apply fun (concat call-args bind-args))))

			(defn identity [v] v)

			(defn second [v] (nth 1 v))
			(defn third [v] (nth 2 v))

			;(defmacro and
			;	([]  true)
			;	([x] x)
			;	([x & rest]
			;		`(let [and# ~x]
			;			(if and# (and ~@rest) and#))))
			;
			;(defmacro or
			;	([]  nil)
			;	([x] x)
			;	([x & rest]
			;		`(let [or# ~x]
			;			(if or# or# (or ~@rest)))))
		";
	}
}
