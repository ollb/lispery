﻿using Lispery.Containers;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lispery
{
	public class Parser
	{
		private TextReader? reader;
		private int paramCount = -1;
		private readonly Regex numberRegex = new(@"^[+-]?\d+(\.\d+)?$", RegexOptions.Compiled);
		private readonly SymbolTable symbolTable;
		private readonly KeywordTable keywordTable;

		/// <summary>
		/// Creates a new parser.
		/// </summary>
		public Parser(SymbolTable symbolTable, KeywordTable keywordTable)
		{
			this.symbolTable = symbolTable;
			this.keywordTable = keywordTable;
		}

		/// <summary>
		/// Parses the source code provided by the given reader.
		/// </summary>
		public ExpressionList Parse(TextReader reader)
		{
			this.reader = reader;

			var expressions = new List<object?>();
			SkipWhitespaceAndComments();

			while (reader.Peek() != -1)
			{
				expressions.Add(ReadExpression());
				SkipWhitespaceAndComments();
			}

			return new ExpressionList(expressions);
		}

		/// <summary>
		/// Reads the next expression from the input stream.
		/// </summary>
		private object? ReadExpression()
		{
			while (true)
			{
				SkipWhitespaceAndComments();

				switch (reader!.Peek())
				{
					case '(': return new List(ReadSequence('(', ')'));
					case '[': return new Vector(ReadSequence('[', ']'));
					case '{': return new Dictionary(ReadSequence('{', '}').GroupKeyValue()!);
					case '"': return ReadString();
					case '#': return ReadMacro();
					case ':': return ReadKeyword();
					case '\'': return ReadQuote();
					case '`': return ReadSyntaxQuote();
					case '~': return ReadUnquote();
					case '!': return ReadMutableOrSymbol();
					case ';': SkipWhitespaceAndComments(); continue;
				}

				return ReadSymbolOrNumber();
			}
		}

		/// <summary>
		/// Reads a sequence of expressions enclosed in chars.
		/// </summary>
		private IEnumerable<object?> ReadSequence(char start, char end)
		{
			Expect(start);
			SkipWhitespaceAndComments();

			while (reader!.Peek() != end)
			{
				yield return ReadExpression();
				SkipWhitespaceAndComments();
				ExpectMoreContent(end);
			}

			Expect(end);
		}

		/// <summary>
		/// Reads a macro from the input stream.
		/// </summary>
		private object ReadMacro()
		{
			Expect('#');

			return reader!.Peek() switch
			{
				'(' => ReadFunctionShorthand(),
				'{' => new Set(ReadSequence('{', '}')),
				'"' => ReadRegex(),
				_   => throw new FormatException("Reader macro not recognized")
			};
		}

		/// <summary>
		/// Reads a function shorthand macro from the input stream.
		/// </summary>
		private object ReadFunctionShorthand()
		{
			if (paramCount >= 0)
			{
				throw new FormatException("Nested function shorthand");
			}

			paramCount = 0;

			object body = new List(ReadSequence('(', ')'));

			Vector parameters = new(
				Enumerable.Range(1, paramCount).
				Select(i => symbolTable.Get($"%{i}")));

			List func = List.Empty;
			func = func.Cons(body);
			func = func.Cons(parameters);
			func = func.Cons(symbolTable.SymFn);

			paramCount = -1;
			return func;
		}

		/// <summary>
		/// Reads a regular expression from the input stream.
		/// </summary>
		private object ReadRegex()
		{
			Expect('"');

			StringBuilder sb = new();

			while (reader!.Peek() != '"')
			{
				ExpectMoreContent();
				char chr = (char)reader!.Read();

				if (chr == '\\')
				{
					ExpectMoreContent();
					char escChr = (char)reader!.Read();

					switch (escChr)
					{
						case '\\':
							sb.Append(escChr);
							break;

						case '"':
							sb.Append(escChr);
							break;

						default:
							sb.Append(chr);
							sb.Append(escChr);
							break;
					}
				}
				else
				{
					sb.Append(chr);
				}
			}

			Expect('"');
			return new Regex(sb.ToString());
		}

		/// <summary>
		/// Reads a string from the input stream.
		/// </summary>
		private object ReadString()
		{
			Expect('"');

			StringBuilder sb = new();

			while (reader!.Peek() != '"')
			{
				ExpectMoreContent();
				char chr = (char)reader!.Read();

				if (chr == '\\')
				{
					ExpectMoreContent();
					chr = (char)reader!.Read();
					sb.Append(chr);
				}
				else
				{
					sb.Append(chr);
				}
			}

			Expect('"');
			return sb.ToString();
		}

		/// <summary>
		/// Reads a keyword from the input stream.
		/// </summary>
		private Keyword ReadKeyword()
		{
			StringBuilder sb = new();

			Expect(':');
			Collect(IsSymbol, sb);

			if (sb.Length == 0)
			{
				throw new FormatException("Empty keyword");
			}

			return keywordTable.Get(sb.ToString());
		}

		/// <summary>
		/// Reads a quoted value from the input stream.
		/// </summary>
		private List ReadQuote()
		{
			Expect('\'');
			return MakeCall("quote", ReadExpression());
		}

		/// <summary>
		/// Reads a quoted value from the input stream.
		/// </summary>
		private List ReadSyntaxQuote()
		{
			Expect('`');
			return MakeCall("syntax-quote", ReadExpression());
		}

		/// <summary>
		/// Reads a quoted value from the input stream.
		/// </summary>
		private List ReadUnquote()
		{
			Expect('~');

			if (reader!.Peek() == '@')
			{
				Expect('@');
				return MakeCall("unquote-splicing", ReadExpression());
			}

			return MakeCall("unquote", ReadExpression());
		}

		/// <summary>
		/// Reads a mutable data structure or a symbol.
		/// </summary>
		private object? ReadMutableOrSymbol()
		{
			Expect('!');

			return reader!.Peek() switch
			{
				'[' => new MutableVector(ReadSequence('[', ']')),
				'{' => new MutableDictionary(ReadSequence('{', '}').GroupKeyValue()!),
				'#' => ReadMutablSetOrSymbol(),
				_   => ReadSymbolOrNumber("!")
			};
		}

		/// <summary>
		/// Reads a mutable set or a symbol.
		/// </summary>
		private object? ReadMutablSetOrSymbol()
		{
			Expect('#');

			return reader!.Peek() switch
			{
				'{' => new MutableSet(ReadSequence('{', '}')),
				_   => ReadSymbolOrNumber("!#")
			};
		}

		/// <summary>
		/// Creates a "call" linked list.
		/// </summary>
		private List MakeCall(string name, object? expression)
		{
			return List.Empty.Cons(expression).Cons(symbolTable.Get(name));
		}

		/// <summary>
		/// Reads a number or symbol from the input stream.
		/// </summary>
		private object? ReadSymbolOrNumber(string prefix = "")
		{
			StringBuilder sb = new();
			sb.Append(prefix);
			Collect(IsSymbol, sb);

			if (sb.Length == 0)
			{
				throw new FormatException("empty symbol or number");
			}

			string value = sb.ToString();

			if (numberRegex.IsMatch(value))
			{
				if (value.Contains('.'))
				{
					return double.Parse(value, CultureInfo.InvariantCulture);
				}
				else
				{
					return long.Parse(value, CultureInfo.InvariantCulture);
				}
			}

			if (paramCount >= 0)
			{
				if (value == "%")
				{
					value = "%1";
					paramCount = Math.Max(paramCount, 1);
				}
				else if (IsArgShorthand(value))
				{
					int index = value[1] - '1';
					paramCount = Math.Max(paramCount, index + 1);
				}
			}

			return value switch
			{
				"nil"   => null,
				"true"  => true,
				"false" => false,
				_       => symbolTable.Get(value)
			};
		}

		#region Helper methods
		/// <summary>
		/// Determines if the given symbol name is an argument shorthand, such as
		/// <c>%</c>, <c>%1</c>, <c>%2</c> etc.
		/// </summary>
		private static bool IsArgShorthand(string name)
		{
			if (name.Length != 2 || !name.StartsWith("%"))
			{
				return false;
			}

			return name[1] >= '1' && name[1] <= '9';
		}

		/// <summary>
		/// Collects all characters satisfying the given predicate into the given
		/// <c>StringBuilder</c>. Always stops on EOF.
		/// </summary>
		private void Collect(Predicate<int> predicate, StringBuilder sb)
		{
			while (reader!.Peek() != -1 && predicate(reader!.Peek()))
			{
				sb.Append((char)reader!.Read());
			}
		}

		/// <summary>
		/// Skips whitespace and line-comments.
		/// </summary>
		private void SkipWhitespaceAndComments()
		{
			SkipWhitespace();

			while (reader!.Peek() == ';')
			{
				reader!.ReadLine();
				SkipWhitespace();
			}
		}

		/// <summary>
		/// Skips all characters until a non-whitespace character or EOF is found.
		/// </summary>
		private void SkipWhitespace()
		{
			while (IsWhiteSpace(reader!.Peek()))
			{
				reader!.Read();
			}
		}

		/// <summary>
		/// Determines if the given character (or EOF) is a symbol character.
		/// </summary>
		private static bool IsSymbol(int chr)
		{
			return chr != -1 &&
				!"()[]{}\"".Contains((char)chr) &&
				!IsWhiteSpace(chr);
		}

		/// <summary>
		/// Determines if the given character (or EOF) is whitespace.
		/// </summary>
		private static bool IsWhiteSpace(int chr)
		{
			return chr != -1 && char.IsWhiteSpace((char)chr);
		}

		/// <summary>
		/// Throws an exception if the next character is not the specified one.
		/// </summary>
		private void Expect(char chr)
		{
			int readChr = reader!.Read();

			if (readChr != chr)
			{
				throw new FormatException($"Unexpected character (expected '{chr}', got {FormatChar(readChr)}");
			}
		}

		/// <summary>
		/// Throws an exception if there is no more content.
		/// </summary>
		private void ExpectMoreContent()
		{
			if (reader!.Peek() == -1)
			{
				throw new FormatException($"Unexpected EOF");
			}
		}

		/// <summary>
		/// Throws an exception if there is no more content. Indicates the character
		/// that the caller would expect in the exception message.
		/// </summary>
		private void ExpectMoreContent(char chr)
		{
			if (reader!.Peek() == -1)
			{
				throw new FormatException($"Unexpected EOF (expected '{chr}')");
			}
		}

		/// <summary>
		/// Formats a character or EOF for display.
		/// </summary>
		private static string FormatChar(int chr)
		{
			return chr == -1 ? "EOF" : $"'{((char)chr)}'";
		}
		#endregion
	}
}
