﻿using Lispery.Adapters;
using Lispery.Containers;
using Lispery.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Sequences
{
	/// <summary>
	/// Sequence over a vector that skips the first N values.
	/// </summary>
	public class SkipSequence<T> :
		IEnumerable<object?>,
		ISequence
		where T : ISupportNth, ISupportCount
	{
		private readonly T vector;
		private readonly int skip = 1;

		/// <summary>
		/// Creates a new skip sequence.
		/// </summary>
		public SkipSequence(T vector, int skip = 1)
		{
			if (skip < 1)
			{
				throw new ArgumentException("Skip needs to be at least one.");
			}

			this.vector = vector;
			this.skip = Math.Min(vector.Count(), skip);
		}

		/// <summary>
		/// Returns the number of values in the container.
		/// </summary>
		public int Count()
		{
			return Math.Max(0, vector.Count() - skip);
		}

		/// <summary>
		/// Returns the n'th value in the container.
		/// </summary>
		object? ISupportNth.Nth(int index)
		{
			return vector.Nth(index + skip);
		}

		/// <summary>
		/// Returns an enumerator for all values in the sequence.
		/// </summary>
		public IEnumerable<object?> GetEntries()
		{
			int count = vector.Count();

			for (int i = skip; i < count; i++)
			{
				yield return vector.Nth(i);
			}
		}

		/// <summary>
		/// Adds a value at the start of the list.
		/// </summary>
		object ISupportCons.Cons(object? value)
		{
			if (skip > 0 && vector is Vector && object.Equals(vector.Nth(skip - 1), value))
			{
				if (skip == 1)
				{
					return vector;
				}

				return new SkipSequence<T>(vector, skip - 1);
			}

			return new LazySequence(value, this);
		}

		/// <summary>
		/// Returns a list with all values except for the first.
		/// </summary>
		ISequence ISequence.Rest()
		{
			return new SkipSequence<T>(vector, skip + 1);
		}

		/// <summary>
		/// Returns a list with N values skipped.
		/// </summary>
		object ISupportSkip.Skip(long skip)
		{
			if (skip == 0)
			{
				return this;
			}

			if (this.skip + skip > int.MaxValue || skip < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(skip));
			}

			return new SkipSequence<T>(vector, this.skip + (int)skip);
		}

		public IEnumerator<object?> GetEnumerator()
		{
			return GetEntries().GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
