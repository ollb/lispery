﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery.Sequences
{
	/// <summary>
	/// Represents a cached, chainable sequence of values.
	/// </summary>
	public class LazySequence :
		ISequence,
		ISupportCons,
		ICachedSequence,
		ISupportReverse,
		ISupportLast
	{
		// Must come first. Static initialization order.
		private static readonly object?[] emptyChunk = Array.Empty<object?>();

		public static readonly LazySequence Empty = new();

		private IEnumerable<object?> chunk = emptyChunk;
		private Func<ISequence?>? nextFun;
		private ISequence? next;

		/// <summary>
		/// Creates a new lazy sequence.
		/// </summary>
		private LazySequence()
		{
		}

		/// <summary>
		/// Creates a lazy sequence with value prepended.
		/// </summary>
		public LazySequence(object? value, ISequence next)
		{
			chunk = new object?[] { value };
			this.next = next;
		}

		/// <summary>
		/// Creates a new lazy sequence from a generator function.
		/// </summary>
		/// <remarks>
		/// The generator is only evaluated when needed.
		/// </remarks>
		public LazySequence(Func<ISequence?> nextFun)
		{
			this.nextFun = nextFun;
		}

		/// <summary>
		/// Creates a new lazy sequence from an enumerable.
		/// </summary>
		public LazySequence(IEnumerable<object?> chunk, ISequence? next = null)
		{
			this.chunk = chunk.Cache();
			this.next = next;
		}

		/// <summary>
		/// Creates a new lazy sequence from an enumerable.
		/// </summary>
		public LazySequence(IEnumerable<object?> chunk, Func<ISequence?>? nextFun = null)
		{
			this.chunk = chunk.Cache();
			this.nextFun = nextFun;
		}

		/// <summary>
		/// Creates a new lazy sequence from an enumerable.
		/// </summary>
		public LazySequence(IEnumerable<object?> chunk)
		{
			this.chunk = chunk.Cache();
		}

		/// <summary>
		/// Creates a lazy sequence from an ordered sequence.
		/// </summary>
		public static LazySequence FromSequence(ISequence seq)
		{
			if (seq is LazySequence lazy)
			{
				return lazy;
			}

			return new LazySequence(seq.GetEntries());
		}

		/// <summary>
		/// Creates a lazy sequence from two sequences.
		/// </summary>
		public static LazySequence FromSequence(ISequence seq, ISequence? next = null)
		{
			if (seq is LazySequence lazy)
			{
				return new LazySequence(lazy.chunk, next);
			}

			return new LazySequence(seq.GetEntries(), next);
		}

		/// <summary>
		/// Returns the next sequence in the chain.
		/// </summary>
		public ISequence? GetNext()
		{
			if (nextFun == null)
			{
				return next;
			}

			next = nextFun();
			nextFun = null;
			return next;
		}

		/// <summary>
		/// Returns an enumerator for all values in the sequence.
		/// </summary>
		public IEnumerable<object?> GetEntries()
		{
			LazySequence? current = this;

			do
			{
				foreach (var item in current.chunk)
				{
					yield return item;
				}

				switch (current.GetNext())
				{
					case null:
						yield break;

					case LazySequence lazy:
						current = lazy;
						break;

					case var seq:
						foreach (var item in seq.GetEntries())
						{
							yield return item;
						}

						yield break;
				}
			}
			while (current != null);
		}

		/// <summary>
		/// Computes the number of values in the sequence.
		/// </summary>
		public int Count()
		{
			return GetEntries().Count();
		}

		/// <summary>
		/// Returns a string representation of the object.
		/// </summary>
		public override string ToString()
		{
			IEnumerable<object?> list = GetEntries();
			string suffix = "";

			if (list.Skip(10).Any())
			{
				list = list.Take(10);
				suffix = "...";
			}

			return $"(#LazySequence {string.Join(" ", list)}{suffix})";
		}

		/// <summary>
		/// Adds a value at the start of the list.
		/// </summary>
		public object Cons(object? value)
		{
			return new LazySequence
			{
				chunk = new object?[] { value },
				next  = this
			};
		}

		/// <summary>
		/// Returns a list with all values except for the first.
		/// </summary>
		public ISequence Rest()
		{
			var enumerator = chunk.GetEnumerator();

			if (!enumerator.MoveNext())
			{
				// Empty chunk. Run Rest() on GetNext().
				return GetNext()?.Rest() ?? List.Empty;
			}

			if (!enumerator.MoveNext())
			{
				// Singleton chunk. Just return GetNext().
				return GetNext() ?? List.Empty;
			}

			// Actually truncate the chunk.
			return new LazySequence
			{
				chunk   = chunk.Skip(1),
				next    = next,
				nextFun = nextFun
			};
		}

		public object Reverse()
		{
			return new LazySequence(GetEntries().Reverse());
		}

		public object? Last()
		{
			return GetEntries().LastOrDefault();
		}
	}
}
