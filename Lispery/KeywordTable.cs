﻿using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	/// <summary>
	/// Stores and manages the keywords known to an interpreter.
	/// </summary>
	public class KeywordTable
	{
		private readonly Dictionary<string, Keyword> keywordMap = new();
		private readonly List<Keyword> keywords = new();

		public Keyword KwWhen { get; init; }
		public Keyword KwLet { get; init; }

		/// <summary>
		/// Creates a new keyword table.
		/// </summary>
		public KeywordTable()
		{
			// Initialize pre-defined keywords.
			KwWhen = Get("when");
			KwLet = Get("let");
		}

		/// <summary>
		/// Returs the keyword for the given name.
		/// </summary>
		public Keyword Get(string name)
		{
			ArgumentNullException.ThrowIfNull(name);

			if (!keywordMap.TryGetValue(name, out Keyword? keyword))
			{
				keyword = new Keyword(keywords.Count, name);
				keywords.Add(keyword);
				keywordMap[name] = keyword;
			}

			return keyword;
		}
	}
}
