﻿# Sequences

Data structures that allow for iteration will implement the `ISequence` interface. This makes it possible
to use a whole range of functions consistently with those data structures.

```clojure
>> (length [1 2 3])
3
>> (length (list 1 2))
2
```

Some functions will return a sequence themselves, for example:

```clojure
>> (map #(* 2 %) [1 2 3])
(2 4 6)
>> (rest [1 2 3])
(2 3)
```

Supported functions:

| Function               | Description |
|:---------------------- |:----------- |
| `(contains value seq)` | Checks if the sequence contains the given value. |
| `(count seq)`          | Returns the number of elements in the sequence. |
| `(first seq)`          | Returns the first element of the sequence. |
| `(filter pred seq)`    | Returns a sequence that only contains values `v` where `(pred v)` is truthy. |
| `(fold fun seq)`       | Folds the function `fun` over the sequence. |
| `(fold fun init seq)`  | Folds the function `fun` over the sequence. Uses `init` as initial accumulator value. |
| `(scan fun seq)`       | Same as `fold`, but returns all function return values as a sequence. |
| `(scan fun init seq)`  | Same as `fold`, but returns all function return values as a sequence. |
| `(map fun seq)`        | Maps the function `fun` over the sequence. Returns a new sequence. |
| `(flatmap fun seq)`    | Maps the function `fun` over the sequence. Concatenates the results into a new sequence. |
| `(nth index seq)`      | Returns the element at the position `index` from the sequence. Throws if out of range. |
| `(reverse seq)`        | Reverses the elements of the sequence. Returns a new sequence. |
| `(cons value seq)`     | Returns the sequence with `value` prepended. |
| `(rest seq)`           | Returns the sequence without the first value. |
| `(skip num seq)`       | Returns the sequence without the first `num` values. |
| `(concat seq...)`      | Concatenates multiple sequences. |
| `(sort seq)`           | Returns a sorted sequence. |
| `(sort order seq)`     | Returns a sorted sequence. `order` can be `:asc` or `:desc`. |

Some data structures that implement the sequence interface may provide more efficient or more logical
implementations for the functions. For example `rest` and `cons` on a List will return another List.
Or `contains` on a Set will use an efficient check, instead of iterating all values.

## Lazy evaluation

Sequences can be defined in a way that allows for lazy evaluation. Such a sequence will only be evaluated
as far as necessary. This also allows for the definition of infinite sequences.

For example using `iterate`, it is simple to define the sequence of natural numbers:

```clojure
>> (def numbers (iterate #(inc %) 1))
(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43
 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83
 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100...)
>> (take 10 numbers)
(1 2 3 4 5 6 7 8 9 10)
>> (sum (take 10000 numbers))
50005000
```

Please note that expressions like `(sum numbers)` will not terminate, because they will try to evaluate
the whole sequence.

Lazy sequences can also be defined recursively using `lazy-seq`:

```clojure
>> (defn numbers [n]
     (cons n (lazy-seq (numbers (inc n)))))
<Function>
>> (take 10 (numbers 1))
(1 2 3 4 5 6 7 8 9 10)
```

`lazy-seq` will take an expression that returns a sequence. That expression will however only be evaluated,
when the iteration of the sequence makes it necessary.