# Summary

- [REPL](./1-REPL.md)
- [Basic Types](./2-BasicTypes.md)
- [Data Types](./3-DataTypes.md)
- [Environments and Functions](./4-EnvironmentsAndFunctions.md)
- [Mutable Data Types](./5-MutableDataTypes.md)
- [Pattern Matching](./6-PatternMatching.md)
- [Sequences](./7-Sequences.md)
- [Regular Expressions](./8-RegularExpressions.md)
- [.NET Integration](./9-DotNetIntegration.md)
