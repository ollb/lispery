﻿# Pattern Matching

Lispery has support for sophisticated pattern matching. This can be used in `def`, `defn`, `fn`, `let` etc.
or more explicitly with the `case` special form.

Those forms will also accept data structures instead of a simple symbol and will try to match it against the
value that is actually passed in. The syntax for matching and constructing data structures is mostly the same.
Any symbol in the match expression will receive the corresponding value from the passed-in data structure.

```clojure
>> (def [a b] [1 2])
[1 2]
>> a
1
>> b
2
```

Data structures and match expressions can be nested:

```clojure
>> (def [a b {:x x}] [1 2 {:a 9 :x 3}])
[1 2 {:x 3 :a 9}]
>> [a b x]
[1 2 3]
```

Failed matches will generate an error:

```clojure
>> (def [a b] [1 2 3])
Error: Match failure
```

## Matching sequences

When matching sequences, such as a `list` or `vector`, you can use `&` in the match expression to match
the rest of the sequence.

```clojure
>> (def [a b & rest] [1 2 3 4 5])
[1 2 3 4 5]
>> rest
(3 4 5)
```

Please note that the type of the sequence is still relevant. For example `[a & rest]` will match a `vector`,
but not a `list`. There is however a special match function `seq`, that can be used to achieve this:

```clojure
>> (def (seq a & rest) [1 2 3])
[1 2 3]
>> rest
(2 3)
>> (def (seq a & rest) (list 1 2 3))
(1 2 3)
>> rest
(2 3)
```

## Matching against multiple expressions

Some special forms accept multiple match expressions. In those cases, the value is compared against the match
expressions in order, until a matching expression is found. If no matching expression is found, an error is
generated.

This can be used in the `case` form for example:

```clojure
>> (case [1 2]
     [a]   a
     [a b] (+ a b))
3
>> (case [1]
     [a]   a
     [a b] (+ a b))
1
```

Another example would be in `defn` or `fn`, which both accept multiple function bodies, that are selected
based on the first match that works.

```clojure
>> (defn test
     ([(seq a)]        a)
     ([(seq a & rest)] (+ a (test rest))))
>> (test [1])
1
>> (test [1 2 3])
6
>> (defn test
     ([0] 1)
     ([n] (* n (test (- n 1)))))
>> (test 0)
1
>> (test 4)
24
```

## Match functions

There are other match functions, which can make more specific matches, for example on the type of the
expression.

```clojure
>> (defn test
     ([(int _)]    :int)
     ([(float _)]  :float)
     ([(string _)] :string)
     ([(vector _)] :vector))
<Function>
>> (test "a")
:string
>> (test 5.5)
:float
```

Also shown here is the use of the special symbol `_`, which indicates that the match should not assign
the corresponding part of the value to any symbol (aka "don't care").

If you want to match against the symbols `&` or `_`, they need to be quoted:

```clojure
>> (defn test
     (['&]        :ampersand)
     ([(quote _)] :underline))
<Function>
>> (test '&)
:ampersand
>> (test '_)
:underline
```

You can also match against the value a symbol refers to instead of assigning the symbol with '=':

```clojure
>> (def a 5)
5
>> (defn test [(= a)] 1)
<Function>
>> (test 1)
Error: Match failure
>> (test 5)
1
```

## `when` match function

The match function `when` is particularly powerful. It makes it possible to attach an additional
condition to a match. The syntax is `(when cond match-expr)`. First `match-expr` is evaluated and
the symbols in it are defined. Then `cond` is evaluated in that environment.

For example the following matches have the same effect:

```clojure
(defn test [(int v)] ...)
(defn test [(when (int? v) v)] ...)
```

When the condition expression returns a function, that function is called with the bound value.
This makes it possible to shorten the syntax a bit:

```clojure
(defn test [(when int? v)] ...)
```

More elaborate conditions are also possible:

```clojure
>> (defn test
     ([(when (< a b) [a b])] :a)
     ([_]                    :b))
<Function>
>> (test [1 2])
:a
>> (test [1 1])
:b
```


## Matching dictionaries

Dictionary values will by default match like any other value:

```clojure
>> (def {:a a} {:x 5 :a 9})
{:x 5 :a 9}
>> a
9
```

You can match a dictionary against a sequence with `>dict`. This will first convert the sequence into
a dictionary (which will require an even amount of values) and then match that against the expression
provided.

```clojure
>> (def (>dict {:a a}) [:x 5 :a 9])
[:x 5 :a 9]
>> a
9
```

This is also very useful for adding optional named arguments to a function:

```clojure
>> (defn test [a & (>dict d)]
     (+ a (get :b d)))
<Function>
>> (test 5 :x 9 :b 8)
13
```

## Matching keyword dictionaries

There is a special form `key-dict` to conveniently construct a dictionary with keyword keys from
existing symbols.

```clojure
>> (def [a b] [1 2])
[1 2]
>> (key-dict a b)
{:a 1 :b 2}
```

In a similar manner, the `key-dict` match function can be used to match against a dictionary:

```clojure
>> (def (key-dict a b) {:x 9 :a 2 :b 3})
{:x 9 :a 2 :b 3}
>> [a b]
[2 3]
```

You can also provide default values like this:

```clojure
>> (def (key-dict [a 2] b) {:x 9 :b 3})
{:x 9 :b 3}
>> [a b]
[2 3]
```

Similar to `>dict`, there exists a match function `>key-dict`, to convert the input value to a
dictionary before matching it with `key-dict`. This simplifies functions with optional named
arguments even further:

```clojure
>> (defn test [a & (>key-dict [b 1])]
     (+ a b))
<Function>
>> (test 1 :b 2)
3
>> (test 1)
2
```
