﻿# Regular Expressions

Lispery has some built-in support for regular expressions. A regular expression can be created using the `#"..."` reader macro:

```clojure
>> #"test\d+"
#"test\\d+"
```

You can use the `re-match` to match a regular expression against a string:

```clojure
>> (re-match "this is a test123 or test321" #"test\d+")
"test123"
```

The function will return `nil` when no match is found, so it converts into a boolean expression that can be used as a condition.

If you want to retrieve all matches, the `re-matches` function can be used:

```clojure
>> (re-matches "this is a test123 or test321" #"test\d+")
("test123" "test321")
```

Named match groups can be accessed with `re-groups`:

```clojure
>> (re-groups "this is a test123 or test321" #"test(?<number>\d+)")
{:number "123" :0 "test123"}
```

You can also perform replacements in a string with `re-replace`. `$1`, `$2` etc. can be used to refer to the match groups:

```clojure
>> (re-replace "this is a test123 or test321" "abcd$1" #"test(\d+)")
"this is a abcd123 or abcd321"
```

A literal `$` in the replacement string can be escaped by repeating it as `$$`.