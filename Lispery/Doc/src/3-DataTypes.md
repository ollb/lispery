﻿# Data Types

Lispery supports several different data structures. By default all data structures are immutable. Therefore
any change to a data structure will produce a new data structure as a result.

## List

The most basic and also most important data structure is the list. It is stored as a singly-linked list in
memory and supports efficient addition and removal of elements at the start of the list with `cons` and `rest`.
Note that lists have to be quoted to prevent the interpreter from evaluating them. Alternatively, the `list`
function can be used to construct them.

```clojure
>> '(1 2 3)
(1 2 3)
>> (list 1 2 3)
(1 2 3)
>> (cons 7 (list 1 2))
(7 1 2)
>> (rest (cons 7 (list 1 2)))
(1 2)
```

Lists will support all functions of the sequence interface (see [Sequences](7-Sequences.md)). `cons` and `rest`
will have more efficient implementations that returns Lists themselves.

## Vector

The vector is similar to the list in that it stores a sequence of values. However it supports efficient random
access and modification, as well as addition and removal of elements at the end of the vector.

```clojure
>> [1 2 3]
[1 2 3]
>> (add 4 [1 2 3])
[1 2 3 4]
>> (remove-at 3 [1 2 3 4])
[1 2 3]
```

Vectors will support all functions of the sequence interface (see [Sequences](7-Sequences.md)) and in addition:

| Function                   | Description |
|:-------------------------- |:----------- |
| `(last vec)`               | Returns the last value in the vector. |
| `(add value vec)`          | Returns a new vector with `value` added at the end. |
| `(remove value vec)`       | Returns a new vector without the first occurrence of `value`. |
| `(insert index value vec)` | Returns a new vector with `value` inserted at postion `index`. |
| `(remove-at index vec)`    | Returns a new vector without the value at position `index`. |
| `(set index value vec)`    | Returns a new vector where position `index` is set to `value`. |
| `(get index vec)`          | Returns the value at the position `index`. Throws when out of range. |
| `(get index def vec)`      | Returns the value at the position `index`. If `index` is out of range returns `def`. |

Functionally `get` and `nth` are equivalent, but the usage of `get` is preferred.

## Dictionary

A dictionary is a container that associates keys with values. On construction, every even argument represents
a key and every odd argument represents the associated values. Dictionary support efficient lookup using `get`
and updates using `set`.

```clojure
>> {:a 1 :b 9}
{:a 1 :b 9}
>> (get :b {:a 1 :b 9})
9
>> (set :c 5 {:a 1 :b 9})
{:b 9 :c 5 :a 1}
```

Dictionaries will support all functions of the sequence interface (see [Sequences](7-Sequences.md)) and in addition:

| Function               | Description |
|:---------------------- |:----------- |
| `(contains key dict)`  | Checks if the dictionary contains `key`. |
| `(remove key dict)`    | Returns a new dictionary without the entry for `key`. |
| `(set key value dict)` | Returns a new dictionary where `key` is set to `value`. |
| `(get key dict)`       | Returns the value associated with `key`. Throws when not found. |
| `(get key def dict)`   | Returns the value associated with `key`. If not found returns `def`. |

Note that `contains` on a dictionary checks for the presence of a key, not for the presence of a value.

## Set

A set stores unique values in no particular order. It supports efficient lookups using `contains` and addition
of values using `add`.

```clojure
>> #{4 3 1}
#{1 3 4}
>> (contains 3 #{4 3 1})
true
>> (add 12 #{4 3 1})
#{1 3 4 12}
```

Sets will support all functions of the sequence interface (see [Sequences](7-Sequences.md)) and in addition:

| Function             | Description |
|:-------------------- |:----------- |
| `(add value set)`    | Returns a new set including `value`. |
| `(remove value set)` | Returns a new set without `value`. |

The implementation of `contains` on a set is optimized to be efficient.