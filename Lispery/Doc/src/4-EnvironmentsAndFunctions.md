﻿# Environments and Functions

All code in Lispery runs in the context of an environment. It is a dictionary that associates symbols
with their values.

Every environment can have a parent environment. For example during a function call, a new child
environment is generated. The function arguments are associated in that environment with their
respective symbols. After function execution, the environment is thrown away, so that those symbols
will not affect the parent environment.

## Def / Let

New associations in the current environment can be created with the `def` special form. It will
associate a symbol with a value. If the symbol is already defined in the current environment, it
will replace the previous value.

```clojure
>> (def a 123)
123
>> a
123
>> (def a 321)
321
>> a
321
```

Similarly `let` can be used to create a new environment that has potentially several symbols bound
to specific values:

```clojure
>> (let [a (+ 2 3)
         b 7]
     (+ a b))
12
```

## Functions

Anonymous functions can be created using the `fn` special form.

```clojure
>> (fn [x] (* 2 x))
<Function>
>> (map (fn [x] (* 2 x)) [1 2 3])
(2 4 6)
>> (def times-2 (fn [x] (* 2 x)))
<Function>
>> (times-2 3)
6
```

There is a shorthand `defn` for the combination of `def` and `fn`:

```clojure
>> (defn times-3 [x] (* x 3))
<Function>
>> (times-3 3)
9
```

The `#(...)` shorthand notation can also be used to define anonymous functions. You can use `%` to
refer to the first argument or `%1`, `%2` etc. to refer to the remaining arguments.

```clojure
>> (map #(* 2 %) [1 2 3])
(2 4 6)
>> (def add #(+ %1 %2))
<Function>
>> (add 3 4)
7
```

### Function names

The `fn` special form accepts an additional argument, which can be used to set the functions name:

```clojure
>> (def a (fn test [x] x))
<Function test>
```

The name can be retrieved later using `fn-name`.

```clojure
>> (fn-name a)
test
```

Note that `defn` will also automatically set the name:

```clojure
>> (defn test [x] x)
<Function test>
>> (fn-name test)
test
```

The purpose of the function name is mostly for debugging and to improve error messages.

## Assign!

Sometimes you want to change the value of a symbol that was defined in another environment. Take
the following example:

```clojure
>> (defn test [] (def a (inc a)))
<Function>
>> (def a 2)
2
>> (test)
3
>> a
2
```

As you can see, the value of `a` was not changed, because `def` created a new binding inside the
environment of `test`, instead of modifying the existing binding for `a`. Instead, this can be done
with the `assign!` special form:

```clojure
>> (defn test [] (assign! a (inc a)))
<Function>
>> (def a 2)
2
>> (test)
3
>> a
3
```

_Note_: usually this function is called `set!` in LISP. However in Lispery, `set` and `set!` are
used for dictionary / vector assignments.