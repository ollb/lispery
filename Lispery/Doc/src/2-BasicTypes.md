﻿# Basic Types

## Numbers

Numbers are stored as 64-bit signed integers by default. They can be prefixed with a sign.

```clojure
>> 123
123
>> +123
123
>> -123
-123
>> (type-of 123)
"System.Int64"
```

If a number contains a decimal point, it will be stored as a 64-bit floating point number.

```clojure
>> 123.55
123.55
>> +123.55
123.55
>> -123.55
-123.55
>> (type-of 123.55)
"System.Double"
```

## Booleans

There is a dedicated type for boolean values. The symbols `true` and `false` resolve to boolean values:

```clojure
>> true
true
>> (type-of true)
"System.Boolean"
>> (not true)
false
```

### Truthy and Falsey

Values are converted to booleans automatically when needed. The conversion rule is simply: `nil` and `false`
convert to `false` (the values are "falsey"). Everything else converts to `true` (the values are "truthy").
This can easily be tested with the `bool` function, which performs the conversion explicitly:

```clojure
>> (bool nil)
false
>> (bool false)
false
>> (bool 5)
true
>> (bool [1 2 3])
true
```

## Strings

Strings are enclosed in double quotes. Special characters can be escaped by prefixing them with a backslash:

```clojure
>> "test"
"test"
>> "test \"test\""
"test \"test\""
>> (print "test \"test\"")
test "test"
nil
```

## Keywords

Keywords are names that are prefixed with a colon. They are used mostly to identify keys in dictionaries or
for named arguments. Internally they are converted to a number during the parse process, making them efficient
for dictionary keys or comparisons in general.

```clojure
>> :test
:test
>> :_123
:_123
```

## Symbols

Symbols are identifiers that are usually used to refer to values stored in the current environment. They will
automatically evaluate to whatever they resolve to in the current environment. The symbol can be quoted using
`'` or `quote` if this is not desired.

```clojure
>> test123
Error: Symbol "test123" not found
>> 'test123
test123
>> (quote test123)
test123
```