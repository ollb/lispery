﻿# Mutable Data Types

Vectors, dictionaries and sets are also available as mutable data structures. They can be created by
prefixing the data structure with an exclamation mark.

```clojure
>> ![1 2 3]
![1 2 3]
>> !{:a 9 :b 10}
!{:a 9 :b 10}
>> !#{1 2 3}
!#{1 2 3}
```

They will not support the usual modification functions `add`, `set`, `remove` etc. and instead use
alternative functions `add!`, `set!` etc., to indicate that the data structure is modified in-place.

Mutable data structures need be associated with a symbol, in order to refer to them multiple times.

```clojure
>> (def a ![1 2 3])
![1 2 3]
>> a
![1 2 3]
>> (add! 4 a)
nil
>> a
![1 2 3 4]
>> (remove! 2 a)
nil
>> a
![1 3 4]
```

Mutable data structures will usually have better performance and use less memory, but are often
less suitable for functional code.

## MutableVector

MutableVector will support all functions of the sequence interface (see [Sequences](7-Sequences.md)) and in addition:

| Function                    | Description |
|:--------------------------- |:----------- |
| `(last vec)`                | Returns the last value in the vector. |
| `(add! value vec)`          | Adds `value` at the end of the vector. |
| `(remove! value vec)`       | Removes the first occurrence of `value` from the vector. |
| `(insert! index value vec)` | Inserts `value` at postion `index` into the vector. |
| `(remove-at! index vec)`    | Removes the value at position `index` from the vector. |
| `(set! index value vec)`    | Sets the position `index` set to `value`. |
| `(get index vec)`           | Returns the value at the position `index`. Throws when out of range. |
| `(get index def vec)`       | Returns the value at the position `index`. If `index` is out of range returns `def`. |
| `(sort! seq)`               | Sorts the vector. |
| `(sort! order seq)`         | Sorts the vector. `order` can be `:asc` or `:desc`. |

## MutableDictionary

MutableDictionary will support all functions of the sequence interface (see [Sequences](7-Sequences.md)) and in addition:

| Function                | Description |
|:----------------------- |:----------- |
| `(contains key dict)`   | Checks if the dictionary contains `key`. |
| `(remove! key dict)`    | Removes the entry for `key` from the dictionary. |
| `(set! key value dict)` | Sets the entry for `key` to `value`. |
| `(get key dict)`        | Returns the value associated with `key`. Throws when not found. |
| `(get key def dict)`    | Returns the value associated with `key`. If not found returns `def`. |

## MutableSet

MutableSet will support all functions of the sequence interface (see [Sequences](7-Sequences.md)) and in addition:

| Function              | Description |
|:--------------------- |:----------- |
| `(add! value set)`    | Adds `value` to the set. |
| `(remove! value set)` | Removes `value` from the set. |

The implementation of `contains` on a MutableSet is optimized to be efficient.