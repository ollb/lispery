﻿# .NET integration

## Simple functions

Extending the environment through .NET can be done by accessing the `Environment` property of the
`Interpreter`:

```csharp
Interpreter interpreter = new();
interpreter.Environment.DefineFunction("mul-5", (long? v) => v! * 5);
```

Please note that you should use `long` for integers and `double` for floating point values. Lispery
does not support `int` or `float`, for simplicity.

Functions can also be overloaded by defining them twice:

```csharp
Interpreter interpreter = new();
interpreter.Environment.DefineFunction("mul-5", (long? v) => v! * 5);
interpreter.Environment.DefineFunction("mul-5", (double? v) => v! * 5);
```

This will work as expected:

```clojure
>> (mul-5 3)
15
>> (mul-5 3.25)
16.25
```

## Variadic functions

You can define variadic functions, by using an array as the last argument:

```csharp
Interpreter interpreter = new();
interpreter.Environment.DefineFunction("sum-doubles", (long?[] v) => v.Sum(v => v! * 2));
```

Resulting in:

```clojure
>> (sum-doubles 1 2 3)
12
```

## Named arguments

A function with named arguments can be created using the variadic function support and the `Option`
utility class:

```csharp
Interpreter interpreter = new();

var env = interpreter.Environment;
var kwA = env.GetKeyword("a");
var kwB = env.GetKeyword("b");

env.DefineFunction("sum-ab", (object?[] args) =>
{
	Options opts = new Options(args);
	return opts.Get<long>(kwA, 1) + opts.Get<long>(kwB, 2);
});
```

This is providing the function `sum-ab` with two optional named arguments `:a` and `:b`:

```clojure
>> (sum-ab)
3
>> (sum-ab :b 10)
11
>> (sum-ab :a 5 :b 10)
15
```

## Native functions

Using `DefineFunction` will automatically provide a check for the function arity and argument types.
This can be customized by directly defining a `NativeFunction`:

```csharp
Interpreter interpreter = new();
interpreter.Environment.Define("test", new NativeFunction(Test));
// ...
object? Test(List args)
{
	// ...
}
```

The function will receive the argument values directly using a `List`. No additional checks for arity
or types are performed and are up to the function. Please note that such a function can not be overloaded
directly.

## Special forms

Special forms will receive unevaluated arguments and are therefore similar to macros. The implementing
function needs to call the `Interpreter` explicitly when an argument should be evaluated. For example
this will create an `unless` special form, that will execute the provided body expression only, if the
condition expression evaluates to `false`. Otherwise it will return `nil`:

```csharp
Interpreter interpreter = new();
interpreter.Environment.Define("unless", new SpecialForm("unless", Unless));
// ...
object? Unless(string name, List args, Environment env)
{
	Check.Arity(args, 2, name);
	var (condExpr, bodyExpr) = args.Match2();

	var cond = Conversion.To<bool>(env.Interpreter.Evaluate(condExpr, env));

	if (!cond)
	{
		return env.Interpreter.EvaluateContinue(bodyExpr, env);
	}
	
	return null;
}
```

Note that you should use `EvaluateContinue()`, whenever the result is just returned by the function.
This will enable the tail recursion optimization. If you need to interpret the result, use the
`Evaluate()` function instead.
