﻿# REPL

Lispery comes with a fully featured REPL (read-eval-print-loop) utility, that can be used to
directly execute typed commands and see their results. It is an ideal starting point for testing
out the language or for prototyping.

## Getting started

The easiest way to get started with the REPL is probably to run it using Docker:

```bash
# docker run -it registry.gitlab.com/ollb/lispery/lispery-repl:[VERSION]
```

Alternatively you can also download the pre-built generic packages for the REPL, unpack them and
run them using `dotnet`. You need to have [.NET Core 6.0](https://docs.microsoft.com/en-us/dotnet/core/install/linux)
installed for this.

```bash
# wget https://gitlab.com/api/v4/projects/33255027/packages/generic/LisperyREPL/[VERSION]/LisperyREPL-[VERSION].tar.gz
# tar xzf LisperyREPL-[VERSION].tar.gz
# cd LisperyREPL-[VERSION]
# ./LisperyREPL
```

## Commands

The REPL supports several commands:

| Command      | Description |
| :----------- | :---------- |
| \type        | Toggles the display of the .NET type for result values. |
| \time        | Toggles the display of the execution time and number of evaluation steps. |
| \reload      | Reloads the files that were provided to the REPL utility as command line arguments. |
| \reset       | Resets the environment and reloads all files. |
| \load [FILE] | Loads the given file into the current environment. |

Note that a reload can also be triggered by pressing `Ctrl+R`. A reset can be triggered by `Shift+Ctrl+R`.
