﻿using Lispery.Adapters;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Sequences;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	using MatchFun = Func<Environment, object?, bool>;

	/// <summary>
	/// Utility class that represents a "precompiled" match expression.
	/// </summary>
	public class Matcher
	{
		private readonly MatchFun matchFun;

		/// <summary>
		/// Creates a new matcher.
		/// </summary>
		internal Matcher(MatchFun matchFun)
		{
			this.matchFun = matchFun;
		}

		/// <summary>
		/// Determines the arity of a sequence match.
		/// </summary>
		public static int GetArity(Environment env, ISequence seq, out bool variadic)
		{
			int count = 0;
			variadic = false;

			foreach (var item in seq.GetEntries())
			{
				if (item is Symbol sym && sym == env.SymbolTable.SymAmpersand)
				{
					variadic = true;
					break;
				}

				count++;
			}

			return count;
		}

		/// <summary>
		/// Matches the given value and binds values in the given environment.
		/// </summary>
		public void Match(Environment env, object? value)
		{
			if (!TryMatch(env, value))
			{
				throw new ArgumentException("Match failure");
			}
		}

		/// <summary>
		/// Matches the given value and binds values in the given environment.
		/// </summary>
		public bool TryMatch(Environment env, object? value)
		{
			return matchFun(env, value);
		}
	}
}
