﻿using Lispery.Containers;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	/// <summary>
	/// Helper class to treat variadic arguments as named arguments.
	/// </summary>
	public class Options
	{
		private readonly Dictionary map;

		/// <summary>
		/// Creates new options from the given variadic arguments.
		/// </summary>
		public Options(object?[] args)
		{
			map = new Dictionary(args.GroupKeyValue()!);
		}

		/// <summary>
		/// Returns the underlying map.
		/// </summary>
		public Dictionary Map
		{
			get { return map; }
		}

		#region Keyword accessors
		/// <summary>
		/// Retrieves the argument with the given keyword. Throws if null or if it does not exist.
		/// </summary>
		public T Get<T>(Keyword keyword)
		{
			T? value = GetNullable<T>(keyword);
			ArgumentNullException.ThrowIfNull(value);
			return value;
		}

		/// <summary>
		/// Retrieves the argument with the given keyword or uses the fallback value. Throws if null.
		/// </summary>
		public T Get<T>(Keyword keyword, object fallback)
		{
			T? value = GetNullable<T>(keyword, fallback);
			ArgumentNullException.ThrowIfNull(value);
			return value;
		}

		/// <summary>
		/// Retrieves the argument with the given keyword. Throws if it does not exist.
		/// </summary>
		public T? GetNullable<T>(Keyword keyword)
		{
			return Conversion.ToNullable<T>(map.Get(keyword));
		}

		/// <summary>
		/// Retrieves the argument with the given keyword or uses the fallback value.
		/// </summary>
		public T? GetNullable<T>(Keyword keyword, object? fallback)
		{
			return Conversion.ToNullable<T>(map.Get(keyword, fallback));
		}
		#endregion
	}
}
