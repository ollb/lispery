﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	/// <summary>
	/// Function that supports dynamic dispatch by arity and types.
	/// </summary>
	internal class OverloadFunction : IFunction
	{
		private readonly OverloadResolver resolver = new();

		/// <summary>
		/// Creates a new dispatch function from the given native functions.
		/// </summary>
		public OverloadFunction(params NativeFunction[] funs) :
			this((IEnumerable<NativeFunction>)funs)
		{
		}

		/// <summary>
		/// Creates a new dispatch function from the given native functions.
		/// </summary>
		public OverloadFunction(IEnumerable<NativeFunction> funs)
		{
			foreach (var fun in funs)
			{
				resolver.AddOverload(fun);
			}
		}

		/// <summary>
		/// Returns the name of the callable or <c>null</c> if unknown.
		/// </summary>
		public string? Name
		{
			get { return Functions.Select(f => f.Name).Distinct().SingleOrDefault((string?)null); }
		}

		public FunctionType FunctionType
		{
			get { return FunctionType.Unknown; }
		}

		/// <summary>
		/// Returns the base functions in the function dispatch.
		/// </summary>
		public IEnumerable<NativeFunction> Functions
		{
			get { return resolver.Overloads; }
		}

		/// <summary>
		/// Adds the given function to the dispatch.
		/// </summary>
		public void AddFunction(NativeFunction fun)
		{
			resolver.AddOverload(fun);
		}

		/// <summary>
		/// Calls the function and returns the result.
		/// </summary>
		public object? Call(List args)
		{
			var overload = SelectOverload(args);
			return overload.Call(args);
		}

		/// <summary>
		/// Selects the overload function for the given arguments.
		/// </summary>
		private NativeFunction SelectOverload(List args)
		{
			return resolver.Resolve(args);
		}

		public override string ToString()
		{
			StringBuilder sb = new();
			bool first = true;

			foreach (var fun in Functions)
			{
				if (!first)
				{
					sb.AppendLine();
				}

				sb.Append(fun.ToString());
				first = false;
			}

			return sb.ToString();
		}
	}
}
