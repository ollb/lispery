﻿using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	using MatchFun = Func<Environment, object?, bool>;

	/// <summary>
	/// Describes how symbols should be matched.
	/// </summary>
	public enum MatchMode
	{
		/// <summary>Symbols should be defined in the input environment.</summary>
		Define,
		/// <summary>Symbols should be assigned in the input environment.</summary>
		Assign
	}

	/// <summary>
	/// Parses a match expression and turns it into a <c>Matcher</c>.
	/// </summary>
	public class MatchParser
	{
		/// <summary>
		/// Gets or sets how symbols should be matched.
		/// </summary>
		public MatchMode MatchMode { get; init; }

		/// <summary>
		/// The environment used to resolve symbols.
		/// </summary>
		public Environment ParseEnvironment { get; init; }

		/// <summary>
		/// Creates a new match parser.
		/// </summary>
		public MatchParser(MatchMode mode, Environment parseEnv)
		{
			MatchMode = mode;
			ParseEnvironment = parseEnv;
		}

		/// <summary>
		/// Parses the given match expression.
		/// </summary>
		public Matcher Parse(object? matchExpr)
		{
			return new Matcher(ParseImpl(matchExpr));
		}

		/// <summary>
		/// Parses the given sequence.
		/// </summary>
		public Matcher ParseSequence(ISequence sequence)
		{
			return new Matcher(ParseSequenceImpl<ISequence>(sequence));
		}

		/// <summary>
		/// Parses the given match expression.
		/// </summary>
		private MatchFun ParseImpl(object? matchExpr)
		{
			return matchExpr switch
			{
				Symbol symbol         => ParseSymbol(symbol),
				List list             => ParseFunction(list),
				Vector vector         => ParseSequenceImpl<Vector>(vector),
				MutableVector vector  => ParseSequenceImpl<MutableVector>(vector),
				Dictionary map        => ParseDictionary<Dictionary>(map),
				MutableDictionary map => ParseDictionary<MutableDictionary>(map),
				_                     => (inputEnv, inputValue) => object.Equals(inputValue, matchExpr),
			};
		}

		/// <summary>
		/// Parses the given match expression symbol.
		/// </summary>
		private MatchFun ParseSymbol(Symbol matchSymbol)
		{
			if (matchSymbol.Name == "_")
			{
				return (_, _) => true;
			}

			switch (MatchMode)
			{
				case MatchMode.Define:
					return (inputEnv, inputValue) =>
					{
						inputEnv.Define(matchSymbol, inputValue);
						return true;
					};

				case MatchMode.Assign:
					return (inputEnv, inputValue) =>
					{
						inputEnv.Assign(matchSymbol, inputValue);
						return true;
					};
			};

			throw new NotImplementedException();
		}

		/// <summary>
		/// Parses the given match expression vector.
		/// </summary>
		private MatchFun ParseSequenceImpl<T>(ISequence matchSeq)
			where T : ISequence
		{
			var matchers = new List<MatchFun>();
			MatchFun? restMatcher = null;

			int count = matchSeq.Count();

			for (int i = 0; i < count; i++)
			{
				var expr = matchSeq.Nth(i);

				if (expr is Symbol sym && sym.Name == "&")
				{
					// After "&" create a matcher for the rest of the sequence.
					if (count != i + 2)
					{
						throw new ArgumentException("& can only match against one expression");
					}

					i++;
					restMatcher = ParseImpl(matchSeq.Nth(i));
				}
				else
				{
					// Create a matcher for each component of the vector.
					matchers.Add(ParseImpl(expr));
				}
			}

			int arity = matchers.Count;
			bool variadic = restMatcher != null;

			return (env, value) =>
			{
				T typedValue = Conversion.To<T>(value);
				ISequence seq = typedValue;

				// Match every matcher individually against the values in the sequence.
				for (int i = 0; i < matchers.Count; i++)
				{
					if (!seq.GetEntries().Any())
					{
						return false;
					}

					if (!matchers[i](env, seq.First()))
					{
						return false;
					}

					seq = seq.Rest();
				}

				// Match the rest against the rest matcher, if present.
				if (restMatcher != null)
				{
					restMatcher(env, seq);
				}
				else if (seq.GetEntries().Any())
				{
					return false;
				}

				return true;
			};
		}

		private MatchFun ParseFunction(List args)
		{
			var (funExpr, rest) = args.Match1R();
			var symbol = Conversion.To<Symbol>(funExpr);
			var symbolTable = ParseEnvironment.SymbolTable;

			// TODO: optimize performance. Use array?
			if (symbol == symbolTable.SymSeq)
			{
				return ParseSequenceImpl<ISequence>(rest);
			}
			else if (symbol == symbolTable.SymWhen)
			{
				return ParseWhen(rest);
			}
			else if (symbol == symbolTable.SymInt)
			{
				return ParseTypeMatch<long>(rest);
			}
			else if (symbol == symbolTable.SymFloat)
			{
				return ParseTypeMatch<double>(rest);
			}
			else if (symbol == symbolTable.SymString)
			{
				return ParseTypeMatch<string>(rest);
			}
			else if (symbol == symbolTable.SymBool)
			{
				return ParseTypeMatch<bool>(rest);
			}
			else if (symbol == symbolTable.SymVector)
			{
				return ParseSequenceImpl<Vector>(rest);
			}
			else if (symbol == symbolTable.SymVectorMut)
			{
				return ParseSequenceImpl<MutableVector>(rest);
			}
			else if (symbol == symbolTable.SymList)
			{
				return ParseSequenceImpl<List>(rest);
			}
			else if (symbol == symbolTable.SymQuote)
			{
				return ParseQuote(rest);
			}
			else if (symbol == symbolTable.SymDict)
			{
				return ParseDictionary<Dictionary>(rest.GroupKeyValue()!);
			}
			else if (symbol == symbolTable.SymDictMut)
			{
				return ParseDictionary<MutableDictionary>(rest.GroupKeyValue()!);
			}
			else if (symbol == symbolTable.SymGtDict)
			{
				Check.Arity(rest, 1);
				return ConvertToDictionary(ParseImpl(rest.First()));
			}
			else if (symbol == symbolTable.SymEquals)
			{
				return ParseSymbolMatch(rest);
			}
			else if (symbol == symbolTable.SymKeyDict)
			{
				return ParseKeyDictionary(rest);
			}
			else if (symbol == symbolTable.SymGtKeyDict)
			{
				return ConvertToDictionary(ParseKeyDictionary(rest));
			}
			else
			{
				throw new ArgumentException("Invalid match function");
			}
		}

		private MatchFun ParseWhen(List args)
		{
			Check.Arity(args, 2, "when");

			var (condExpr, expr) = args.Match2();
			var matchFun = ParseImpl(expr);

			return (env, value) =>
			{
				if (!matchFun(env, value))
				{
					return false;
				}

				var result = env.Interpreter.Evaluate(condExpr, env);

				if (Conversion.ToCallableOrNull(result) is IFunction fun)
				{
					return Conversion.To<bool>(fun.Call(value));
				}
				
				return Conversion.To<bool>(result);
			};
		}

		private MatchFun ParseTypeMatch<T>(List args)
		{
			Check.Arity(args, 1, "type-match");

			object? matchExpr = args.First();
			var matcher = ParseImpl(matchExpr);

			return (env, value) =>
			{
				if (value is not T)
				{
					return false;
				}

				matcher(env, value);
				return true;
			};
		}

		private static MatchFun ParseQuote(List args)
		{
			Check.Arity(args, 1, "quote");

			object? matchExpr = args.First();
			var symbol = Conversion.To<Symbol>(matchExpr);

			return (env, value) =>
			{
				return object.Equals(value, symbol);
			};
		}

		/// <summary>
		/// Parses the given match expression tree map.
		/// </summary>
		private MatchFun ParseDictionary<T>(IEnumerable<KeyValuePair<object, object?>> entries)
			where T : ISupportTryGetValue
		{
			var matchers = new Dictionary<object, MatchFun>();

			foreach (var entry in entries)
			{
				matchers[entry.Key] = ParseImpl(entry.Value);
			}

			return (env, value) => MatchDictionary<T>(matchers, env, value);
		}

		/// <summary>
		/// Parses the given match expression key map.
		/// </summary>
		private MatchFun ParseKeyDictionary(IEnumerable<object?> entries)
		{
			var matchers = new Dictionary<object, MatchFun>();
			var builder = new Dictionary.Builder();

			foreach (var entry in entries)
			{
				if (entry is Vector vector)
				{
					if (vector.Count != 2)
					{
						throw new ArgumentException("Invalid match expression");
					}

					Symbol symbol = Conversion.To<Symbol>(vector[0]);
					Keyword keyword = ParseEnvironment.GetKeyword(symbol.Name);
					builder.Set(keyword, vector[1]);
					matchers[keyword] = ParseImpl(symbol);
				}
				else
				{
					Symbol symbol = Conversion.To<Symbol>(entry);
					Keyword keyword = ParseEnvironment.GetKeyword(symbol.Name);
					matchers[keyword] = ParseImpl(symbol);
				}
			}

			var defaults = builder.ToImmutable();

			return (env, value) =>
			{
				Dictionary map = Conversion.To<Dictionary>(value);

				foreach (var entry in defaults)
				{
					if (!map.Contains(entry.Key))
					{
						map = map.Set(entry.Key, entry.Value);
					}
				}

				return MatchDictionary<Dictionary>(matchers, env, map);
			};
		}

		/// <summary>
		/// Matches a dictionary against the given matcher functions.
		/// </summary>
		private static bool MatchDictionary<T>(Dictionary<object, MatchFun> matchers, Environment env, object? value)
			where T : ISupportTryGetValue
		{
			T typedValue = Conversion.To<T>(value);

			foreach (var entry in matchers)
			{
				if (!typedValue.TryGetValue(entry.Key, out object? entryValue))
				{
					return false;
				}

				if (!entry.Value(env, entryValue))
				{
					return false;
				}
			}

			return true;
		}

		private static MatchFun ConvertToDictionary(MatchFun fun)
		{
			return (env, value) =>
			{
				if (value is Dictionary map)
				{
					return fun(env, map);
				}

				var seq = Conversion.To<ISequence>(value);
				map = new Dictionary(seq.GetEntries().GroupKeyValue()!);

				return fun(env, map);
			};
		}

		/// <summary>
		/// Parses a match on symbol contents.
		/// </summary>
		private MatchFun ParseSymbolMatch(List args)
		{
			Check.Arity(args, 1, "=");

			Symbol symbol = Conversion.To<Symbol>(args.First());
			object? matchValue = ParseEnvironment.Resolve(symbol);

			return (env, value) => object.Equals(value, matchValue);
		}
	}
}
