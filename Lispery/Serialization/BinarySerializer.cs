﻿using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lispery.Serialization
{
	/// <summary>
	/// Supports serialization to and from binary data.
	/// </summary>
	public class BinarySerializer
	{
		private enum ValueType : byte
		{
			Unknown = 0,
			Long = 1,
			Double = 2,
			String = 3,
			Bool = 4,
			Null = 5,
			Symbol = 6,
			Keyword = 7,
			Regex = 8,
			List = 9,
			Vector = 10,
			TreeMap = 11,
			HashSet = 12,
			KeyValue = 13
		}

		/// <summary>
		/// Serializes the given object to the given binary writer.
		/// </summary>
		public void Serialize(object? obj, BinaryWriter writer)
		{
			if (obj == null)
			{
				writer.Write((byte)ValueType.Null);
				return;
			}

			switch (obj)
			{
				case long num:
					writer.Write((byte)ValueType.Long);
					writer.Write(num);
					break;

				case double num:
					writer.Write((byte)ValueType.Double);
					writer.Write(num);
					break;

				case string str:
					writer.Write((byte)ValueType.String);
					writer.Write(str);
					break;

				case bool value:
					writer.Write((byte)ValueType.Bool);
					writer.Write(value);
					break;

				case Symbol symbol:
					writer.Write((byte)ValueType.Symbol);
					writer.Write(symbol.Name);
					break;

				case Keyword keyword:
					writer.Write((byte)ValueType.Keyword);
					writer.Write(keyword.Name);
					break;

				case Regex regex:
					writer.Write((byte)ValueType.Regex);
					writer.Write((int)regex.Options);
					writer.Write(regex.ToString());
					break;

				case List list:
					writer.Write((byte)ValueType.List);
					writer.Write((int)list.Count);

					foreach (var item in list)
					{
						Serialize(item, writer);
					}
					break;

				case Vector vector:
					writer.Write((byte)ValueType.Vector);
					writer.Write((int)vector.Count);

					foreach (var item in vector)
					{
						Serialize(item, writer);
					}
					break;

				case Dictionary treeMap:
					writer.Write((byte)ValueType.TreeMap);
					writer.Write((int)treeMap.Count);

					foreach (var entry in treeMap)
					{
						Serialize(entry.Key, writer);
						Serialize(entry.Value, writer);
					}
					break;

				case Set hashSet:
					writer.Write((byte)ValueType.HashSet);
					writer.Write((int)hashSet.Count);

					foreach (var value in hashSet)
					{
						Serialize(value, writer);
					}
					break;

				case KeyValuePair<object?, object?> pair:
					writer.Write((byte)ValueType.KeyValue);
					Serialize(pair.Key, writer);
					Serialize(pair.Value, writer);
					break;

				default:
					throw new InvalidOperationException("Can not serialize value");
			}
		}

		/// <summary>
		/// Deserializes an object from the given binary reader.
		/// </summary>
		public object? Deserialize(BinaryReader reader)
		{
			byte type = reader.ReadByte();

			return (ValueType)type switch
			{
				ValueType.Long     => reader.ReadInt64(),
				ValueType.Double   => reader.ReadDouble(),
				ValueType.String   => reader.ReadString(),
				ValueType.Bool     => reader.ReadBoolean(),
				ValueType.Null     => null,
				ValueType.Symbol   => new LazySymbol(reader.ReadString()),
				ValueType.Keyword  => new LazyKeyword(reader.ReadString()),
				ValueType.Regex    => DeserializeRegex(reader),
				ValueType.List     => DeserializeList(reader),
				ValueType.Vector   => DeserializeVector(reader),
				ValueType.TreeMap  => DeserializeTreeMap(reader),
				ValueType.HashSet  => DeserializeHashSet(reader),
				ValueType.KeyValue => DeserializeKeyValue(reader),
				_                  => throw new InvalidOperationException("Can not deserialize value")
			};
		}

		/// <summary>
		/// Deserializes a regular expression object.
		/// </summary>
		private static object DeserializeRegex(BinaryReader reader)
		{
			int options = reader.ReadInt32();
			string pattern = reader.ReadString();
			return new Regex(pattern, (RegexOptions)options);
		}

		/// <summary>
		/// Deserializes a linked list object.
		/// </summary>
		private object DeserializeList(BinaryReader reader)
		{
			var builder = new List.Builder();
			int length = reader.ReadInt32();

			for (int i = 0; i < length; i++)
			{
				builder.Add(Deserialize(reader));
			}

			return builder.ToImmutable();
		}

		/// <summary>
		/// Deserializes a vector object.
		/// </summary>
		private object DeserializeVector(BinaryReader reader)
		{
			var builder = new Vector.Builder();
			int length = reader.ReadInt32();

			for (int i = 0; i < length; i++)
			{
				builder.Add(Deserialize(reader));
			}

			return builder.ToImmutable();
		}

		/// <summary>
		/// Deserializes a hash set object.
		/// </summary>
		private object DeserializeHashSet(BinaryReader reader)
		{
			var builder = new Set.Builder();
			int length = reader.ReadInt32();

			for (int i = 0; i < length; i++)
			{
				builder.Add(Deserialize(reader));
			}

			return builder.ToImmutable();
		}

		/// <summary>
		/// Deserializes a tree map object.
		/// </summary>
		private object DeserializeTreeMap(BinaryReader reader)
		{
			var builder = new Dictionary.Builder();
			int length = reader.ReadInt32();

			for (int i = 0; i < length; i++)
			{
				object? key = Deserialize(reader);
				object? value = Deserialize(reader);
				builder.Set(key!, value);
			}

			return builder.ToImmutable();
		}

		/// <summary>
		/// Deserializes a key value pair.
		/// </summary>
		private object DeserializeKeyValue(BinaryReader reader)
		{
			object? key = Deserialize(reader);
			object? value = Deserialize(reader);
			return KeyValuePair.Create(key, value);
		}
	}
}
