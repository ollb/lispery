﻿using Lispery.Adapters;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Sequences;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	/// <summary>
	/// Utility class for interpreting for expressions.
	/// </summary>
	internal class ForInterpreter
	{
		/// <summary>
		/// Captures a nested step in the loop comprehension.
		/// </summary>
		abstract record class InterpretStep
		{
			/// <summary>
			/// The next step in the queue.
			/// </summary>
			public InterpretStep? Next { get; set; }

			/// <summary>
			/// Evaluates the step.
			/// </summary>
			public abstract IEnumerable<object?> Evaluate(Environment env);
		}

		/// <summary>
		/// Represents the iteration of a sequence.
		/// </summary>
		record class IterateSequenceStep(object? MatchExpr, object? Expr) : InterpretStep
		{
			public override IEnumerable<object?> Evaluate(Environment env)
			{
				Environment boundEnv = new(env);
				ISequence sequence = Conversion.To<ISequence>(env.Interpreter.Evaluate(Expr, env));

				// Iterate through all values in the sequence and bind them.
				foreach (var value in sequence.GetEntries())
				{
					MatchParser parser = new(MatchMode.Define, env);
					Matcher matcher = parser.Parse(MatchExpr);
					matcher.Match(boundEnv, value);

					foreach (var nextValue in Next!.Evaluate(boundEnv))
					{
						yield return nextValue;
					}
				}
			}
		}

		/// <summary>
		/// Represents a where clause.
		/// </summary>
		record class WhenStep(object? Expr) : InterpretStep
		{
			public override IEnumerable<object?> Evaluate(Environment env)
			{
				if (Conversion.To<bool>(env.Interpreter.Evaluate(Expr, env)))
				{
					return Next!.Evaluate(env);
				}

				return Enumerable.Empty<object?>();
			}
		}

		/// <summary>
		/// Represents a let clause.
		/// </summary>
		record class LetStep(Vector Bindings) : InterpretStep
		{
			public override IEnumerable<object?> Evaluate(Environment env)
			{
				Environment boundEnv = new(env);

				foreach (var entry in Bindings.GroupKeyValue())
				{
					MatchParser parser = new(MatchMode.Define, env);
					Matcher matcher = parser.Parse(entry.Key);
					object? value = env.Interpreter.Evaluate(entry.Value, env);
					matcher.Match(boundEnv, value);
				}

				return Next!.Evaluate(boundEnv);
			}
		}

		/// <summary>
		/// Represents the for loop body. Always the last step.
		/// </summary>
		record class BodyStep(List Body) : InterpretStep
		{
			public override IEnumerable<object?> Evaluate(Environment env)
			{
				object? result = null;

				foreach (var expr in Body)
				{
					result = env.Interpreter.Evaluate(expr, env);
				}

				yield return result;
			}
		}

		private readonly List<InterpretStep> steps = new();

		/// <summary>
		/// Creates a new for interpreter.
		/// </summary>
		public ForInterpreter(KeywordTable keywordTable, string name, List args)
		{
			Check.MinArity(args, 2, name);

			// Get the clauses.
			var (clausesExpr, body) = args.Match1R();
			Vector clauses = Conversion.To<Vector>(clausesExpr);

			foreach (var clause in clauses.GroupKeyValue())
			{
				// Interpret special clauses and turn them into steps.
				if (clause.Key is Keyword kw)
				{
					if (kw == keywordTable.KwWhen)
					{
						steps.Add(new WhenStep(clause.Value));
						continue;
					}
					else if (kw == keywordTable.KwLet)
					{
						Vector bindings = Conversion.To<Vector>(clause.Value);
						steps.Add(new LetStep(bindings));
						continue;
					}
				}
				
				// By default interpret as iteration.
				steps.Add(new IterateSequenceStep(clause.Key, clause.Value));
			}

			steps.Add(new BodyStep(body));

			// Chain the steps together.
			for (int i = 1; i < steps.Count; i++)
			{
				steps[i - 1].Next = steps[i];
			}
		}

		/// <summary>
		/// Evaluates the for expression.
		/// </summary>
		public object? Evaluate(Environment env)
		{
			return new LazySequence(steps.First().Evaluate(new Environment(env)));
		}
	}
}
