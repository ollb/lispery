﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lispery
{
	public class InterruptEventArgs : EventArgs
	{
		public long EvaluatedSteps { get; private set; }

		public InterruptEventArgs(long evaluatedSteps)
		{
			EvaluatedSteps = evaluatedSteps;
		}
	}
}
