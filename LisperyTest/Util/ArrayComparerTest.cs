﻿using Lispery.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest.Util
{
	public class ArrayComparerTest
	{
		[Fact]
		public void TestEquals()
		{
			ArrayComparer<int> comparer = new();

			Assert.True(comparer.Equals(null, null));
			Assert.False(comparer.Equals(null, Array.Empty<int>()));
			Assert.False(comparer.Equals(Array.Empty<int>(), null));
			Assert.False(comparer.Equals(Array.Empty<int>(), new int[] { 1 }));
			Assert.False(comparer.Equals(new int[] { 1, 2, 3 }, new int[] { 3, 2, 1 }));
			Assert.True(comparer.Equals(new int[] { 1, 2, 3 }, new int[] { 1, 2, 3 }));
		}

		[Fact]
		public void TestHashCode()
		{
			ArrayComparer<object?> comparer = new();

			int a = comparer.GetHashCode(new object?[] { 1, null, 3 });
			int b = comparer.GetHashCode(new object?[] { 1, null, 3 });
			Assert.Equal(a, b);
		}
	}
}
