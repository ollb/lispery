﻿using Lispery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest
{
	public class FunctionTest
	{
		private readonly Interpreter interpreter = new();

		[Fact]
		public void TestMultiFormFunction()
		{
			interpreter.Evaluate(@"(defn test ([a] a) ([a b] (+ a b)))");

			Assert.Equal((long)1, interpreter.Evaluate(@"(test 1)"));
			Assert.Equal((long)3, interpreter.Evaluate(@"(test 1 2)"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(test 1 2 3)"));
		}
	}
}
