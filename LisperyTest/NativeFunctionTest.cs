﻿using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest
{
	public class NativeFunctionTest
	{
		[Fact]
		public void TestMissingTypeInfo()
		{
			var fun = new NativeFunction(args => null);

			Assert.False(fun.HasTypeInfo);
			Assert.True(fun.Arity < 0);
			Assert.Throws<InvalidOperationException>(() => fun.GetArgumentType(0));
			Assert.Throws<InvalidOperationException>(() => fun.IsVariadic);
			Assert.Throws<InvalidOperationException>(() => fun.SupportsArity(0));
		}

		[Fact]
		public void TestTypeInfo()
		{
			var fun = new NativeFunction(null, new Type[] { typeof(int) }, null, args => null);

			Assert.True(fun.HasTypeInfo);
			Assert.Equal(1, fun.Arity);
			Assert.Equal(typeof(int), fun.GetArgumentType(0));
			Assert.Null(fun.GetArgumentType(1));
			Assert.False(fun.IsVariadic);
			Assert.False(fun.SupportsArity(0));
			Assert.True(fun.SupportsArity(1));
			Assert.False(fun.SupportsArity(2));

			fun = new NativeFunction(null, new Type[] { typeof(int) }, typeof(long), args => null);

			Assert.True(fun.HasTypeInfo);
			Assert.Equal(1, fun.Arity);
			Assert.Equal(typeof(int), fun.GetArgumentType(0));
			Assert.Equal(typeof(long), fun.GetArgumentType(1));
			Assert.Equal(typeof(long), fun.GetArgumentType(2));
			Assert.True(fun.IsVariadic);
			Assert.False(fun.SupportsArity(0));
			Assert.True(fun.SupportsArity(1));
			Assert.True(fun.SupportsArity(2));
		}
	}
}
