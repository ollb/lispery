﻿using Lispery;
using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest
{
	public class ParserTest
	{
		[Fact]
		public void TestParsePrimitives()
		{
			SymbolTable symbolTable = new();
			KeywordTable keywordTable = new();
			Parser parser = new(symbolTable, keywordTable);
			ExpressionList seq;

			seq = parser.Parse(new StringReader(@"7"));
			Assert.Equal(new object[] { (long)7 }, seq);

			seq = parser.Parse(new StringReader(@"+7"));
			Assert.Equal(new object[] { (long)7 }, seq);

			seq = parser.Parse(new StringReader(@"-7"));
			Assert.Equal(new object[] { (long)-7 }, seq);

			seq = parser.Parse(new StringReader(@"0.0625"));
			Assert.Equal(new object[] { (double)0.0625 }, seq);

			seq = parser.Parse(new StringReader(@"+0.0625"));
			Assert.Equal(new object[] { (double)0.0625 }, seq);

			seq = parser.Parse(new StringReader(@"-0.0625"));
			Assert.Equal(new object[] { (double)-0.0625 }, seq);

			seq = parser.Parse(new StringReader(@"""test"""));
			Assert.Equal(new object[] { "test" }, seq);

			seq = parser.Parse(new StringReader(@"""te\""s\\t"""));
			Assert.Equal(new object[] { "te\"s\\t" }, seq);

			seq = parser.Parse(new StringReader(@":keyword"));
			Assert.Equal(new object[] { keywordTable.Get("keyword") }, seq);

			seq = parser.Parse(new StringReader(@"symbol"));
			Assert.Equal(new object[] { symbolTable.Get("symbol") }, seq);

			seq = parser.Parse(new StringReader(@"#""test"""));
			Assert.Single(seq);
			Assert.Equal("test", ((Regex)seq.First()!).ToString());
		}

		[Fact]
		public void TestParseContainers()
		{
			SymbolTable symbolTable = new();
			KeywordTable keywordTable = new();

			var resultSeq = new object?[]
			{
				symbolTable.Get("a"),
				symbolTable.Get("b"),
				symbolTable.Get("c")
			};

			Parser parser = new(symbolTable, keywordTable);
			ExpressionList seq;

			seq = parser.Parse(new StringReader(@"(a b c)"));
			Assert.Single(seq);

			var list = seq.First() as List;
			Assert.NotNull(list);
			Assert.Equal(resultSeq, list);

			seq = parser.Parse(new StringReader(@"[a b c]"));
			Assert.Single(seq);

			var vector = seq.First() as Vector;
			Assert.NotNull(vector);
			Assert.Equal(resultSeq, vector);

			seq = parser.Parse(new StringReader(@"#{a b c}"));
			Assert.Single(seq);

			var hashSet = seq.First() as Set;
			Assert.NotNull(hashSet);
			Assert.Contains(symbolTable.Get("a"), hashSet);
			Assert.Contains(symbolTable.Get("b"), hashSet);
			Assert.Contains(symbolTable.Get("c"), hashSet);
			Assert.Equal(3, hashSet!.Count);

			seq = parser.Parse(new StringReader(@"{a 0 b 9 b 1 c 2}"));
			Assert.Single(seq);

			var treeMap = seq.First() as Dictionary;
			Assert.NotNull(treeMap);
			Assert.Equal((long)0, treeMap!.Get(symbolTable.Get("a")));
			Assert.Equal((long)1, treeMap!.Get(symbolTable.Get("b")));
			Assert.Equal((long)2, treeMap!.Get(symbolTable.Get("c")));
			Assert.Equal(3, treeMap!.Count);
		}

		[Fact]
		public void TestParseFunction1()
		{
			SymbolTable symbolTable = new();
			KeywordTable keywordTable = new();
			Parser parser = new(symbolTable, keywordTable);
			ExpressionList seq;

			// (fn [%1] (a %1 %1))
			seq = parser.Parse(new StringReader(@"#(a % %)"));

			Assert.Single(seq);

			var list = seq.First() as List;
			Assert.NotNull(list);

			Assert.Equal(symbolTable.SymFn, list!.ElementAt(0));

			var args = list!.ElementAt(1) as Vector;
			Assert.NotNull(args);
			Assert.Equal(new object[] { symbolTable.Get("%1") }, args);

			var body = list!.ElementAt(2) as List;
			Assert.NotNull(body);
			Assert.Equal(new object[] { symbolTable.Get("a"), symbolTable.Get("%1"), symbolTable.Get("%1") }, body);
		}

		[Fact]
		public void TestParseFunction2()
		{
			SymbolTable symbolTable = new();
			KeywordTable keywordTable = new();
			Parser parser = new(symbolTable, keywordTable);
			ExpressionList seq;

			// (fn [%1] (a %1 %1))
			seq = parser.Parse(new StringReader(@"#(a %2 %1 %)"));

			Assert.Single(seq);

			var list = seq.First() as List;
			Assert.NotNull(list);

			Assert.Equal(symbolTable.SymFn, list!.ElementAt(0));

			var args = list!.ElementAt(1) as Vector;
			Assert.NotNull(args);

			Assert.Equal(new object[]
			{
				symbolTable.Get("%1"),
				symbolTable.Get("%2")
			}, args);

			var body = list!.ElementAt(2) as List;
			Assert.NotNull(body);

			Assert.Equal(new object[]
			{
				symbolTable.Get("a"),
				symbolTable.Get("%2"),
				symbolTable.Get("%1"),
				symbolTable.Get("%1")
			}, body);
		}

		[Fact]
		public void TestParseNestedFunction()
		{
			// Throw on nested function.
			Parser parser = new(new SymbolTable(), new KeywordTable());
			Assert.Throws<FormatException>(() => parser.Parse(new StringReader(@"#(a #(b))")));
		}

		[Fact]
		public void TestParseTestCode()
		{
			string script = @"
				; a
				(a b
				 ; b
				 c [1 2 3] #{d e f} ""test"")
				; test
				{:a 0 :b 1 :c 2 
				 ; y
				 }
				(a (b c) ((d))
				 ; x
				 )
			";

			SymbolTable symbolTable = new();
			KeywordTable keywordTable = new();
			Parser parser = new(symbolTable, keywordTable);
			ExpressionList seq = parser.Parse(new StringReader(script));

			Assert.Equal(3, seq.Count());

			var list1 = seq.ElementAt(0) as List;
			Assert.NotNull(list1);
			Assert.Equal(symbolTable.Get("a"), list1!.ElementAt(0));
			Assert.Equal("test", list1!.ElementAt(5));

			var vector = list1!.ElementAt(3) as Vector;
			Assert.NotNull(vector);
			Assert.Equal((long)1, vector!.ElementAt(0));

			var hashSet = list1!.ElementAt(4) as Set;
			Assert.NotNull(hashSet);
			Assert.Contains(symbolTable.Get("f"), hashSet);

			var treeMap = seq.ElementAt(1) as Dictionary;
			Assert.NotNull(treeMap);
			Assert.Equal((long)1, treeMap!.Get(keywordTable.Get("b")));

			var list2 = seq.ElementAt(2) as List;
			Assert.NotNull(list2);

			var list3 = list2!.ElementAt(2) as List;
			Assert.NotNull(list3);

			var list4 = list3!.ElementAt(0) as List;
			Assert.NotNull(list4);
			Assert.Equal(symbolTable.Get("d"), list4!.ElementAt(0));
		}
	}
}
