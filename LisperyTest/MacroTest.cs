﻿using Lispery;
using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest
{
	public class MacroTest
	{
		private readonly Interpreter interpreter = new();
		private readonly Lispery.Environment env;

		public MacroTest()
		{
			env = interpreter.Environment;
		}

		[Fact]
		public void TestQuote()
		{
			var res = interpreter.Evaluate(@"'(+ 1 2 (- 3 4))");

			List? outer = res as List;
			Assert.NotNull(outer);
			Assert.Equal(4, outer!.Count);
			Assert.Equal(env.GetSymbol("+"), outer.Nth(0));

			List? inner = outer.Nth(3) as List;
			Assert.NotNull(inner);
			Assert.Equal(3, inner!.Count);
			Assert.Equal(env.GetSymbol("-"), inner.Nth(0));
			Assert.Equal((long)3, inner.Nth(1));
			Assert.Equal((long)4, inner.Nth(2));
		}

		[Fact]
		public void TestSyntaxQuote()
		{
			var res = interpreter.Evaluate(@"`(+ 1 2 (- 3 4))");

			List? outer = res as List;
			Assert.NotNull(outer);
			Assert.Equal(4, outer!.Count);
			Assert.Equal(env.GetSymbol("+"), outer.Nth(0));

			List? inner = outer.Nth(3) as List;
			Assert.NotNull(inner);
			Assert.Equal(3, inner!.Count);
			Assert.Equal(env.GetSymbol("-"), inner.Nth(0));
			Assert.Equal((long)3, inner.Nth(1));
			Assert.Equal((long)4, inner.Nth(2));
		}

		[Fact]
		public void TestUnquote()
		{
			var res = interpreter.Evaluate(@"`(+ 1 2 ~(- 3 4) {:a ~(+ 4 9) :b (+ 9 8)})");

			List? outer = res as List;
			Assert.NotNull(outer);
			Assert.Equal(5, outer!.Count);
			Assert.Equal(env.GetSymbol("+"), outer.Nth(0));
			Assert.Equal((long)1, outer.Nth(1));
			Assert.Equal((long)2, outer.Nth(2));
			Assert.Equal((long)-1, outer.Nth(3));

			Dictionary? treeMap = outer.Nth(4) as Dictionary;
			Assert.NotNull(treeMap);
			Assert.Equal((long)13, treeMap!.Get(env.GetKeyword("a")));

			List? inner = treeMap.Get(env.GetKeyword("b")) as List;
			Assert.NotNull(inner);
			Assert.Equal(3, inner!.Count);
			Assert.Equal(env.GetSymbol("+"), inner.Nth(0));
			Assert.Equal((long)9, inner.Nth(1));
			Assert.Equal((long)8, inner.Nth(2));
		}

		[Fact]
		public void TestUnquoteSplicingList()
		{
			var res = interpreter.Evaluate(@"`(+ 1 ~@(list 9 8) 2 ~@[] ~@[3 4])");

			List? list = res as List;
			Assert.NotNull(list);
			Assert.Equal(7, list!.Count);
			Assert.Equal(env.GetSymbol("+"), list.Nth(0));
			Assert.Equal((long)1, list.Nth(1));
			Assert.Equal((long)9, list.Nth(2));
			Assert.Equal((long)8, list.Nth(3));
			Assert.Equal((long)2, list.Nth(4));
			Assert.Equal((long)3, list.Nth(5));
			Assert.Equal((long)4, list.Nth(6));
		}

		[Fact]
		public void TestUnquoteSplicingVector()
		{
			var res = interpreter.Evaluate(@"`[+ 1 ~@(list 9 8) 2 ~@[] ~@[3 4]]");

			Vector? vector = res as Vector;
			Assert.NotNull(vector);
			Assert.Equal(7, vector!.Count);
			Assert.Equal(env.GetSymbol("+"), vector.Nth(0));
			Assert.Equal((long)1, vector.Nth(1));
			Assert.Equal((long)9, vector.Nth(2));
			Assert.Equal((long)8, vector.Nth(3));
			Assert.Equal((long)2, vector.Nth(4));
			Assert.Equal((long)3, vector.Nth(5));
			Assert.Equal((long)4, vector.Nth(6));
		}

		[Fact]
		public void TestUnquoteSplicingHashSet()
		{
			var res = interpreter.Evaluate(@"`#{+ 1 ~@(list 2 8) 2 ~@[] ~@[3 8]}");

			Set? set = res as Set;
			Assert.NotNull(set);
			Assert.Equal(5, set!.Count);
			Assert.Contains(env.GetSymbol("+"), set);
			Assert.Contains((long)1, set);
			Assert.Contains((long)2, set);
			Assert.Contains((long)3, set);
			Assert.Contains((long)8, set);
		}

		[Fact]
		public void TestMacro()
		{
			var res = interpreter.Evaluate(@"
				(defmacro test [x] `[2 3 ~x])
				(macroexpand (test (+ 1 2)))
			");

			Vector? outer = res as Vector;
			Assert.NotNull(outer);
			Assert.Equal(3, outer!.Count);
			Assert.Equal((long)2, outer.Nth(0));
			Assert.Equal((long)3, outer.Nth(1));

			List? inner = outer.Nth(2) as List;
			Assert.NotNull(inner);
			Assert.Equal(3, inner!.Count);
			Assert.Equal(env.GetSymbol("+"), inner.Nth(0));
			Assert.Equal((long)1, inner.Nth(1));
			Assert.Equal((long)2, inner.Nth(2));

			Assert.Equal((long)8, interpreter.Evaluate(@"(apply + (test (+ 1 2)))"));
		}

		[Fact]
		public void TestMacroUnquoteSplice()
		{
			var res = interpreter.Evaluate(@"
				(defmacro test [& r] `[2 3 ~@r ~@r])
				(macroexpand (test 1 0))
			");

			Vector? vector = res as Vector;
			Assert.NotNull(vector);
			Assert.Equal(6, vector!.Count);
			Assert.Equal((long)2, vector.Nth(0));
			Assert.Equal((long)3, vector.Nth(1));
			Assert.Equal((long)1, vector.Nth(2));
			Assert.Equal((long)0, vector.Nth(3));
			Assert.Equal((long)1, vector.Nth(4));
			Assert.Equal((long)0, vector.Nth(5));

			Assert.Equal((long)15, interpreter.Evaluate(@"(apply + (test (+ 1 2) 2))"));
		}

		[Fact]
		public void TestMultiFormMacro()
		{
			interpreter.Evaluate(@"(defmacro test ([a] a) ([a b] (+ a b)))");

			Assert.Equal((long)1, interpreter.Evaluate(@"(test 1)"));
			Assert.Equal((long)3, interpreter.Evaluate(@"(test 1 2)"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(test 1 2 3)"));
		}

		[Fact]
		public void TestMacroArityError()
		{
			interpreter.Evaluate(@"(defmacro test [a] a)");
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(test 1 2)"));
		}

		[Fact]
		public void TestGenSymbols()
		{
			Vector? vector = interpreter.Evaluate(@"`[test# test#]") as Vector;

			Assert.NotNull(vector);
			Assert.Equal(2, vector!.Count);
			Assert.True(vector[0] is Symbol);
			Assert.True(vector[1] is Symbol);
			Assert.Equal(vector[0], vector[1]);

			vector = interpreter.Evaluate(@"[`test# `test#]") as Vector;

			Assert.NotNull(vector);
			Assert.Equal(2, vector!.Count);
			Assert.True(vector[0] is Symbol);
			Assert.True(vector[1] is Symbol);
			Assert.NotEqual(vector[0], vector[1]);
		}
	}
}
