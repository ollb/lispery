﻿using Lispery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest.Euler
{
	/// <summary>
	/// Test cases from project euler.
	/// </summary>
	public class EulerTest
	{
		[Fact]
		public void TestEuler1()
		{
			string source = @"
				(defn is-multiple [n]
					(or (= (mod n 3) 0)
					    (= (mod n 5) 0)))

				(defn sum-multiples [n]
					(apply + (filter is-multiple (range 1 n))))

				(sum-multiples 1000)
			";

			Interpreter interpreter = new();
			Assert.Equal((long)233168, interpreter.Evaluate(source));
		}

		[Fact]
		public void TestEuler2()
		{
			string source = @"
				(defn fibonacci []
					(let [step (fn [[a b]] [b (+ a b)])]
						(map first (iterate step [0 1]))))

				(defn even? [n]
					(= (mod n 2) 0))

				(apply +
					(filter even?
						(take-while #(<= % 4000000) (fibonacci))))
			";

			Interpreter interpreter = new();
			Assert.Equal((long)4613732, interpreter.Evaluate(source));
		}

		[Fact]
		public void TestEuler3()
		{
			string source = @"
				(defn find-divisor [n]
					(first (filter #(zero? (mod n %)) (range 2 (inc n)))))

				(defn prime-factors [n]
					(if (<= n 1)
						[n]
						(let [d (find-divisor n)]
							(cons d (prime-factors (/ n d))))))

				(apply max (prime-factors 600851475143))
			";

			Interpreter interpreter = new();
			Assert.Equal((long)6857, interpreter.Evaluate(source));
		}

		[Fact]
		public void TestEuler4()
		{
			// The result number has the format "abccba"
			//   res = 100000a + 10000b + 1000c + 100c + 10b + a
			//   res = 100001a + 10010b + 1100c
			//   res = 11*(9091a + 910b + 100c)
			//   num = 9091a + 910b + 100c = res / 11
			//
			// The result is the product of two 3-digit numbers x and y:
			//   res = x*y
			//   num = x*y / 11
			// 
			// Let z = x/11, then:
			//   num = z*y
			//   x >= 100 --> z >= 10
			//   x <= 999 --> z <= 90
			//
			// 1. Iterate all palindromes, by going through a, b, c.
			// 2. Calculate the corresponding num.
			// 3. Find z in [10, 90] that evenly divides num and find the
			//    corresponding y, which has to be < 999.
			// 4. Calculate res = num * 11.
			//
			// To find the largest palindrome, iterate in descending order,
			// with a first, then b, then c. The first result should be the
			// highest number.

			string source = @"
				(first
					(for [a (range 9 0 -1)
						  b (range 9 -1 -1)
						  c (range 9 -1 -1)
						  :let [num (+ (* 9091 a) (* 910 b) (* 100 c))]
						  z (range 10 91)
						  :when (zero? (mod num z))
						  :let [y (/ num z)]
						  :when (< y 999)]
					(* 11 num)))
			";

			Interpreter interpreter = new();
			Assert.Equal((long)906609, interpreter.Evaluate(source));
		}

		[Fact]
		public void TestEuler5()
		{
			// GCD: greatest common divisor
			// LCM: least common multiple

			string source = @"
				(defn gcd [a b]
					(if (= b 0)
						a
						(gcd b (mod a b))))

				(defn lcm [a b]
					(div (abs (* a b)) (gcd a b)))

				(fold lcm 2 (range 3 20))
			";

			Interpreter interpreter = new();
			Assert.Equal((long)232792560, interpreter.Evaluate(source));
		}

		[Fact]
		public void TestEuler6()
		{
			string source = @"
				(defn sum-of-squares [s]
					(apply + (map #(* % %) s)))
	
				(defn square-of-sum [s]
					(#(* % %) (apply + s)))

				(let [s (range 1 101)]
					(- (square-of-sum s) (sum-of-squares s)))
			";

			Interpreter interpreter = new();
			Assert.Equal((long)25164150, interpreter.Evaluate(source));
		}

		[Fact]
		public void TestEuler8()
		{
			string source = @"
				(def number [
					7 3 1 6 7 1 7 6 5 3 1 3 3 0 6 2 4 9 1 9 2 2 5 1 1 9 6 7 4 4 2 6 5 7 4 7 4 2 3 5 5 3 4 9 1 9 4 9 3 4
					9 6 9 8 3 5 2 0 3 1 2 7 7 4 5 0 6 3 2 6 2 3 9 5 7 8 3 1 8 0 1 6 9 8 4 8 0 1 8 6 9 4 7 8 8 5 1 8 4 3
					8 5 8 6 1 5 6 0 7 8 9 1 1 2 9 4 9 4 9 5 4 5 9 5 0 1 7 3 7 9 5 8 3 3 1 9 5 2 8 5 3 2 0 8 8 0 5 5 1 1
					1 2 5 4 0 6 9 8 7 4 7 1 5 8 5 2 3 8 6 3 0 5 0 7 1 5 6 9 3 2 9 0 9 6 3 2 9 5 2 2 7 4 4 3 0 4 3 5 5 7
					6 6 8 9 6 6 4 8 9 5 0 4 4 5 2 4 4 5 2 3 1 6 1 7 3 1 8 5 6 4 0 3 0 9 8 7 1 1 1 2 1 7 2 2 3 8 3 1 1 3
					6 2 2 2 9 8 9 3 4 2 3 3 8 0 3 0 8 1 3 5 3 3 6 2 7 6 6 1 4 2 8 2 8 0 6 4 4 4 4 8 6 6 4 5 2 3 8 7 4 9
					3 0 3 5 8 9 0 7 2 9 6 2 9 0 4 9 1 5 6 0 4 4 0 7 7 2 3 9 0 7 1 3 8 1 0 5 1 5 8 5 9 3 0 7 9 6 0 8 6 6
					7 0 1 7 2 4 2 7 1 2 1 8 8 3 9 9 8 7 9 7 9 0 8 7 9 2 2 7 4 9 2 1 9 0 1 6 9 9 7 2 0 8 8 8 0 9 3 7 7 6
					6 5 7 2 7 3 3 3 0 0 1 0 5 3 3 6 7 8 8 1 2 2 0 2 3 5 4 2 1 8 0 9 7 5 1 2 5 4 5 4 0 5 9 4 7 5 2 2 4 3
					5 2 5 8 4 9 0 7 7 1 1 6 7 0 5 5 6 0 1 3 6 0 4 8 3 9 5 8 6 4 4 6 7 0 6 3 2 4 4 1 5 7 2 2 1 5 5 3 9 7
					5 3 6 9 7 8 1 7 9 7 7 8 4 6 1 7 4 0 6 4 9 5 5 1 4 9 2 9 0 8 6 2 5 6 9 3 2 1 9 7 8 4 6 8 6 2 2 4 8 2
					8 3 9 7 2 2 4 1 3 7 5 6 5 7 0 5 6 0 5 7 4 9 0 2 6 1 4 0 7 9 7 2 9 6 8 6 5 2 4 1 4 5 3 5 1 0 0 4 7 4
					8 2 1 6 6 3 7 0 4 8 4 4 0 3 1 9 9 8 9 0 0 0 8 8 9 5 2 4 3 4 5 0 6 5 8 5 4 1 2 2 7 5 8 8 6 6 6 8 8 1
					1 6 4 2 7 1 7 1 4 7 9 9 2 4 4 4 2 9 2 8 2 3 0 8 6 3 4 6 5 6 7 4 8 1 3 9 1 9 1 2 3 1 6 2 8 2 4 5 8 6
					1 7 8 6 6 4 5 8 3 5 9 1 2 4 5 6 6 5 2 9 4 7 6 5 4 5 6 8 2 8 4 8 9 1 2 8 8 3 1 4 2 6 0 7 6 9 0 0 4 2
					2 4 2 1 9 0 2 2 6 7 1 0 5 5 6 2 6 3 2 1 1 1 1 1 0 9 3 7 0 5 4 4 2 1 7 5 0 6 9 4 1 6 5 8 9 6 0 4 0 8
					0 7 1 9 8 4 0 3 8 5 0 9 6 2 4 5 5 4 4 4 3 6 2 9 8 1 2 3 0 9 8 7 8 7 9 9 2 7 2 4 4 2 8 4 9 0 9 1 8 8
					8 4 5 8 0 1 5 6 1 6 6 0 9 7 9 1 9 1 3 3 8 7 5 4 9 9 2 0 0 5 2 4 0 6 3 6 8 9 9 1 2 5 6 0 7 1 7 6 0 6
					0 5 8 8 6 1 1 6 4 6 7 1 0 9 4 0 5 0 7 7 5 4 1 0 0 2 2 5 6 9 8 3 1 5 5 2 0 0 0 5 5 9 3 5 7 2 9 7 2 5
					7 1 6 3 6 2 6 9 5 6 1 8 8 2 6 7 0 4 2 8 2 5 2 4 8 3 6 0 0 8 2 3 2 5 7 5 3 0 4 2 0 7 5 2 9 6 3 4 5 0])

				(defn scan [list]
					(let [(seq a b c d e f g h i j k l m & _) list]
						(cons (* a b c d e f g h i j k l m)
							(if (> (count list) 13)
								(scan (rest list))
								[]))))

				(apply max (scan number))
			";

			Interpreter interpreter = new();
			Assert.Equal((long)23514624000, interpreter.Evaluate(source));
		}
	}
}
