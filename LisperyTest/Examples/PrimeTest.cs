﻿using Lispery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest.Examples
{
	public class PrimeTest
	{
		private const string commonSource = @"
			(def numbers-from-2 (iterate inc 2))

			(defn filter-multiples [number seq]
				(filter #(not= (mod % number) 0) seq))
		";

		[Fact]
		public void TestPrime1()
		{
			string source = @"
				(defn prime-step [[_ (seq prime & rest)]]
					[prime (filter-multiples prime rest)])

				(def primes
					(map first (rest (iterate prime-step [0 numbers-from-2]))))

				(apply + (take 100 primes))
			";

			Interpreter interpreter = new();
			interpreter.Evaluate(commonSource);
			Assert.Equal((long)24133, interpreter.Evaluate(source));
		}

		[Fact]
		public void TestPrime2()
		{
			string source = @"
				(defn primes-loop [(seq prime & rest)]
					(lazy-seq (cons prime (primes-loop (filter-multiples prime rest)))))

				(def primes (primes-loop numbers-from-2))

				(apply + (take 100 primes))
			";

			Interpreter interpreter = new();
			interpreter.Evaluate(commonSource);
			Assert.Equal((long)24133, interpreter.Evaluate(source));
		}
	}
}
