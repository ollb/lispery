﻿using Lispery.Adapters;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Sequences;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest.Adapters
{
	public class LazySequenceTest
	{
		[Fact]
		public void TestCaching()
		{
			int counter = 0;

			IEnumerable<object> Generator()
			{
				int index = 0;

				while (true)
				{
					counter++;
					yield return index++;
				}
			}

			var seq = new LazySequence(Generator());
			int index = 0;

			foreach (var item in seq.GetEntries().Take(50))
			{
				Assert.Equal(index++, item);
			}

			Assert.Equal(50, counter);
			index = 0;
			counter = 0;

			foreach (var item in seq.GetEntries().Take(75))
			{
				Assert.Equal(index++, item);
			}

			Assert.Equal(25, counter);
		}

		[Fact]
		public void TestToString()
		{
			var seq = new LazySequence(Enumerable.Range(0, 5).Cast<object>());
			Assert.Equal("(#LazySequence 0 1 2 3 4)", seq.ToString());

			seq = new LazySequence(Enumerable.Range(0, 20).Cast<object>());
			Assert.Equal("(#LazySequence 0 1 2 3 4 5 6 7 8 9...)", seq.ToString());
		}
	}
}
