﻿using Lispery;
using Lispery.Sequences;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest
{
	public class FormatterTest
	{
		[Fact]
		public void TestPrimitives()
		{
			string source = @"(nil true false 1 1.5 ""test"" test #""test"")";
			Assert.Equal(source, ParseFormat(source));
		}

		[Fact]
		public void TestContainers()
		{ 
			string source = @"([1 2] {:a 1} #{1} ![1 2] !{:a 1} !#{1})";
			Assert.Equal(source, ParseFormat(source));
		}

		[Fact]
		public void TestSequence()
		{
			string str = Formatter.Format(new LazySequence(
				Enumerable.Range(1, 3)
				.Select(v => (long)v)
				.Cast<object?>()));

			Assert.Equal(@"(1 2 3)", str);

			str = Formatter.Format(new LazySequence(
				Enumerable.Range(1, 10000)
				.Select(v => (long)v)
				.Cast<object?>()));

			Assert.StartsWith("(1 2 3", str);
			Assert.Contains("...", str);
		}

		[Fact]
		public void TestBigInt()
		{
			Assert.Equal("12345", Formatter.Format(new System.Numerics.BigInteger(12345)));
		}

		[Fact]
		public void TestKeyValue()
		{
			Assert.Equal("(kv-pair 1 2)", Formatter.Format(
				KeyValuePair.Create(
					(object)(long)1,
					(object)(long)2)));
		}

		[Fact]
		public void TestToString()
		{
			Assert.Equal("<(1, 2)>", Formatter.Format((1, 2)));
		}

		[Fact]
		public void TestEscape()
		{
			Assert.Equal(@"""\\te\""st""", Formatter.Format(@"\te""st"));
		}

		private static string ParseFormat(string str)
		{
			SymbolTable symbolTable = new();
			KeywordTable keywordTable = new();
			Parser parser = new(symbolTable, keywordTable);
			object? value = parser.Parse(new StringReader(str));
			return Formatter.Format(value).Trim();
		}
	}
}
