﻿using Lispery;
using Lispery.Containers;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest
{
	public class InterpreterTest
	{
		readonly Interpreter interpreter = new();
		readonly Lispery.Environment env;

		public InterpreterTest()
		{
			env = interpreter.Environment;
		}

		[Fact]
		public void TestFac()
		{
			string source = @"
				(defn fac [n]
					(if (<= n 1)
						1
						(* n (fac (- n 1)))))
			";

			interpreter.Evaluate(source);

			Assert.Equal((long)1, interpreter.Evaluate("(fac 0)"));
			Assert.Equal((long)1, interpreter.Evaluate("(fac 1)"));
			Assert.Equal((long)2, interpreter.Evaluate("(fac 2)"));
			Assert.Equal((long)6, interpreter.Evaluate("(fac 3)"));
			Assert.Equal((long)24, interpreter.Evaluate("(fac 4)"));
			Assert.Equal((long)120, interpreter.Evaluate("(fac 5)"));
		}

		[Fact]
		public void TestFib()
		{
			string source = @"
				(defn fib [n]
					(if (<= n 2)
						1
						(+ (fib (- n 1)) (fib (- n 2)))))
			";

			interpreter.Evaluate(source);

			Assert.Equal((long)1, interpreter.Evaluate("(fib 1)"));
			Assert.Equal((long)8, interpreter.Evaluate("(fib 6)"));
			Assert.Equal((long)34, interpreter.Evaluate("(fib 9)"));
			Assert.Equal((long)55, interpreter.Evaluate("(fib 10)"));
		}

		[Fact]
		public void TestDef()
		{
			Assert.Equal((long)5, interpreter.Evaluate(@"(def a 5) a"));
			Assert.Equal((long)7, interpreter.Evaluate(@"(def a 5) (def a 7) a"));
			Assert.Equal((long)9, interpreter.Evaluate(@"(def b 9) (def a b) a"));
		}

		[Fact]
		public void TestEvaluateVector()
		{
			var vector = interpreter.Evaluate(@"[1 (+ 1 1) (+ 1 1 1)]") as Vector;

			Assert.NotNull(vector);
			Assert.Equal(new object[] { (long)1, (long)2, (long)3 }, vector);
		}

		[Fact]
		public void TestEvaluateHashSet()
		{
			var hashSet = interpreter.Evaluate(@"#{1 (+ 1 1) (+ 1 1 1)}") as Set;

			Assert.NotNull(hashSet);
			Assert.Contains((long)1, hashSet);
			Assert.Contains((long)2, hashSet);
			Assert.Contains((long)3, hashSet);
			Assert.Equal(3, hashSet!.Count);
		}

		[Fact]
		public void TestEvaluateTreeMap()
		{
			var treeMap = interpreter.Evaluate(@"{1 (+ 1 1) (+ 1 1) (+ 1 1 1)}") as Dictionary;

			Assert.NotNull(treeMap);
			Assert.Equal((long)2, treeMap!.Get((long)1));
			Assert.Equal((long)3, treeMap!.Get((long)2));
			Assert.Equal(2, treeMap!.Count);
		}

		[Fact]
		public void TestBooleans()
		{
			Assert.Equal(false, interpreter.Evaluate("(not true)"));
			Assert.Equal(false, interpreter.Evaluate("(not 5)"));
			Assert.Equal(true, interpreter.Evaluate("(not false)"));
			Assert.Equal(true, interpreter.Evaluate("(not nil)"));

			Assert.Equal(true, interpreter.Evaluate("(and)"));
			Assert.Equal(true, interpreter.Evaluate("(and true)"));
			Assert.Equal(false, interpreter.Evaluate("(and false)"));
			Assert.Null(interpreter.Evaluate("(and nil)"));
			Assert.Equal((long)5, interpreter.Evaluate("(and 5)"));

			Assert.Null(interpreter.Evaluate("(and true nil 5)"));
			Assert.Equal(false, interpreter.Evaluate("(and true false 5)"));
			Assert.Equal("test", interpreter.Evaluate("(and true 5 7 \"test\")"));
			Assert.Equal(true, interpreter.Evaluate("(and true 5 7 \"test\" true)"));

			Assert.Null(interpreter.Evaluate("(or)"));
			Assert.Equal(true, interpreter.Evaluate("(or true)"));
			Assert.Equal(false, interpreter.Evaluate("(or false)"));
			Assert.Null(interpreter.Evaluate("(or nil)"));
			Assert.Equal((long)5, interpreter.Evaluate("(or 5)"));

			Assert.Equal((long)5, interpreter.Evaluate("(or false 5 nil 5)"));
			Assert.Null(interpreter.Evaluate("(or false nil)"));
			Assert.Equal(false, interpreter.Evaluate("(or nil false)"));
			Assert.Equal("test", interpreter.Evaluate("(or nil false \"test\" false)"));
			Assert.Equal("test", interpreter.Evaluate("(or nil false \"test\" false 5)"));
		}

		[Fact]
		public void TestArithmetics()
		{
			Assert.Equal((long)0, interpreter.Evaluate("(+)"));
			Assert.Equal((long)1, interpreter.Evaluate("(+ 1)"));
			Assert.Equal((long)6, interpreter.Evaluate("(+ 1 2 3)"));
			Assert.Equal((double)6, interpreter.Evaluate("(+ 1 2 3.0)"));

			Assert.Throws<ArgumentException>(() => interpreter.Evaluate("(-)"));
			Assert.Equal((long)-1, interpreter.Evaluate("(- 1)"));
			Assert.Equal((long)4, interpreter.Evaluate("(- 5 1)"));
			Assert.Equal((long)2, interpreter.Evaluate("(- 5 1 2)"));
			Assert.Equal((double)2, interpreter.Evaluate("(- 5 1 2.0)"));

			Assert.Equal((long)1, interpreter.Evaluate("(*)"));
			Assert.Equal((long)2, interpreter.Evaluate("(* 2)"));
			Assert.Equal((long)30, interpreter.Evaluate("(* 2 3 5)"));
			Assert.Equal((double)30, interpreter.Evaluate("(* 2 3 5.0)"));

			// Note:
			// 0.5    = 2^-1
			// 0.0625 = 2^-4
			// Both can be represented exactly as double.

			Assert.Throws<ArgumentException>(() => interpreter.Evaluate("(/)"));
			Assert.Equal((double)0.5, interpreter.Evaluate("(/ 2)"));
			Assert.Equal((double)0.0625, interpreter.Evaluate("(/ 2.0 4 8)"));
			Assert.Equal((long)25, interpreter.Evaluate("(/ 200 2 4)"));
			Assert.Equal((long)2, interpreter.Evaluate("(/ 5 2)"));
		}

		[Fact]
		public void TestDefMatch()
		{
			Assert.Equal((long)5, interpreter.Evaluate(@"(def (when int? a) 5)"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(def (when (int? a) a) 5)"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(def (when int? a) :a)"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(def (when (int? a) a) :a)"));
		}

		[Fact]
		public void TestLetMatch()
		{
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(let [[a b] [1]] nil)"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(let [[a b] [1 2 3]] nil)"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(let [(list a b) (list 1)] nil)"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(let [(list a b) (list 1 2 3)] nil)"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(let [(seq a b) (range 1 2)] nil)"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(let [(seq a b) (range 1 4)] nil)"));
			Assert.Equal((long)1, interpreter.Evaluate(@"(let [[a b] [1 2]] (- b a))"));
			Assert.Equal((long)1, interpreter.Evaluate(@"(let [(list a b) (list 1 2)] (- b a))"));
			Assert.Equal((long)1, interpreter.Evaluate(@"(let [(seq a b) (range 1 3)] (- b a))"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(let [[a & r] [1 2 3]] (apply + r))"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(let [(list a & r) (list 1 2 3)] (apply + r))"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(let [(seq a & r) (range 1 4)] (apply + r))"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(let [[[a b] & r] [[1 2] 3]] (+ b (apply + r)))"));
		}

		[Fact]
		public void TestFnMatch()
		{
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"((fn [[a b]] nil) [1])"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"((fn [[a b]] nil) [1 2 3])"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"((fn [(list a b)] nil) (list 1))"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"((fn [(list a b)] nil) (list 1 2 3))"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"((fn [(seq a b)] nil) (range 1 2))"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"((fn [(seq a b)] nil) (range 1 4))"));
			Assert.Equal((long)1, interpreter.Evaluate(@"((fn [[a b]] (- b a)) [1 2])"));
			Assert.Equal((long)1, interpreter.Evaluate(@"((fn [(list a b)] (- b a)) (list 1 2))"));
			Assert.Equal((long)1, interpreter.Evaluate(@"((fn [(seq a b)] (- b a)) (range 1 3))"));
			Assert.Equal((long)5, interpreter.Evaluate(@"((fn [[a & r]] (apply + r)) [1 2 3])"));
			Assert.Equal((long)5, interpreter.Evaluate(@"((fn [(list a & r)] (apply + r)) (list 1 2 3))"));
			Assert.Equal((long)5, interpreter.Evaluate(@"((fn [(seq a & r)] (apply + r)) (range 1 4))"));
			Assert.Equal((long)5, interpreter.Evaluate(@"((fn [[[a b] & r]] (+ b (apply + r))) [[1 2] 3])"));
		}

		[Fact]
		public void TestDefnMatch()
		{
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(defn f [[a b]] nil) (f [1])"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(defn f [[a b]] nil) (f [1 2 3])"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(defn f [(list a b)] nil) (f (list 1))"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(defn f [(list a b)] nil) (f (list 1 2 3))"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(defn f [(seq a b)] nil) (f (range 1 2))"));
			Assert.Throws<ArgumentException>(() => interpreter.Evaluate(@"(defn f [(seq a b)] nil) (f (range 1 4))"));
			Assert.Equal((long)1, interpreter.Evaluate(@"(defn f [[a b]] (- b a)) (f [1 2])"));
			Assert.Equal((long)1, interpreter.Evaluate(@"(defn f [(list a b)] (- b a)) (f (list 1 2))"));
			Assert.Equal((long)1, interpreter.Evaluate(@"(defn f [(seq a b)] (- b a)) (f (range 1 3))"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(defn f [[a & r]] (apply + r)) (f [1 2 3])"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(defn f [(list a & r)] (apply + r)) (f (list 1 2 3))"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(defn f [(seq a & r)] (apply + r)) (f (range 1 4))"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(defn f [[[a b] & r]] (+ b (apply + r))) (f [[1 2] 3])"));
		}

		[Fact]
		public void TestRest()
		{
			object? result;

			result = interpreter.Evaluate(@"(rest [])");
			Assert.True(result is Vector vector && vector.Count == 0);

			result = interpreter.Evaluate(@"(rest (list))");
			Assert.True(result is List list && list.Count == 0);
		}

		[Fact]
		public void TestSkip()
		{
			object? result;

			result = interpreter.Evaluate(@"(skip 0 [])");
			Assert.True(result is Vector vector1 && vector1.Count == 0);
			result = interpreter.Evaluate(@"(skip 1 [])");
			Assert.True(result is Vector vector2 && vector2.Count == 0);
			result = interpreter.Evaluate(@"(skip 10 [])");
			Assert.True(result is Vector vector3 && vector3.Count == 0);

			result = interpreter.Evaluate(@"(skip 0 (list))");
			Assert.True(result is List list1 && list1.Count == 0);
			result = interpreter.Evaluate(@"(skip 1 (list))");
			Assert.True(result is List list2 && list2.Count == 0);
			result = interpreter.Evaluate(@"(skip 10 (list))");
			Assert.True(result is List list3 && list3.Count == 0);
		}

		[Fact]
		public void TestLazyValues()
		{
			interpreter.Evaluate(@"(def a 10)");
			interpreter.Evaluate(@"(def b {:a 5})");

			var code = new ExpressionList(
				new LazySymbol("a")
			);

			Assert.Equal((long)10, interpreter.Evaluate(code));

			code = new ExpressionList(
				new object?[]
				{
					new List(new object?[]
					{
						env.GetSymbol("get"),
						new LazyKeyword("a"),
						new LazySymbol("b")
					})
				}
			);

			Assert.Equal((long)5, interpreter.Evaluate(code));
		}

		[Fact]
		public void TestSpecialFunctions()
		{
			interpreter.Evaluate(@"(def a #""x\d+"")");
			interpreter.Evaluate(@"(def b {:a 10 :b 5})");
			interpreter.Evaluate(@"(def c #{1 2 3})");

			Assert.Equal(true, interpreter.Evaluate(@"(a ""x123"")"));
			Assert.Equal(false, interpreter.Evaluate(@"(a ""xy123"")"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(b :b)"));
			Assert.Null(interpreter.Evaluate(@"(b :x)"));
			Assert.Equal(true, interpreter.Evaluate(@"(c 2)"));
			Assert.Equal(false, interpreter.Evaluate(@"(c 5)"));
		}

		[Fact]
		public void TestExpressionList()
		{
			ExpressionList list = interpreter.Parse(@"
				(defn test [] (assign! a (inc a)))
				(def a 1)
				(test)
				(test)
			");

			Assert.Equal((long)3, interpreter.Evaluate(list));
		}
	}
}
