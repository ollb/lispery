using Lispery;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace LisperyTest.Containers
{
	public class MutableSetTest
	{
		private readonly Interpreter interpreter = new();

		private static IEnumerable<object?> GetEntries(object? obj)
		{
			Assert.True(obj is ISequence);
			return ((ISequence)obj!).GetEntries().OrderBy(v => v);
		}

		private static IEnumerable<object?> GetEnumerable(params int[] values)
		{
			return values.Select(v => (object)(long)v);
		}

		[Fact]
		public void TestCreation()
		{
			Assert.Equal(GetEnumerable(1, 2, 3), GetEntries(interpreter.Evaluate(@"!#{1 2 3}")));
			Assert.Equal(GetEnumerable(1, 2, 3), GetEntries(interpreter.Evaluate(@"(make-set! 1 2 3)")));

			Assert.Equal((long)3, interpreter.Evaluate(@"(count !#{1 2 3})"));
		}

		[Fact]
		public void TestAddRemove()
		{
			interpreter.Evaluate(@"(def a !#{1 2 3})");
			Assert.Null(interpreter.Evaluate(@"(add! 2 a)"));
			Assert.Null(interpreter.Evaluate(@"(add! 5 a)"));
			Assert.Equal((long)4, interpreter.Evaluate(@"(count a)"));

			Assert.Equal(GetEnumerable(1, 2, 3, 5), GetEntries(interpreter.Evaluate(@"a")));

			Assert.Null(interpreter.Evaluate(@"(remove! 2 a)"));

			Assert.Equal(GetEnumerable(1, 3, 5), GetEntries(interpreter.Evaluate(@"a")));
		}

		[Fact]
		public void TestContains()
		{
			interpreter.Evaluate(@"(def a !#{1 2 3})");

			Assert.Equal(true, interpreter.Evaluate(@"(contains 2 a)"));
			Assert.Equal(false, interpreter.Evaluate(@"(contains 5 a)"));
		}

		[Fact]
		public void TestIteration()
		{
			interpreter.Evaluate(@"(def a !#{1 2 3})");

			Assert.Equal(GetEnumerable(2, 4, 6), GetEntries(interpreter.Evaluate(@"(map #(* 2 %) a)")));
		}

		[Fact]
		public void TestToString()
		{
			Assert.Equal(@"!#{1}", interpreter.Evaluate(@"!#{1}")!.ToString());
			Assert.Contains("...", interpreter.Evaluate(@"!#{1 2 3 4 5 6 7 8 9 10 11 12}")!.ToString());
		}
	}
}