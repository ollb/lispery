﻿using Lispery;
using Lispery.Containers;
using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest.Containers
{
	public class ListTest
	{
		private readonly Interpreter interpreter = new();

		private static IEnumerable<object?> GetEntries(object? obj)
		{
			Assert.True(obj is ISequence);
			return ((ISequence)obj!).GetEntries();
		}

		private static IEnumerable<object?> GetEnumerable(params int[] values)
		{
			return values.Select(v => (object)(long)v);
		}

		[Fact]
		public void TestCreation()
		{
			Assert.Equal(GetEnumerable(1, 2, 3), GetEntries(interpreter.Evaluate(@"'(1 2 3)")));
			Assert.Equal(GetEnumerable(1, 2, 3), GetEntries(interpreter.Evaluate(@"(list 1 2 3)")));
			Assert.Equal(GetEnumerable(1, 2, 3), GetEntries(interpreter.Evaluate(@"(make-list 1 2 3)")));

			Assert.Equal((long)3, interpreter.Evaluate(@"(count (list 1 2 3))"));
		}

		[Fact]
		public void TestBuilder()
		{
			var list = new List(new object?[] { (long)1, (long)2 });

			var builder = list.ToBuilder();
			builder.Add((long)3);
			builder.Add((long)4);
			list = builder.ToImmutable();

			Assert.Equal(GetEnumerable(1, 2, 3, 4), list);

			builder = list.ToBuilder();
			builder.Clear();
			builder.AddRange(Enumerable.Range(3, 2).Select(v => (object)(long)v));
			list = builder.ToImmutable();

			Assert.Equal(GetEnumerable(3, 4), list);

			builder = new List.Builder();
			builder.AddRange(Enumerable.Range(3, 2).Select(v => (object)(long)v));
			list = builder.ToImmutable();

			Assert.Equal(GetEnumerable(3, 4), list);
		}

		[Fact]
		public void TestEmpty()
		{
			Assert.Equal(0, List.Empty.Count);
			Assert.Equal(Array.Empty<object?>(), List.Empty);
		}

		[Fact]
		public void TestConsRest()
		{
			interpreter.Evaluate(@"(def a (list 1 2 3))");
			interpreter.Evaluate(@"(def a (cons 4 a))");
			interpreter.Evaluate(@"(def a (cons 5 a))");
			Assert.Equal((long)5, interpreter.Evaluate(@"(count a)"));

			Assert.Equal(GetEnumerable(5, 4, 1, 2, 3), GetEntries(interpreter.Evaluate(@"a")));

			interpreter.Evaluate(@"(def a (rest a))");
			Assert.Equal(GetEnumerable(4, 1, 2, 3), GetEntries(interpreter.Evaluate(@"a")));

			interpreter.Evaluate(@"(def a (rest a))");
			Assert.Equal(GetEnumerable(1, 2, 3), GetEntries(interpreter.Evaluate(@"a")));
		}

		[Fact]
		public void TestFirst()
		{
			Assert.Equal((long)1, interpreter.Evaluate(@"(first (list 1 2 3 4 5))"));
			Assert.Null(interpreter.Evaluate(@"(first (list))"));
		}

		[Fact]
		public void TestRestSkip()
		{
			interpreter.Evaluate(@"(def a (list 1 2))");

			Assert.Equal(GetEnumerable(1, 2), GetEntries(interpreter.Evaluate(@"(skip 0 a)")));

			Assert.Equal(GetEnumerable(2), GetEntries(interpreter.Evaluate(@"(rest a)")));
			Assert.Equal(GetEnumerable(2), GetEntries(interpreter.Evaluate(@"(skip 1 a)")));

			Assert.Equal(GetEnumerable(), GetEntries(interpreter.Evaluate(@"(rest (rest a))")));
			Assert.Equal(GetEnumerable(), GetEntries(interpreter.Evaluate(@"(skip 2 a)")));
			Assert.Equal(GetEnumerable(), GetEntries(interpreter.Evaluate(@"(skip 5 a)")));
			Assert.Equal(GetEnumerable(), GetEntries(interpreter.Evaluate(@"(rest (list))")));

			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(skip -1 a)"));
		}

		[Fact]
		public void TestReverse()
		{
			interpreter.Evaluate(@"(def a (list 1 2 3))");

			Assert.Equal(GetEnumerable(3, 2, 1), GetEntries(interpreter.Evaluate(@"(reverse a)")));
		}

		[Fact]
		public void TestNth()
		{
			interpreter.Evaluate(@"(def a (list 1 2 3))");

			Assert.Equal((long)1, interpreter.Evaluate(@"(nth 0 a)"));
			Assert.Equal((long)2, interpreter.Evaluate(@"(nth 1 a)"));
			Assert.Equal((long)3, interpreter.Evaluate(@"(nth 2 a)"));

			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(nth -1 a)"));
			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(nth 3 a)"));
		}

		[Fact]
		public void TestIteration()
		{
			interpreter.Evaluate(@"(def a (list 1 2 3))");

			Assert.Equal(GetEnumerable(2, 4, 6), GetEntries(interpreter.Evaluate(@"(map #(* 2 %) a)")));
		}

		[Fact]
		public void TestMatch()
		{
			var list = new List(new object?[] { (long)1, (long)2, (long)3, (long)4 });

			var match1 = list.Match1();
			Assert.Equal((long)1, match1);

			var match1R = list.Match1R();
			Assert.Equal((long)1, match1R.Item1);
			Assert.Equal(GetEnumerable(2, 3, 4), match1R.Item2);
			Assert.Equal(3, match1R.Item2.Count);

			var match2 = list.Match2();
			Assert.Equal((long)1, match2.Item1);
			Assert.Equal((long)2, match2.Item2);

			var match2R = list.Match2R();
			Assert.Equal((long)1, match2R.Item1);
			Assert.Equal((long)2, match2R.Item2);
			Assert.Equal(GetEnumerable(3, 4), match2R.Item3);
			Assert.Equal(2, match2R.Item3.Count);

			var match3 = list.Match3();
			Assert.Equal((long)1, match3.Item1);
			Assert.Equal((long)2, match3.Item2);
			Assert.Equal((long)3, match3.Item3);

			var match3R = list.Match3R();
			Assert.Equal((long)1, match3R.Item1);
			Assert.Equal((long)2, match3R.Item2);
			Assert.Equal((long)3, match3R.Item3);
			Assert.Equal(GetEnumerable(4), match3R.Item4);
			Assert.Equal(1, match3R.Item4.Count);

			var match4 = list.Match4();
			Assert.Equal((long)1, match4.Item1);
			Assert.Equal((long)2, match4.Item2);
			Assert.Equal((long)3, match4.Item3);
			Assert.Equal((long)4, match4.Item4);

			var match4R = list.Match4R();
			Assert.Equal((long)1, match4R.Item1);
			Assert.Equal((long)2, match4R.Item2);
			Assert.Equal((long)3, match4R.Item3);
			Assert.Equal((long)4, match4R.Item4);
			Assert.Equal(GetEnumerable(), match4R.Item5);
			Assert.Equal(0, match4R.Item5.Count);

			Assert.Throws<InvalidOperationException>(() => new List(list.Take(0)).Match1());
			Assert.Throws<InvalidOperationException>(() => new List(list.Take(0)).Match1R());
			Assert.Throws<InvalidOperationException>(() => new List(list.Take(1)).Match2());
			Assert.Throws<InvalidOperationException>(() => new List(list.Take(1)).Match2R());
			Assert.Throws<InvalidOperationException>(() => new List(list.Take(2)).Match3());
			Assert.Throws<InvalidOperationException>(() => new List(list.Take(2)).Match3R());
			Assert.Throws<InvalidOperationException>(() => new List(list.Take(3)).Match4());
			Assert.Throws<InvalidOperationException>(() => new List(list.Take(3)).Match4R());
		}

		[Fact]
		public void TestToString()
		{
			Assert.Equal(@"(1 2 3)", interpreter.Evaluate(@"(list 1 2 3)")!.ToString());
			Assert.Equal(@"(1 2 3 4 5 6 7 8 9 10...)", interpreter.Evaluate(@"(list 1 2 3 4 5 6 7 8 9 10 11 12)")!.ToString());
		}
	}
}
