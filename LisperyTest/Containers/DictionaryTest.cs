using Lispery;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Sequences;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace LisperyTest.Containers
{
	public class DictionaryTest
	{
		private readonly Interpreter interpreter = new();
		private readonly Lispery.Environment env;

		public DictionaryTest()
		{
			env = interpreter.Environment;
		}

		private static IEnumerable<(string, long)> GetEntries(object? obj)
		{
			return ((ISequence)obj!)
				.GetEntries()
				.Cast<KeyValuePair<object, object?>>()
				.Select(e => (((Keyword)e.Key).Name, (long)e.Value!))
				.OrderBy(e => e);
		}

		private static IEnumerable<(string, long)> GetEnumerable(params object[] values)
		{
			return values.GroupKeyValue()
				.Select(e => ((string)e.Key, Convert.ToInt64(e.Value)))
				.OrderBy(e => e);
		}

		[Fact]
		public void TestCreation()
		{
			Assert.Equal(
				GetEnumerable("a", 1, "b", 2, "c", 3),
				GetEntries(interpreter.Evaluate(@"{:a 1 :b 2 :c 3}")));

			Assert.Equal(
				GetEnumerable("a", 1, "b", 2, "c", 3),
				GetEntries(interpreter.Evaluate(@"(make-dict :a 1 :b 2 :c 3)")));

			Assert.Equal((long)3, interpreter.Evaluate(@"(count {:a 1 :b 2 :c 3})"));

			Assert.Throws<ArgumentNullException>(() => interpreter.Evaluate(@"{:a 1 nil 2}"));
		}

		[Fact]
		public void TestBuilder()
		{
			var dict = new Dictionary(new KeyValuePair<object, object?>[] {
				KeyValuePair.Create<object, object?>(env.GetKeyword("a"), (long)5),
				KeyValuePair.Create<object, object?>(env.GetKeyword("b"), (long)2),
			});

			var builder = dict.ToBuilder();
			builder.Set(env.GetKeyword("b"), (long)9);
			builder.Set(env.GetKeyword("c"), (long)7);
			dict = builder.ToImmutable();

			Assert.Equal(GetEnumerable("a", 5, "b", 9, "c", 7), GetEntries(dict));

			builder = dict.ToBuilder();
			builder.Clear();
			builder.Set(env.GetKeyword("a"), (long)12);
			dict = builder.ToImmutable();

			Assert.Equal(GetEnumerable("a", 12), GetEntries(dict));

			builder = new Dictionary.Builder();
			builder.Set(env.GetKeyword("a"), (long)12);
			dict = builder.ToImmutable();

			Assert.Equal(GetEnumerable("a", 12), GetEntries(dict));
		}

		[Fact]
		public void TestEmpty()
		{
			Assert.Equal(0, Dictionary.Empty.Count);
			Assert.Equal(Array.Empty<KeyValuePair<object, object?>>(), Dictionary.Empty);
		}

		[Fact]
		public void TestGetSet()
		{
			interpreter.Evaluate(@"(def a {:a 1 :b 2})");

			interpreter.Evaluate(@"(def a (set :b 3 a))");
			Assert.Equal(GetEnumerable("a", 1, "b", 3), GetEntries(interpreter.Evaluate(@"a")));

			interpreter.Evaluate(@"(def a (set :c 5 a))");
			Assert.Equal(GetEnumerable("a", 1, "b", 3, "c", 5), GetEntries(interpreter.Evaluate(@"a")));

			Assert.Equal((long)1, interpreter.Evaluate(@"(get :a a)"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(get :c a)"));

			Assert.Throws<KeyNotFoundException>(() => interpreter.Evaluate(@"(get :x a)"));

			Assert.Equal((long)1, interpreter.Evaluate(@"(get :a 9 a)"));
			Assert.Equal((long)9, interpreter.Evaluate(@"(get :d 9 a)"));
		}

		[Fact]
		public void TestRemove()
		{
			interpreter.Evaluate(@"(def a {:a 1 :b 2})");

			interpreter.Evaluate(@"(def a (remove :b a))");
			Assert.Equal(GetEnumerable("a", 1), GetEntries(interpreter.Evaluate(@"a")));

			interpreter.Evaluate(@"(def a (remove :x a))");
			Assert.Equal(GetEnumerable("a", 1), GetEntries(interpreter.Evaluate(@"a")));
		}

		[Fact]
		public void TestContains()
		{
			interpreter.Evaluate(@"(def a {:a 1 :b 2})");

			Assert.Equal(true, interpreter.Evaluate(@"(contains :a a)"));
			Assert.Equal(false, interpreter.Evaluate(@"(contains :c a)"));
			Assert.Equal(false, interpreter.Evaluate(@"(contains nil a)"));
		}

		[Fact]
		public void TestToString()
		{
			Assert.Equal(@"{:a 1}", interpreter.Evaluate(@"{:a 1}")!.ToString());
			Assert.Contains("...", interpreter.Evaluate(@"{:a 1 :b 2 :c 3 :d 4 :e 5 :f 6}")!.ToString());
		}
	}
}