﻿using Lispery;
using Lispery.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest.Containers
{
	public class MutableVectorTest
	{
		private readonly Interpreter interpreter = new();

		private static IEnumerable<object?> GetEntries(object? obj)
		{
			Assert.True(obj is ISequence);
			return ((ISequence)obj!).GetEntries();
		}

		private static IEnumerable<object?> GetEnumerable(params int[] values)
		{
			return values.Select(v => (object)(long)v);
		}

		[Fact]
		public void TestCreation()
		{
			Assert.Equal(GetEnumerable(1, 2, 3), GetEntries(interpreter.Evaluate(@"![1 2 3]")));
			Assert.Equal(GetEnumerable(1, 2, 3), GetEntries(interpreter.Evaluate(@"(make-vec! 1 2 3)")));

			Assert.Equal((long)3, interpreter.Evaluate(@"(count ![1 2 3])"));
		}

		[Fact]
		public void TestAddRemove()
		{
			interpreter.Evaluate(@"(def a ![1 2 3])");
			Assert.Null(interpreter.Evaluate(@"(add! 4 a)"));
			Assert.Null(interpreter.Evaluate(@"(add! 5 a)"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(count a)"));

			Assert.Equal(GetEnumerable(1, 2, 3, 4, 5), GetEntries(interpreter.Evaluate(@"a")));

			Assert.Null(interpreter.Evaluate(@"(remove-at! 1 a)"));
			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(remove-at! -1 a)"));
			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(remove-at! 4 a)"));

			Assert.Equal(GetEnumerable(1, 3, 4, 5), GetEntries(interpreter.Evaluate(@"a")));

			Assert.Null(interpreter.Evaluate(@"(remove! 4 a)"));

			Assert.Equal(GetEnumerable(1, 3, 5), GetEntries(interpreter.Evaluate(@"a")));
		}

		[Fact]
		public void TestInsert()
		{
			interpreter.Evaluate(@"(def a ![1 2 3])");
			interpreter.Evaluate(@"(insert! 1 2 a)");

			Assert.Equal(GetEnumerable(1, 2, 2, 3), GetEntries(interpreter.Evaluate(@"a")));

			interpreter.Evaluate(@"(insert! 4 2 a)");

			Assert.Equal(GetEnumerable(1, 2, 2, 3, 2), GetEntries(interpreter.Evaluate(@"a")));

			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(insert! -1 2 a)"));
			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(insert! 6 2 a)"));
		}

		[Fact]
		public void TestFirstLast()
		{
			Assert.Equal((long)1, interpreter.Evaluate(@"(first ![1 2 3 4 5])"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(last ![1 2 3 4 5])"));

			Assert.Null(interpreter.Evaluate(@"(first ![])"));
			Assert.Null(interpreter.Evaluate(@"(last ![])"));
		}

		[Fact]
		public void TestRestSkip()
		{
			interpreter.Evaluate(@"(def a ![1 2])");

			Assert.Equal(GetEnumerable(1, 2), GetEntries(interpreter.Evaluate(@"(skip 0 a)")));

			Assert.Equal(GetEnumerable(2), GetEntries(interpreter.Evaluate(@"(rest a)")));
			Assert.Equal(GetEnumerable(2), GetEntries(interpreter.Evaluate(@"(skip 1 a)")));

			Assert.Equal(GetEnumerable(), GetEntries(interpreter.Evaluate(@"(rest (rest a))")));
			Assert.Equal(GetEnumerable(), GetEntries(interpreter.Evaluate(@"(skip 2 a)")));
			Assert.Equal(GetEnumerable(), GetEntries(interpreter.Evaluate(@"(skip 5 a)")));
			Assert.Equal(GetEnumerable(), GetEntries(interpreter.Evaluate(@"(rest ![])")));

			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(skip -1 a)"));
		}

		[Fact]
		public void TestSet()
		{
			interpreter.Evaluate(@"(def a ![1 2 3])");
			interpreter.Evaluate(@"(set! 1 3 a)");

			Assert.Equal(GetEnumerable(1, 3, 3), GetEntries(interpreter.Evaluate(@"a")));

			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(set! -1 3 a)"));
			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(set! 3 3 a)"));
		}

		[Fact]
		public void TestReverse()
		{
			interpreter.Evaluate(@"(def a ![1 2 3])");
			Assert.Null(interpreter.Evaluate(@"(reverse! a)"));

			Assert.Equal(GetEnumerable(3, 2, 1), GetEntries(interpreter.Evaluate(@"a")));
		}

		[Fact]
		public void TestNth()
		{
			interpreter.Evaluate(@"(def a ![1 2 3])");

			Assert.Equal((long)1, interpreter.Evaluate(@"(nth 0 a)"));
			Assert.Equal((long)2, interpreter.Evaluate(@"(nth 1 a)"));
			Assert.Equal((long)3, interpreter.Evaluate(@"(nth 2 a)"));

			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(nth -1 a)"));
			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(nth 3 a)"));
		}

		[Fact]
		public void TestGet()
		{
			interpreter.Evaluate(@"(def a ![1 2 3])");

			Assert.Equal((long)1, interpreter.Evaluate(@"(get 0 a)"));
			Assert.Equal((long)2, interpreter.Evaluate(@"(get 1 a)"));
			Assert.Equal((long)3, interpreter.Evaluate(@"(get 2 a)"));

			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(get -1 a)"));
			Assert.Throws<ArgumentOutOfRangeException>(() => interpreter.Evaluate(@"(get 3 a)"));

			Assert.Equal((long)5, interpreter.Evaluate(@"(get -1 5 a)"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(get 3 5 a)"));
		}

		[Fact]
		public void TestContains()
		{
			interpreter.Evaluate(@"(def a ![1 2 3])");

			Assert.Equal(true, interpreter.Evaluate(@"(contains 2 a)"));
			Assert.Equal(false, interpreter.Evaluate(@"(contains 5 a)"));
		}

		[Fact]
		public void TestIteration()
		{
			interpreter.Evaluate(@"(def a ![1 2 3])");

			Assert.Equal(GetEnumerable(2, 4, 6), GetEntries(interpreter.Evaluate(@"(map #(* 2 %) a)")));
		}

		[Fact]
		public void TestToString()
		{
			Assert.Equal(@"![1 2 3]", interpreter.Evaluate(@"![1 2 3]")!.ToString());
			Assert.Equal(@"![1 2 3 4 5 6 7 8 9 10...]", interpreter.Evaluate(@"![1 2 3 4 5 6 7 8 9 10 11 12]")!.ToString());
		}
	}
}
