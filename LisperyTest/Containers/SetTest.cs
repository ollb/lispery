using Lispery;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace LisperyTest.Containers
{
	public class SetTest
	{
		private readonly Interpreter interpreter = new();

		private static IEnumerable<object?> GetEntries(object? obj)
		{
			Assert.True(obj is ISequence);
			return ((ISequence)obj!).GetEntries().OrderBy(v => v);
		}

		private static IEnumerable<object?> GetEnumerable(params int[] values)
		{
			return values.Select(v => (object)(long)v);
		}

		[Fact]
		public void TestCreation()
		{
			Assert.Equal(GetEnumerable(1, 2, 3), GetEntries(interpreter.Evaluate(@"#{1 2 3}")));
			Assert.Equal(GetEnumerable(1, 2, 3), GetEntries(interpreter.Evaluate(@"(make-set 1 2 3)")));

			Assert.Equal((long)3, interpreter.Evaluate(@"(count #{1 2 3})"));
		}

		[Fact]
		public void TestBuilder()
		{
			var set = new Set(new object?[] { (long)1 });

			var builder = set.ToBuilder();
			builder.Add((long)2);
			set = builder.ToImmutable();

			Assert.Equal(GetEnumerable(1, 2), GetEntries(set));

			builder = set.ToBuilder();
			builder.Clear();
			builder.AddRange(Enumerable.Range(3, 2).Select(v => (object)(long)v));
			set = builder.ToImmutable();

			Assert.Equal(GetEnumerable(3, 4), GetEntries(set));

			builder = new Set.Builder();
			builder.AddRange(Enumerable.Range(3, 2).Select(v => (object)(long)v));
			set = builder.ToImmutable();

			Assert.Equal(GetEnumerable(3, 4), GetEntries(set));
		}

		[Fact]
		public void TestEmpty()
		{
			Assert.Equal(0, Set.Empty.Count);
			Assert.Equal(Array.Empty<object?>(), Set.Empty);
		}

		[Fact]
		public void TestAddRemove()
		{
			interpreter.Evaluate(@"(def a #{1 2 3})");
			interpreter.Evaluate(@"(def a (add 2 a))");
			interpreter.Evaluate(@"(def a (add 5 a))");
			Assert.Equal((long)4, interpreter.Evaluate(@"(count a)"));

			Assert.Equal(GetEnumerable(1, 2, 3, 5), GetEntries(interpreter.Evaluate(@"a")));

			interpreter.Evaluate(@"(def a (remove 2 a))");

			Assert.Equal(GetEnumerable(1, 3, 5), GetEntries(interpreter.Evaluate(@"a")));
		}

		[Fact]
		public void TestContains()
		{
			interpreter.Evaluate(@"(def a #{1 2 3})");

			Assert.Equal(true, interpreter.Evaluate(@"(contains 2 a)"));
			Assert.Equal(false, interpreter.Evaluate(@"(contains 5 a)"));
		}

		[Fact]
		public void TestIteration()
		{
			interpreter.Evaluate(@"(def a #{1 2 3})");

			Assert.Equal(GetEnumerable(2, 4, 6), GetEntries(interpreter.Evaluate(@"(map #(* 2 %) a)")));
		}

		[Fact]
		public void TestToString()
		{
			Assert.Equal(@"#{1 2 3}", interpreter.Evaluate(@"#{1 2 3}")!.ToString());
			Assert.Equal(@"#{1 2 3 4 5 6 7 8 9 10...}", interpreter.Evaluate(@"#{1 2 3 4 5 6 7 8 9 10 11 12}")!.ToString());
		}
	}
}