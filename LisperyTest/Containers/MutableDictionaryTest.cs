﻿using Lispery;
using Lispery.Containers;
using Lispery.Interfaces;
using Lispery.Util;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LisperyTest.Containers
{
	public class MutableDictionaryTest
	{
		private readonly Interpreter interpreter = new();

		private static IEnumerable<(string, long)> GetEntries(object? obj)
		{
			return ((ISequence)obj!)
				.GetEntries()
				.Cast<KeyValuePair<object, object?>>()
				.Select(e => (((Keyword)e.Key).Name, (long)e.Value!))
				.OrderBy(e => e);
		}

		private static IEnumerable<(string, long)> GetEnumerable(params object[] values)
		{
			return values.GroupKeyValue()
				.Select(e => ((string)e.Key, Convert.ToInt64(e.Value)))
				.OrderBy(e => e);
		}

		[Fact]
		public void TestCreation()
		{
			Assert.Equal(
				GetEnumerable("a", 1, "b", 2, "c", 3),
				GetEntries(interpreter.Evaluate(@"!{:a 1 :b 2 :c 3}")));

			Assert.Equal(
				GetEnumerable("a", 1, "b", 2, "c", 3),
				GetEntries(interpreter.Evaluate(@"(make-dict! :a 1 :b 2 :c 3)")));

			Assert.Equal((long)3, interpreter.Evaluate(@"(count !{:a 1 :b 2 :c 3})"));

			Assert.Throws<ArgumentNullException>(() => interpreter.Evaluate(@"!{:a 1 nil 2}"));
		}

		[Fact]
		public void TestGetSet()
		{
			interpreter.Evaluate(@"(def a !{:a 1 :b 2})");
			
			Assert.Null(interpreter.Evaluate(@"(set! :b 3 a)"));
			Assert.Equal(GetEnumerable("a", 1, "b", 3), GetEntries(interpreter.Evaluate(@"a")));

			Assert.Null(interpreter.Evaluate(@"(set! :c 5 a)"));
			Assert.Equal(GetEnumerable("a", 1, "b", 3, "c", 5), GetEntries(interpreter.Evaluate(@"a")));

			Assert.Equal((long)1, interpreter.Evaluate(@"(get :a a)"));
			Assert.Equal((long)5, interpreter.Evaluate(@"(get :c a)"));
			Assert.Null(interpreter.Evaluate(@"(get :x a)"));

			Assert.Equal((long)1, interpreter.Evaluate(@"(get :a 9 a)"));
			Assert.Equal((long)9, interpreter.Evaluate(@"(get :d 9 a)"));
		}

		[Fact]
		public void TestRemove()
		{
			interpreter.Evaluate(@"(def a !{:a 1 :b 2})");

			Assert.Null(interpreter.Evaluate(@"(remove! :b a)"));
			Assert.Equal(GetEnumerable("a", 1), GetEntries(interpreter.Evaluate(@"a")));

			Assert.Null(interpreter.Evaluate(@"(remove! :x a)"));
			Assert.Equal(GetEnumerable("a", 1), GetEntries(interpreter.Evaluate(@"a")));
		}

		[Fact]
		public void TestContains()
		{
			interpreter.Evaluate(@"(def a !{:a 1 :b 2})");

			Assert.Equal(true, interpreter.Evaluate(@"(contains :a a)"));
			Assert.Equal(false, interpreter.Evaluate(@"(contains :c a)"));
			Assert.Equal(false, interpreter.Evaluate(@"(contains nil a)"));
		}

		[Fact]
		public void TestToString()
		{
			Assert.Equal(@"!{:a 1}", interpreter.Evaluate(@"!{:a 1}")!.ToString());
			Assert.Contains("...", interpreter.Evaluate(@"!{:a 1 :b 2 :c 3 :d 4 :e 5 :f 6}")!.ToString());
		}
	}
}
