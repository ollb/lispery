# Lispery

Simple embeddable LISP implementation for .NET inspired by Schemy and Clojure. It has no additional
dependencies apart from .NET Core 6.0. The goal is to provide a flexible and reasonably powerful language
that can easily be used in .NET projects.

The language supports the following features:

- Immutable and mutable data structures (list, vector, dictionary, set).
- Extensibility using LISP macros.
- Tail call optimization.
- Regular expressions.
- Pattern matching for data structures.
- Optional named parameters using pattern matching.
- Generalized sequences with lazy execution.
- Safe execution due to the exclusion of IO functionality by default.
- Easy addition of functions / special forms from .NET, including overloading.

The implementation aims to be reasonably efficient and light-weight. It is not intended to be highly
optimized, in order to keep the code base simple.

The language is also making its own compromises when it comes to language design. It is not aimed to
exactly replicate any existing language. There are a lot of inspirations especially from Clojure, but
there are also a lot of differences, for example with the pattern matching vs destructuring in Clojure.

_Note:_ please note that the project is still in an early stage and subject to frequent changes.

## Usage

The project currently provides a Nuget repository through the Gitlab package registry. Alternatively,
you can also just add the source code directly to your own solution. There is also a REPL program
(read-eval-print-loop) included in the project. You can use this, to test out Lispery or to do
prototyping / testing.

## Build

The project can be built using Visual Studio or using the `dotnet build` command line. For reference,
you can also have a look at the CI configuration in `.gitlab-ci.yml`.

## Getting Started

Code can be executed using the `Interpreter` class as follows:

```c#
Interpreter interpreter = new();
interpreter.Evaluate(@"(print ""Hello World!"")");
```

Note that `print` will by default use `Console.WriteLine()`, but this can be changed, by adding a
handler for the `Print` event. For example:

```c#
interpreter.Print += (s, e) => Debug.WriteLine(e.Output);
```

The result of the evaluation will be returned as an `object?` from `Evaluate()`. If errors occur during
execution, `Evaluate()` will throw an exception directly. You may need to use C# pattern matching or
`switch` statements / expressions to determine the type of the result.

The environment is retained between different executions of `Evaluate()`. If you want to restore a clean
environment, you have to create a new `Interpreter` object.

## Documentation

The documentation can be found at https://ollb.gitlab.io/lispery/.

## Examples

Some example programs written in Lispery are provided in [`LisperyREPL/Examples`](https://gitlab.com/ollb/lispery/-/tree/main/LisperyREPL/Examples).
They are based on the problems from [Project Euler](https://projecteuler.net).
