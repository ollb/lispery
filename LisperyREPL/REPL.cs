﻿using Lispery;
using Lispery.Values;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LisperyREPL
{
	public class REPL
	{
		private readonly CodeEditor editor = new();
		private readonly HashSet<string> importedFiles = new();
		private string? historyPath = null;

		protected Interpreter Interpreter { get; set; }

		protected Lispery.Environment Environment
		{
			get { return Interpreter.Environment; }
		}

		public REPL()
		{
			Terminal.HandleCtrlC = false;
			Reset();
			Debug.Assert(Interpreter != null);
		}

		/// <summary>
		/// Gets or sets the path of the history file.
		/// </summary>
		public string? HistoryPath
		{
			get { return historyPath; }

			set
			{
				if (value != historyPath)
				{
					historyPath = value;
					LoadHistory();
				}
			}
		}

		/// <summary>
		/// Resets the console and reloads all imported files.
		/// </summary>
		public virtual void Reset()
		{
			Interpreter = new Interpreter();
			Interpreter.Print += Interpreter_Print;
			Interpreter.Interrupt += Interpreter_Interrupt;
			ReloadFiles();
		}

		/// <summary>
		/// Reloads all imported files.
		/// </summary>
		public void ReloadFiles()
		{
			LoadFiles(importedFiles.ToArray());
		}

		/// <summary>
		/// Imports a file into the REPL.
		/// </summary>
		public bool ImportFiles(params string[] files)
		{
			bool result = true;

			foreach (var file in files)
			{
				if (!LoadFiles(file))
				{
					importedFiles.Add(file);
					result = false;
				}
			}

			return result;
		}

		/// <summary>
		/// Loads a file into the REPL (without importing it).
		/// </summary>
		public bool LoadFiles(params string[] files)
		{
			bool result = true;

			foreach (var file in files)
			{
				if (!File.Exists(file))
				{
					Terminal.WriteLine($"File not found: {file}");
					result = false;
					continue;
				}

				using Stream stream = File.OpenRead(file);
				using TextReader reader = new StreamReader(stream);

				try
				{
					PrepareQueryEnvironment();
					Interpreter.Evaluate(reader);
				}
				catch (Exception ex)
				{
					Terminal.WriteLine($"Error in {file}: {ex.Message}");
				}
			}

			return result;
		}

		/// <summary>
		/// Loads the history file.
		/// </summary>
		private void LoadHistory()
		{
			if (File.Exists(historyPath))
			{
				editor.History = File.ReadAllLines(historyPath)
					.Select(l => l.Trim())
					.Where(l => !string.IsNullOrEmpty(l))
					.ToList();
			}
		}

		/// <summary>
		/// Saves the history file.
		/// </summary>
		private void SaveHistory()
		{
			if (historyPath != null)
			{
				File.WriteAllLines(historyPath, editor.History);
			}
		}


		/// <summary>
		/// Prints the result of the evaluation.
		/// </summary>
		protected virtual void PrintResult(object? result)
		{
			if (result is EmptyResult)
			{
				return;
			}

			string resultStr = Formatter.Format(result);

			CodeTokenizer tokenizer = new();
			TokenBuffer tokens = tokenizer.Tokenize(resultStr);
			tokens.RenderLine();
		}

		/// <summary>
		/// Runs the REPL.
		/// </summary>
		public void Run()
		{
			bool timing = true;
			bool type = false;

			while (true)
			{
				string? expr = editor.ReadExpression();
				SaveHistory();

				if (expr == null)
				{
					break;
				}
				else if (expr == @"\time")
				{
					timing = !timing;
					Terminal.WriteLine("Times " + (timing ? "enabled" : "disabled"));
				}
				else if (expr == @"\type")
				{
					type = !type;
					Terminal.WriteLine("Types " + (type ? "enabled" : "disabled"));
				}
				else if (expr == @"\reload")
				{
					Terminal.WriteLine("Reloading files...");
					ReloadFiles();
				}
				else if (expr == @"\reset")
				{
					Terminal.WriteLine("Resetting REPL...");
					Reset();
				}
				else if (expr.StartsWith(@"\load "))
				{
					LoadFiles(expr[6..]);
				}
				else
				{
					Stopwatch sw = new();
					sw.Start();

					try
					{
						PrepareQueryEnvironment();
						object? result = Interpreter.Evaluate(expr);
						PrintResult(result);

						if (type)
						{
							Terminal.SetFgColor(TerminalColor.Gray);
							Terminal.WriteLine(result?.GetType().ToString() ?? "nil");
							Terminal.ResetFgColor();
						}
					}
					catch (Exception ex)
					{
						Terminal.WriteLine($"Error: {ex.Message}");
					}

					if (timing)
					{
						Terminal.SetFgColor(TerminalColor.Gray);
						Terminal.WriteLine($"{(int)Interpreter.EvaluationTime.TotalMilliseconds} ms, {Interpreter.EvaluationSteps} steps");
						Terminal.ResetFgColor();
					}
				}
			}
		}

		protected virtual void PrepareQueryEnvironment()
		{
		}

		protected virtual void PrintOutput(string output)
		{
			Terminal.WriteLine(output);
		}
			
		private void Interpreter_Print(object? sender, PrintEventArgs e)
		{
			PrintOutput(e.Output);
		}

		private void Interpreter_Interrupt(object? sender, InterruptEventArgs e)
		{
			ConsoleKeyInfo? key = Terminal.PeekKey(true);

			if (key != null && key.Value.Key == ConsoleKey.C && key.Value.Modifiers.HasFlag(ConsoleModifiers.Control))
			{
				throw new InterruptException();
			}
		}
	}
}
