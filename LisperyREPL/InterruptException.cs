﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LisperyREPL
{
	public class InterruptException : Exception
	{
		public InterruptException() :
			base("Execution interrupt")
		{
		}
	}
}
