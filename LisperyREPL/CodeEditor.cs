﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LisperyREPL
{
	internal class CodeEditor
	{
		private StringBuilder sb = new();
		private int index = 0;
		private List<string> history = new();
		private int historyIndex = -1;
		private CodeTokenizer tokenizer;
		private TokenBuffer tokens;

		public CodeEditor()
        {
			Reset();

			Debug.Assert(tokenizer != null);
			Debug.Assert(tokens != null);
		}

		public List<string> History
		{
			get
			{
				return history;
			}

			set
			{
				history = value;
				historyIndex = -1;
			}
		}

		public int HistoryLimit { get; set; } = 1000;

		private void Reset()
		{
			sb.Clear();
			index = 0;
			historyIndex = -1;

			tokenizer = new CodeTokenizer
			{
				PrefixToken         = new Token(">> ", TerminalColor.Gray),
				ContinuePrefixToken = new Token("+> ", TerminalColor.Gray)
			};

			tokens = tokenizer.Tokenize(string.Empty);
		}

		public string? ReadExpression()
		{
			Reset();

			tokens.Render();
			tokens.SyncTerminalCursor();

			while (true)
			{
				var info = Terminal.ReadKey(true);

				if (info.Key == ConsoleKey.Enter)
				{
					if (tokenizer.NestLevel == 0)
					{
						string code = sb.ToString().Trim();

						if (!string.IsNullOrEmpty(code) &&
							(historyIndex < 0 || historyIndex != History.Count - 1))
						{
							if (History.Count >= HistoryLimit)
							{
								History.RemoveAt(0);
							}

							History.Add(code);
						}

						Terminal.WriteLine();
						return code;
					}

					int indent = 2 * tokenizer.NestLevel;
					sb.Insert(index, '\n');
					sb.Insert(index + 1, new string(' ', indent));
					index += 1 + indent;
				}
				else if (info.Key == ConsoleKey.Delete)
				{
					historyIndex = -1;
					Delete();
				}
				else if (info.Key == ConsoleKey.Backspace)
				{
					historyIndex = -1;
					Backspace();
				}
				else if (info.Key == ConsoleKey.UpArrow)
				{
					HistoryUp();
				}
				else if (info.Key == ConsoleKey.DownArrow)
				{
					HistoryDown();
				}
				else if (info.Key == ConsoleKey.LeftArrow)
				{
					MoveLeft();
				}
				else if (info.Key == ConsoleKey.RightArrow)
				{
					MoveRight();
				}
				else if (info.Key == ConsoleKey.Home)
				{
					index = 0;
				}
				else if (info.Key == ConsoleKey.End)
				{
					index = sb.Length;
				}
				else if (info.Key == ConsoleKey.Tab)
				{
					historyIndex = -1;
					sb.Insert(index, "  ");
					index += 2;
				}
				else if (info.Key == ConsoleKey.R && info.Modifiers.HasFlag(ConsoleModifiers.Control))
				{
					Terminal.WriteLine();

					if (info.Modifiers.HasFlag(ConsoleModifiers.Shift))
					{
						return @"\reset";
					}
					
					return @"\reload";
				}
				else if (info.Key == ConsoleKey.C && info.Modifiers.HasFlag(ConsoleModifiers.Control))
				{
					Terminal.WriteLine();
					return null;
				}
				else if (info.KeyChar != '\0')
				{
					historyIndex = -1;
					sb.Insert(index, info.KeyChar);
					index++;
				}

				TokenBuffer newTokens = tokenizer.Tokenize(sb, index);
				tokens.Update(newTokens);
				tokens.SyncTerminalCursor();
			}
		}

		private void HistoryUp()
		{
			if (History.Count == 0 || historyIndex == 0)
			{
				return;
			}

			if (historyIndex < 0)
			{
				historyIndex = History.Count;
			}

			sb = new StringBuilder(History[--historyIndex]);
			index = sb.Length;
		}

		private void HistoryDown()
		{
			if (History.Count == 0 || historyIndex < 0)
			{
				return;
			}

			if (historyIndex == History.Count - 1)
			{
				sb.Clear();
				index = 0;
				historyIndex = -1;
				return;
			}

			sb = new StringBuilder(History[++historyIndex]);
			index = sb.Length;
		}

		private void Backspace()
		{
			if (index > 0)
			{
				MoveLeft();
				Delete();
			}
		}

		private void Delete()
		{
			if (index < sb.Length)
			{
				sb.Remove(index, 1);
			}
		}

		private void MoveLeft()
		{
			index = Math.Max(0, index - 1);
		}

		private void MoveRight()
		{
			index = Math.Min(sb.Length, index + 1);
		}
	}
}
