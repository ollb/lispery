﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LisperyREPL
{
	public class TokenBuffer : IEnumerable<Token>
	{
		private HashSet<Token> tokens = new();
		private Position origin;
		private int scrollOffset;

		public TokenBuffer()
		{
		}

		public TokenBuffer(IEnumerable<Token> tokens)
		{
			this.tokens = new HashSet<Token>(tokens);
		}

		/// <summary>
		/// The cursor position relative to the origin.
		/// </summary>
		public Position CursorPosition
		{
			get;
			set;
		}

		/// <summary>
		/// Moves the terminal cursor to the stored cursor position.
		/// </summary>
		public bool SyncTerminalCursor()
		{
			if (CursorPosition.Y >= scrollOffset)
			{
				Terminal.SetPosition(CursorPosition + ScrolledOrigin);
				return true;
			}

			return false;
		}

		/// <summary>
		/// Returns the total height of the token set.
		/// </summary>
		public int Height
		{
			get { return GetHeight(tokens); }
		}

		private static int GetHeight(IEnumerable<Token> tokens)
		{
			if (!tokens.Any())
			{
				return 1;
			}

			return tokens.Max(t => t.Position.Y) + 1;
		}

		/// <summary>
		/// Moves the cursor to the start of the line following the token set.
		/// </summary>
		public bool GotoNextLine()
		{
			return Terminal.SetPosition(Origin + new Position(0, Height));
		}

		public Position Origin
		{
			get { return origin; }
		}

		public Position ScrolledOrigin
		{
			get { return Origin - new Position(0, scrollOffset); }
		}

		/// <summary>
		/// Renders the token set at the current position.
		/// </summary>
		public void Render()
		{
			Render(Terminal.GetAbsolutePosition());
		}

		/// <summary>
		/// Renders the token set at the given position.
		/// </summary>
		public void Render(Position origin)
		{
			this.origin = origin;
			Terminal.ShowCursor(false);
			UpdateScroll(Height);
			RenderTokens(tokens);
			Terminal.ShowCursor(true);
		}

		/// <summary>
		/// Renders the token set at the current position and writes a newline.
		/// </summary>
		public void RenderLine()
		{
			Render();
			Terminal.WriteLine();
		}

		public void Erase()
		{
			EraseTokens(tokens);
		}

		private bool UpdateScroll(int requiredHeight)
		{
			int linesBelowOrigin = Terminal.TotalHeight - origin.Y;
			int linesMissing = requiredHeight - linesBelowOrigin;

			// We have enough space and we are not scrolled. Everything good.
			if (linesMissing <= 0 && scrollOffset <= 0)
			{
				return false;
			}

			int linesAboveOrigin = origin.Y - Terminal.WindowOffset;

			// If we have some space above the origin, scroll the terminal to use it.
			if (linesMissing > 0 && linesAboveOrigin > 0)
			{
				int scrollAmount = Math.Min(linesAboveOrigin, linesMissing);
				Terminal.ScrollUp(scrollAmount);

				origin.Y -= scrollAmount;
				linesMissing -= scrollAmount;
				//linesBelowOrigin += scrollAmount;
				//linesAboveOrigin -= scrollAmount;
			}

			// Update the scroll offset to show as many lines as we can.
			if (scrollOffset != linesMissing)
			{
				// Erase the tokens, update the scroll offset.
				Erase();
				scrollOffset = linesMissing;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Updates a token set that is already on screen.
		/// </summary>
		public void Update(TokenBuffer newTokens)
		{
			Terminal.ShowCursor(false);

			if (UpdateScroll(GetHeight(newTokens)))
			{
				RenderTokens(newTokens);
				tokens = new HashSet<Token>(newTokens);
				CursorPosition = newTokens.CursorPosition;
				return;
			}

			// If we already have something rendered, figure out the difference.
			HashSet<Token> addedTokens = new(newTokens);
			HashSet<Token> removedTokens = tokens;

			addedTokens.ExceptWith(tokens);
			removedTokens.ExceptWith(newTokens);

			// Erase / render / update set.
			EraseTokens(removedTokens);
			RenderTokens(addedTokens);

			tokens = new HashSet<Token>(newTokens);
			CursorPosition = newTokens.CursorPosition;

			Terminal.ShowCursor(true);
		}

		/// <summary>
		/// Moves a token set that is already on screen.
		/// </summary>
		public void Move(Position destination)
		{
			Terminal.ShowCursor(false);
			EraseTokens(tokens);
			origin = destination;
			RenderTokens(tokens);
			Terminal.ShowCursor(true);
		}

		private void RenderTokens(IEnumerable<Token> tokens)
		{
			Position bottomRight = new();

			foreach (var token in tokens)
			{
				if (token.Position.Y < scrollOffset)
				{
					continue;
				}

				token.Render(ScrolledOrigin);

				if (token.Position.Y > bottomRight.Y)
				{
					bottomRight.Y = token.Position.Y;
					bottomRight.X = token.Position.X + token.Length;
				}
				else if (token.Position.Y == bottomRight.Y)
				{
					bottomRight.X = Math.Max(bottomRight.X, token.Position.X + token.Length);
				}
			}

			// Leave the cursor at the bottom right.
			Terminal.SetPosition(bottomRight + ScrolledOrigin);
		}

		private void EraseTokens(IEnumerable<Token> tokens)
		{
			foreach (var token in tokens)
			{
				if (token.Position.Y >= scrollOffset)
				{
					token.Erase(ScrolledOrigin);
				}
			}
		}

		/// <summary>
		/// Adds a token to the token set.
		/// </summary>
		public void Add(Token token)
		{
			tokens.Add(token);
		}

		/// <summary>
		/// Clears the token set.
		/// </summary>
		public void Clear()
		{
			tokens.Clear();
		}

		public IEnumerator<Token> GetEnumerator()
		{
			return tokens.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
