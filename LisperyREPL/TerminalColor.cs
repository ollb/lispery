﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LisperyREPL
{
	public class TerminalColor
	{
		public const uint Gray = 0x808080u;
		public const uint Red = 0xff0000u;
		public const uint Green = 0x00ff00u;
		public const uint Blue = 0x6060ffu;
		public const uint Yellow = 0xffff00u;
		public const uint Cyan = 0x00ffffu;
		public const uint Magenta = 0xff00ffu;
		public const uint White = 0xffffffu;
		public const uint Invalid = 0xffffffffu;
		public const uint Default = 0xffffffffu - 1;

		public static uint RGB(byte r, byte g, byte b)
		{
			return (uint)r << 16 | (uint)g << 8 | (uint)b;
		}
	}
}
