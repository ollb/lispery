﻿using LisperyREPL;
using System.Globalization;
using System.Reflection;

CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;
CultureInfo.CurrentUICulture = CultureInfo.InvariantCulture;

var assemblyName = Assembly.GetCallingAssembly().GetName();
Terminal.WriteLine($"{assemblyName.Name} {assemblyName.Version}");
Terminal.WriteLine();

REPL repl = new();

repl.HistoryPath = Path.Combine(
	Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
	".lispery.history");

repl.ImportFiles(args);
repl.Run();