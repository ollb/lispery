﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LisperyREPL
{
	public record Token
	{
		private Position position;

		public string Text { get; private set; }
		public uint FgColor { get; set; } = TerminalColor.Invalid;
		public uint BgColor { get; set; } = TerminalColor.Invalid;
		public bool Underline { get; set; } = false;

		public ref Position Position
		{
			get { return ref position; }
		}

		public int Length
		{
			get { return Text.Length; }
		}

		public Token(Position position, Token rhs)
		{
			this.position = position;
			this.Text = rhs.Text;
			this.FgColor = rhs.FgColor;
			this.BgColor = rhs.BgColor;
		}

		public Token(string text, uint fgColor = TerminalColor.Invalid, uint bgColor = TerminalColor.Invalid, bool underline = false)
		{
			this.Text = text;
			this.FgColor = fgColor;
			this.BgColor = bgColor;
			this.Underline = underline;
		}

		public Token(Position position, string text, uint fgColor = TerminalColor.Invalid, uint bgColor = TerminalColor.Invalid, bool underline = false)
		{
			this.position = position;
			this.Text = text;
			this.FgColor = fgColor;
			this.BgColor = bgColor;
			this.Underline = underline;
		}

		/// <summary>
		/// Renders the token on the screen.
		/// </summary>
		/// <remarks>
		/// Returns <c>true</c> if the token could be rendered to the terminal buffer.
		/// </remarks>
		public bool Render(Position origin)
		{
			if (!IsBuffered(origin))
			{
				return false;
			}

			Terminal.SetPosition(origin + position);
			bool changed = Terminal.SetColors(FgColor, BgColor);
			Terminal.SetUnderline(Underline);
			Terminal.Write(Text);
			Terminal.SetUnderline(false);
			Terminal.ResetColors(changed);
			return true;
		}

		/// <summary>
		/// Erases the token from the screen.
		/// </summary>
		public void Erase(Position origin)
		{
			if (!IsBuffered(origin))
			{
				return;
			}

			Terminal.SetPosition(origin + position);
			Terminal.Write(new String(' ', Text.Length));
		}

		/// <summary>
		/// Determines if the token is in the terminal buffer.
		/// </summary>
		public bool IsBuffered(Position origin)
		{
			// Tokens don't spread multiple lines. So if the start position
			// is in the buffer, so is the rest of the token.
			return Terminal.IsBuffered(origin + position);
		}

		/// <summary>
		/// Determines if the token is visible.
		/// </summary>
		public bool IsVisible(Position origin)
		{
			Position startPosition = origin + position;
			Position endPosition = startPosition + new Position(Text.Length - 1, 0);
			return Terminal.IsVisible(startPosition) && Terminal.IsVisible(endPosition);
		}
	}
}
