﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LisperyREPL
{
	public record struct Position(int X, int Y)
	{
		public override string ToString()
		{
			return $"({X}, {Y})";
		}

		public static Position operator + (Position lhs, Position rhs)
		{
			return new Position(lhs.X + rhs.X, lhs.Y + rhs.Y);
		}

		public static Position operator - (Position lhs, Position rhs)
		{
			return new Position(lhs.X - rhs.X, lhs.Y - rhs.Y);
		}
	}
}
