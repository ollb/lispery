﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LisperyREPL
{
	public class CodeTokenizer
	{
		private readonly StringBuilder sb = new();
		private TextReader? reader;
		private TokenBuffer? tokens;

		private Position position;
		private int width;

		static readonly uint[] colors = new uint[]
		{
			TerminalColor.Green,
			TerminalColor.Yellow,
			TerminalColor.Cyan,
			TerminalColor.Magenta,
			TerminalColor.Blue,
			TerminalColor.Red
		};

		private static uint GetFgColor(int level)
		{
			return level <= 0 ? TerminalColor.White : colors[(level - 1) % colors.Length];
		}

		private static uint GetBgColor(int level)
		{
			return level <= 0 ? TerminalColor.Red : TerminalColor.Default;
		}

		/// <summary>
		/// The current level of indentation.
		/// </summary>
		public int NestLevel { get; private set; }

		/// <summary>
		/// Sets a prefix token to prepend on every line.
		/// </summary>
		public Token? PrefixToken { get; set; }

		/// <summary>
		/// Sets a different prefix token to prepend on continuation lines.
		/// </summary>
		public Token? ContinuePrefixToken { get; set; }

		private uint fgColor = TerminalColor.Invalid;
		private uint bgColor = TerminalColor.Invalid;

		private static readonly Dictionary<char, char> matchingChars = new()
		{
			{ '(', ')' }, { '[', ']' }, { '{', '}' }
		};

		public TokenBuffer Tokenize(string str, int cursorIndex = -1, int width = 0)
		{
			return Tokenize(new StringReader(str), cursorIndex, width);
		}

		public TokenBuffer Tokenize(StringBuilder sb, int cursorIndex = -1, int width = 0)
		{
			return Tokenize(sb.ToString(), cursorIndex, width);
		}

		public TokenBuffer Tokenize(TextReader reader, int cursorIndex = -1, int width = 0)
		{
			if (width <= 0)
			{
				width = Terminal.WindowWidth;
			}

			this.reader = reader;
			this.width = width;

			fgColor = TerminalColor.Invalid;
			bgColor = TerminalColor.Invalid;

			position = new Position();
			tokens = new TokenBuffer();

			if (PrefixToken != null)
			{
				tokens.Add(new Token(position, PrefixToken));
				position.X += PrefixToken.Length;

				if (position.X >= width)
				{
					throw new ArgumentException("prefix token is too long");
				}
			}

			Stack<Token> parenStack = new();

			if (cursorIndex <= 0)
			{
				tokens.CursorPosition = position;
			}

			bool inString = false;
			int level = 0;
			char lastChr = '\0';
			int chrInt;
			bool lastCursor = false;

			while ((chrInt = reader.Read()) >= 0)
			{
				char chr = (char)chrInt;

				if (chr == '\r' || chr == '\n')
				{
					ReadIfPresent('\n');
					AdvanceLine();
				}
				else
				{
					bool handled = false;

					if (inString)
					{
						if (chr == '\"' && lastChr != '\\')
						{
							sb.Append(chr);
							Advance();
							MakeTextToken();
							fgColor = TerminalColor.Invalid;
							inString = false;
							handled = true;
						}
					}
					else
					{
						switch (chr)
						{
							case '\"':
								MakeTextToken();
								fgColor = TerminalColor.Cyan;
								inString = true;
								break;

							case '[': goto case '(';
							case '{': goto case '(';
							case '(':
								level++;
								MakeTextToken();
								parenStack.Push(MakeCharToken(chr.ToString(), GetFgColor(level), GetBgColor(level)));
								Advance();
								handled = true;
								break;

							case ']': goto case ')';
							case '}': goto case ')';
							case ')':
								if (lastCursor && parenStack.Count > 0)
                                {
									parenStack.First().Underline = true;
                                }

								bool error = false;

								if (parenStack.Count > 0)
								{
									Token token = parenStack.Pop();

									if (token.Text.Length != 1 || matchingChars[token.Text[0]] != chr)
									{
										error = true;
									}
								}
								else
								{
									error = true;
								}

								MakeTextToken();

								if (error)
								{
									MakeCharToken(chr.ToString(), TerminalColor.White, TerminalColor.Red);
								}
								else
								{
									MakeCharToken(chr.ToString(), GetFgColor(level), GetBgColor(level));
									level--;
								}

								Advance();
								handled = true;
								break;
						}
					}

					if (!handled)
					{
						sb.Append(chr);
						Advance();
					}
				}

				lastCursor = false;

				if (cursorIndex > 0 && --cursorIndex == 0)
				{
					tokens.CursorPosition = position;
					lastCursor = true;
				}

				lastChr = chr;
			}

			if (cursorIndex > 0)
			{
				tokens.CursorPosition = position;
			}

			MakeTextToken();

			NestLevel = level;
			return tokens;
		}

		private void Advance()
		{
			position.X++;

			if (position.X >= width)
			{
				AdvanceLine();
			}
		}

		private void AdvanceLine()
		{
			MakeTextToken();
			position.X = 0;
			position.Y++;

			Token? prefixToken = ContinuePrefixToken ?? PrefixToken;

			if (prefixToken != null)
			{
				tokens!.Add(new Token(position, prefixToken));
				position.X += prefixToken.Length;
			}
		}

		private void MakeTextToken()
		{
			if (sb.Length <= 0)
			{
				return;
			}

			Position tokenPosition = position;
			tokenPosition.X -= sb.Length;

			Token token = new(tokenPosition, sb.ToString(), fgColor, bgColor);
			tokens!.Add(token);
			sb.Clear();
		}

		private Token MakeCharToken(string text, uint fgColor = TerminalColor.Invalid, uint bgColor = TerminalColor.Invalid, bool underline = false)
		{
			Token result = new(position, text, fgColor, bgColor, underline);
			tokens!.Add(result);
			return result;
		}

		private void ReadIfPresent(char chr)
		{
			if (reader!.Peek() == chr)
			{
				reader.Read();
			}
		}
	}
}
