﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LisperyREPL
{
	[SuppressMessage("ApiDesign", "RS0030")]
	public static class Terminal
	{
		/// <summary>
		/// Returns the Y offset of the current buffer.
		/// </summary>
		public static int BufferOffset { get; private set; } = 0;

		/// <summary>
		/// Gets or sets whether Ctrl+C is handled automatically.
		/// </summary>
		public static bool HandleCtrlC
		{
			get { return !System.Console.TreatControlCAsInput; }
			set { System.Console.TreatControlCAsInput = !value; }
		}

		/// <summary>
		/// Returns if a key is available in the input stream.
		/// </summary>
		public static bool HasKey
		{
			get { return System.Console.KeyAvailable; }
		}

		/// <summary>
		/// Returns the Y offset of the current window.
		/// </summary>
		public static int WindowOffset
		{
			get { return BufferOffset + System.Console.WindowTop; }
		}

		/// <summary>
		/// Returns the height of the window.
		/// </summary>
		public static int WindowHeight
		{
			get { return System.Console.WindowHeight; }
		}

		/// <summary>
		/// Returns the width of the window.
		/// </summary>
		public static int WindowWidth
		{
			get { return System.Console.WindowWidth; }
		}

		/// <summary>
		/// Returns the height of the buffer.
		/// </summary>
		public static int BufferHeight
		{
			get { return System.Console.BufferHeight; }
		}

		/// <summary>
		/// Returns the width of the buffer.
		/// </summary>
		public static int BufferWidth
		{
			get { return System.Console.BufferWidth; }
		}

		/// <summary>
		/// Returns the total height (for absolute coordinates).
		/// </summary>
		public static int TotalHeight
		{
			get { return BufferOffset + System.Console.BufferHeight; }
		}

		/// <summary>
		/// Returns the absolute cursor position.
		/// </summary>
		public static Position GetAbsolutePosition()
		{
			var (x, y) = System.Console.GetCursorPosition();
			return new Position(x, y + BufferOffset);
		}

		/// <summary>
		/// Returns the cursor position in the buffer.
		/// </summary>
		public static Position GetBufferPosition()
		{
			var (x, y) = System.Console.GetCursorPosition();
			return new Position(x, y);
		}

		/// <summary>
		/// Returns the cursor position in the screen.
		/// </summary>
		public static Position GetScreenPosition()
		{
			var (x, y) = System.Console.GetCursorPosition();

			return new Position(
				x - System.Console.WindowLeft,
				y - System.Console.WindowTop);
		}

		/// <summary>
		/// Reads a key from the keyboard.
		/// </summary>
		public static ConsoleKeyInfo ReadKey(bool intercept)
		{
			return System.Console.ReadKey(intercept);
		}

		/// <summary>
		/// Reads a key from the keyboard, if one is available in the input stream.
		/// </summary>
		public static ConsoleKeyInfo? PeekKey(bool intercept)
		{
			if (!HasKey)
			{
				return null;
			}

			return ReadKey(intercept);
		}

		/// <summary>
		/// Moves the cursor to the specified absolute position.
		/// </summary>
		public static bool SetPosition(Position position)
		{
			if (!IsBuffered(position))
			{
				return false;
			}

			System.Console.SetCursorPosition(position.X, position.Y - BufferOffset);
			return true;
		}

		/// <summary>
		/// Determines if the given absolute position is in the buffer.
		/// </summary>
		public static bool IsBuffered(Position position)
		{
			return
				position.Y >= BufferOffset &&
				position.Y < BufferOffset + System.Console.BufferHeight;
		}

		/// <summary>
		/// Determines if the given absolute position is on the screen.
		/// </summary>
		public static bool IsVisible(Position position)
		{
			int windowLeft = System.Console.WindowLeft;

			return
				position.Y >= WindowOffset &&
				position.Y < WindowOffset + System.Console.WindowHeight &&
				position.X >= windowLeft &&
				position.X < windowLeft + System.Console.WindowWidth;
		}

		/// <summary>
		/// Writes the given value.
		/// </summary>
		public static void Write(int value)
		{
			System.Console.Write(value);
		}

		/// <summary>
		/// Writes the given text.
		/// </summary>
		public static void Write(char chr)
		{
			System.Console.Write(chr);
		}

		/// <summary>
		/// Writes the given text.
		/// </summary>
		public static void Write(string text)
		{
			System.Console.Write(text);
		}

		/// <summary>
		/// Writes the given text.
		/// </summary>
		public static void Write(string format, params object?[] args)
		{
			System.Console.Write(format, args);
		}

		/// <summary>
		/// Writes the given value and a newline.
		/// </summary>
		public static void WriteLine(int value)
		{
			Write(value);
			WriteLine();
		}

		/// <summary>
		/// Writes the given value and a newline.
		/// </summary>
		public static void WriteLine(char chr)
		{
			Write(chr);
			WriteLine();
		}

		/// <summary>
		/// Writes the given text and a newline.
		/// </summary>
		public static void WriteLine(string text)
		{
			Write(text);
			WriteLine();
		}

		/// <summary>
		/// Writes the given text and a newline.
		/// </summary>
		public static void WriteLine(string format, params object?[] args)
		{
			Write(format, args);
			WriteLine();
		}

		/// <summary>
		/// Writes a newline.
		/// </summary>
		public static void WriteLine()
		{
			int windowBottom = System.Console.WindowTop + System.Console.WindowHeight;

			// Determine if we will have scrolling.
			if (System.Console.CursorTop + 1 == windowBottom)
			{
				// Destructive scrolling.
				if (System.Console.BufferHeight == windowBottom)
				{
					// Line 0 in the buffer will be lost.
					BufferOffset++;
				}
			}

			System.Console.WriteLine();
		}

		/// <summary>
		/// Shows or hides the cursor.
		/// </summary>
		public static void ShowCursor(bool shown)
		{
			Write(shown ? "\x1b[?25h" : "\x1b[?25l");
		}

		/// <summary>
		/// Sets the foreground color.
		/// </summary>
		/// <returns>
		/// <c>false</c> if <c>TerminalColor.Invalid</c> was passed in; otherwise <c>true</c>.
		/// </returns>
		public static bool SetFgColor(uint color)
		{
			switch (color)
			{
				case TerminalColor.Default:
					ResetFgColor();
					return true;

				case TerminalColor.Invalid:
					return false;

				default:
					byte r = (byte)(color >> 16);
					byte g = (byte)(color >> 8);
					byte b = (byte)(color >> 0);

					System.Console.Write($"\x1b[38;2;{r};{g};{b}m");
					return true;
			}
		}

		/// <summary>
		/// Sets the background color.
		/// </summary>
		/// <returns>
		/// <c>false</c> if <c>TerminalColor.Invalid</c> was passed in; otherwise <c>true</c>.
		/// </returns>
		public static bool SetBgColor(uint color)
		{
			switch (color)
			{
				case TerminalColor.Default:
					ResetBgColor();
					return true;

				case TerminalColor.Invalid:
					return false;

				default:
					byte r = (byte)(color >> 16);
					byte g = (byte)(color >> 8);
					byte b = (byte)(color >> 0);

					System.Console.Write($"\x1b[48;2;{r};{g};{b}m");
					return true;
			}
		}

		/// <summary>
		/// Resets the foreground color.
		/// </summary>
		public static void ResetFgColor()
		{
			System.Console.Write("\x1b[39m");
		}

		/// <summary>
		/// Resets the background color.
		/// </summary>
		public static void ResetBgColor()
		{
			System.Console.Write("\x1b[49m");
		}

		/// <summary>
		/// Sets foreground and backend colors.
		/// </summary>
		public static bool SetColors(uint fgColor, uint bgColor)
		{
			bool changed = false;
			changed |= SetFgColor(fgColor);
			changed |= SetBgColor(bgColor);
			return changed;
		}

		public static void SetUnderline(bool enabled)
        {
			System.Console.Write(enabled ? "\x1b[4m" : "\x1b[24m");
        }

		public static void SetInverted(bool enabled)
		{
			System.Console.Write(enabled ? "\x1b[7m" : "\x1b[27m");
		}

		/// <summary>
		/// Resets foreground and backend colors.
		/// </summary>
		public static void ResetColors()
		{
			System.Console.Write("\x1b[39m\x1b[49m");
		}

		/// <summary>
		/// Resets foreground and backend colors, if <c>changed</c> is <c>true</c>.
		/// </summary>
		public static void ResetColors(bool changed)
		{
			if (changed)
			{
				ResetColors();
			}
		}

		/// <summary>
		/// Scrolls up the whole buffer by the given amount of lines.
		/// </summary>
		public static void ScrollUp(int lines)
		{
			System.Console.Write($"\x1b[{lines}S");
		}
	}
}
