﻿# v0.3.2 (2022-03-19)
- Added IFormatable interface.
- Improved documentation with mdbook.
- Simplified sequence interfaces.
- Added sort functionality.
- Added support for `when` in matches.
- Added optimization support for aggregator functions.
- Added trigonometric functions and PI.
- Added aggregator functions (`sum`, `product`, ...).
- Added `kv-key` and `kv-val` accessor functions.
- Added `clamp`, `identity`, `bind`, `second` and `third`.
- Better support for function naming. Added `fn-name`.
- Renamed `mapcat` to `flatmap`.
- Proper REPL support for "[]" and "{}" nesting.
- Added REPL class / library to allow extending the REPL.
- Added docker image build for the REPL.

# v0.3.1 (2022-03-02)
- Added support for `=` match function.
- Added support for `assign!` and `undef`.
- Added mutable data structures.
- Changed evaluation mechanism for more performance / extensibility.
- Added `when-let` and `if-let`.
- Added `bigint` for working with large integers.
- Added `chars`, `chars!` and `make-string` for some string manipulation.
- Added `re-match`, `re-matches`, `re-groups` and `re-replace` to improve regex support.
- Added lots of documentation, `LICENSE` file etc.
- Added / updated a lot of the test code.

# v0.3.0 (2022-02-17)
- Added proper macro support.
- Added support for syntax-quoting.
- Added support for generating symbol names.
- Symbols and keywords are per-interpreter to enable proper cleanup.
- Some API changes (`set`/`remove` instead of `assoc`/`dissoc` etc.).
- Standard-library functions in LISP.
- Better arity errors (mentioning the function / macro name).
- Fixed handling of non-char producing keys in REPL.

# v0.2.0 (2022-02-15)
- Added REPL utility.
- Match expressions in `let`, `def`, `fn`, `defn`, etc.
- Support for multiple forms in `fn` and `defn`.
- Support for lazy sequences constructed using `lazy-seq`.
- `VectorSkipSequence` to optimize `rest` / `cons` on vectors.
- Various performance optimizations.
- Fixed parsing of negative numbers.
- Print and Interrupt handlers on Interpreter.
- Added support for the `for` special form.
- Various additions to the standard library.

# v0.1.0 (2022-01-31)
- Initial release.
